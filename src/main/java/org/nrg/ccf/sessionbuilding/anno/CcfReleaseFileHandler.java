package org.nrg.ccf.sessionbuilding.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CcfReleaseFileHandler {
	
	String FILE_HANDLER = "releaseFileHandler";
	
	String description() default "";

}
