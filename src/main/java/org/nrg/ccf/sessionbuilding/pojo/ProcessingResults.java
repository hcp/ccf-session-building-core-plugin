package org.nrg.ccf.sessionbuilding.pojo;

import java.util.List;

import org.nrg.xdat.om.XnatMrsessiondata;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProcessingResults {
	
	private List<String> errorList;
	private List<XnatMrsessiondata> sessionList;
	
}
