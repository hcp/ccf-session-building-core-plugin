package org.nrg.ccf.sessionbuilding.abst;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sessionbuilding.exception.ReleaseTransferException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseFileHandlerI;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.interfaces.FilepathTransformerI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatAddfieldI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.CatCatalog;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.base.auto.AutoXnatResource;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.helpers.file.StoredFile;
import org.nrg.xnat.helpers.resource.XnatResourceInfo;
import org.nrg.xnat.helpers.resource.direct.DirectScanResourceImpl;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractCcfReleaseFileHandler implements CcfReleaseFileHandlerI {
	
	final protected List<XnatImagesessiondata> _srcSessions;
	final protected XnatImagesessiondata _destSession;
	final protected CcfReleaseRulesI _releaseRules;
	final protected UserI _user;
	protected static final String JSON_UPPERCASE_ENDING = ".JSON";
	protected static final String SERIES_DESCRIPTION_VAL = "SeriesDescription";
	protected static final String SERIES_INSTANCE_UID_VAL = "SeriesInstanceUID";
	protected static final String READOUT_DIRECTION_VAL = "ReadoutDirection";
	protected static final String ACQUISITION_TIME_VAL = "AcquisitionTime";
	protected static final String ACQUISITION_DATE_TIME_VAL = "AcquisitionDateTime";
	//protected static final String STUDY_INSTANCE_UID_VAL = "StudyInstanceUID";
	//protected static final String READOUT_SAMPLE_SPACING_VAL = "ReadoutSampleSpacing";
	//protected static final String FMRI_VERSION_NUMBER = "FmriVersionNumber";
	protected static final String PULSE_SEQUENCE_DETAILS = "PulseSequenceDetails";
	final protected Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
	final protected Gson gson = new GsonBuilder().create();
	
	public AbstractCcfReleaseFileHandler(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession, CcfReleaseRulesI releaseRules, UserI user) {
		super();
		_srcSessions = srcSessions;
		_destSession = destSession;
		_releaseRules = releaseRules;
		_user = user;
	}
	public List<XnatImagesessiondata> getSrcSessions() {
		return _srcSessions;
	}

	public XnatImagesessiondata getDestSession() {
		return _destSession;
	}

	public UserI getUser() {
		return _user;
	}

	protected Gson getGson() {
		return gson;
	}

	protected Gson getPrettyGson() {
		return prettyGson;
	}
	
	protected DirectScanResourceImpl getScanModifier(XnatImagescandata combScan, EventMetaI ci) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void transferFilesForResourceWithHandlingForJsonFiles(String sourceSessionResource, String destSessionResource, 
			List<String> pathMatchRegex, FilepathTransformerI transformer, EventMetaI ci) throws ReleaseTransferException {
		transferFilesForResource(destSessionResource, destSessionResource, pathMatchRegex, transformer, ci, true);
	}
	
	public void transferFilesForResource(String sourceSessionResource, String destSessionResource, List<String> pathMatchRegex, 
			FilepathTransformerI transformer, EventMetaI ci)
			throws ReleaseTransferException {
		transferFilesForResource(destSessionResource, destSessionResource, pathMatchRegex, transformer, ci, false);
	}
	
	@SuppressFBWarnings
	private void transferFilesForResource(String sourceSessionResource, String destSessionResource, 
			List<String> pathMatchRegex, FilepathTransformerI transformer, EventMetaI ci, boolean handleJson) throws ReleaseTransferException {
		final XnatImagesessiondata combSess = getDestSession();
		for (final XnatImagesessiondata expt : getSrcSessions()) {
			for (final XnatImagescandataI scan : expt.getScans_scan()) {
				if (scan.getDbsession() == null || !scan.getDbsession().equals(combSess.getLabel())) {
					continue;
				}
				//// TEMPORARY
				//if (scan.getType().contains("tfMRI") || scan.getType().contains("rfMRI")) {
				//	continue;
				//}
				//// END TEMPORARY
				for (final XnatAbstractresourceI resourceI : scan.getFile()) {
					if (!(resourceI.getLabel().equals(sourceSessionResource) && resourceI instanceof XnatResource)) {
						continue;
					}
					final XnatResource resource = (XnatResource)resourceI;
					if (resource instanceof XnatResourcecatalog) {
						// Let's be sure to reread files from filesystem for resource catalogs.
						((XnatResourcecatalog)resource).clearFiles();
						
					}
					if (resource.getFileResources(CommonConstants.ROOT_PATH) == null || resource.getFileResources(CommonConstants.ROOT_PATH).size()<1) {
						continue;
					}
					final XnatImagescandata combScan = combSess.getScanById(scan.getDbid());
					if (combScan == null) {
						log.warn("ERROR:  Could not locate and open destination session scan (SCAN=" + scan.getDbid() + ")");
						continue;
					}
					
					final DirectScanResourceImpl scanModifier = new DirectScanResourceImpl(combScan, combSess, true, _user, ci);
					XnatResourcecatalog destResource = null;
					for (final ResourceFile rFile : resource.getFileResources(CommonConstants.ROOT_PATH)) {
						String resourcePath = resource.getFullPath(CommonConstants.ROOT_PATH);
						resourcePath = resourcePath.replaceFirst("[/]$", "");
						resourcePath = resourcePath.substring(0,resourcePath.lastIndexOf(File.separator));
						final File file = rFile.getF();
						if (pathMatchRegex!=null) {
							boolean hasMatch = false;
							for (final String regex : pathMatchRegex) {
								if (rFile.getAbsolutePath().matches(regex)) {
									hasMatch = true;
									continue;
								}
							}
							if (!hasMatch) {
								continue;
							}
						}
						// Initialize resource now.  We only want to create it if we're going to save to it.
						if (destResource == null) {
							destResource = getOrCreateResource(combScan, scanModifier, destSessionResource, scan, ci);
							if (destResource==null) {
								continue;
							}
							destResource.clearCountAndSize();
							destResource.clearFiles();
						}
						File tempFile = null;
						try {
							tempFile = File.createTempFile("CCFSessionBuilding", "fil");
							if (handleJson && file.getName().toUpperCase().endsWith(JSON_UPPERCASE_ENDING)) {
								final ByteArrayOutputStream baos = new ByteArrayOutputStream();
								FileUtils.copyFile(file, baos);
								String fileString = baos.toString(StandardCharsets.UTF_8.name());
								baos.close();
								JsonElement jsonEle;
								try {
									 jsonEle = new JsonParser().parse(fileString);
								} catch (Exception e) {
									log.error("JSON Parsing Error (FILE=" + file.getName() + "): \n" + fileString);
									// Sometimes gets AcquisitionDateTime values wrong.  Let's first try putting quotes around the value.  
									//  That should work.  If there are still problems, let's just remove AcquisitionDateTime.
									try {
										fileString = fileString.replaceAll("(\"" + ACQUISITION_DATE_TIME_VAL + "\": )([0-9][^,]*)(,)", "$1\"$2\"$3");
										jsonEle = new JsonParser().parse(fileString);
									} catch (Exception e2) {
										try {
											fileString = fileString.replaceAll("\"" + ACQUISITION_DATE_TIME_VAL + "\":[^,]*,", "");
											jsonEle = new JsonParser().parse(fileString);
										} catch (Exception e3) {
											log.error("JSON Parsing Error on Retry (FILE=" + file.getName() + "): \n" + fileString);
											throw e3;
										}
									}
								}
								if (jsonEle.isJsonObject()) {
									final JsonObject jsonObj = jsonEle.getAsJsonObject();
									handleJsonObject(jsonObj, combScan);
								} else if (jsonEle.isJsonArray()) {
									final JsonArray jsonArr = jsonEle.getAsJsonArray();
									handleJsonArray(jsonArr, combScan);
								}
								// Only continue JSON processing if we've got a StudyInstanceUID, otherwise we'll just return JSON as is
								final InputStream is;
								try {
									is = new ByteArrayInputStream(prettyGson.toJson(jsonEle).getBytes(StandardCharsets.UTF_8));
								} catch (Exception e) {
									log.error("JSON Creation Error (FILE=" + file.getName() + "): " + fileString);
									throw e;
								}
								FileUtils.copyInputStreamToFile(is, tempFile);
							} else {
								FileUtils.copyFile(file, tempFile);
							}
							final ArrayList<StoredFile> fws = new ArrayList<>();
							fws.add(new StoredFile(tempFile, false));
							String newName = file.getName();
							if (transformer != null) {
								newName = transformer.transformFilename(newName,expt,combSess,scan,combScan);
							}
							/*
							if (substSessionLabel) {
								newName = newName.replaceFirst("^" + expt.getLabel(), combSess.getLabel());
							}
							if (substSeriesDesc) {
								newName = newName.replaceFirst("_" + scan.getSeriesDescription(), "_" + combScan.getSeriesDescription());
							}
							*/
							String newDir = file.getParentFile().getAbsolutePath().replace(resourcePath, "");
							if (transformer != null) {
								newDir = transformer.transformParentDirectoryPath(newDir,file,expt,combSess,scan,combScan);
							}
							final String newPath = (newDir != null && newDir.length()>0) ? newDir + File.separator + newName : newName;
							scanModifier.addFile(fws, destSessionResource, null, newPath, new XnatResourceInfo(_user, new Date(), new Date()), false);
						} catch (Exception e) {
							throw new ReleaseTransferException("Error copying file", e);
						} finally {
							if (tempFile != null && tempFile.exists()) {
								tempFile.delete();
							}
						}
					}
					if (destResource == null) {
						continue;
					}
					destResource.calculate(CommonConstants.ROOT_PATH);
					destResource.setFileCount(destResource.getCount(CommonConstants.ROOT_PATH));
					destResource.setFileSize(destResource.getSize(CommonConstants.ROOT_PATH));
					try {
						//SaveItemHelper.authorizedSave(combScan,_user,false,true,ci);
						SaveItemHelper.authorizedSave(destResource,_user,false,true,ci);
					} catch (Exception e) {
						// Do nothing.  Only the file counts are affected.
					}
				}
			}
		}
		
	}
	
	protected void handleJsonObject(final JsonObject jsonObj, final XnatImagescandataI scan) {
		anonymizeSeriesInstanceUidValue(jsonObj, scan);
		pullAdditionalValuesFromScan(jsonObj, scan);
		removeSpecifiedValuesFromScan(jsonObj, scan);
	}
	
	protected void handleJsonArray(final JsonArray jsonArr, final XnatImagescandataI scan) {
		for (final JsonElement innerEle : jsonArr) {
			if (innerEle.isJsonArray()) {
				final JsonArray innerArr = innerEle.getAsJsonArray();
				handleJsonArray(innerArr, scan);
			} else if (innerEle.isJsonObject()) {
				final JsonObject innerObj = innerEle.getAsJsonObject();
				handleJsonObject(innerObj, scan);
			}
		}
	}
	
	protected void anonymizeSeriesInstanceUidValue(JsonObject jsonObj, XnatImagescandataI scan) {
		if (jsonObj.has(SERIES_INSTANCE_UID_VAL)) {
			jsonObj.addProperty(SERIES_INSTANCE_UID_VAL,scan.getUid());
		}
	}
	
	protected void removeSpecifiedValuesFromScan(JsonObject jsonObj, XnatImagescandataI scan) {
		if (scan instanceof XnatMrscandata && (jsonObj.has(SERIES_DESCRIPTION_VAL) || jsonObj.has(ACQUISITION_DATE_TIME_VAL))) {
			if (jsonObj.has(ACQUISITION_DATE_TIME_VAL)) {
				if (!jsonObj.has(ACQUISITION_TIME_VAL)) {
					final JsonElement acquisitionTimeEle = jsonObj.get(ACQUISITION_DATE_TIME_VAL);
					final String acquisitionTime = acquisitionTimeEle.getAsString().replaceFirst("^.*T", "");
					jsonObj.addProperty(ACQUISITION_TIME_VAL, acquisitionTime);
				}
				jsonObj.remove(ACQUISITION_DATE_TIME_VAL);
			}
		}
	}
	
	protected void pullAdditionalValuesFromScan(JsonObject jsonObj, XnatImagescandataI scan) {
		if (scan instanceof XnatMrscandata && jsonObj.has(SERIES_DESCRIPTION_VAL)) {
			
			/*  Per Mike Harms 2017/09/01 e-mail, ReadoutSampleSpacing is now in the JSON as DwellTime 
			if (!jsonObj.has(READOUT_SAMPLE_SPACING_VAL)) {
				final String readoutSampleSpacingStr = ((XnatMrscandata)scan).getParameters_readoutsamplespacing();
				Number readoutSampleSpacing = null;
				try {
					readoutSampleSpacing = Float.parseFloat(readoutSampleSpacingStr);
				} catch (Exception e) {
					// Do nothing
				}
				if (readoutSampleSpacing!=null) {
					jsonObj.addProperty(READOUT_SAMPLE_SPACING_VAL, readoutSampleSpacing);
				} else if (readoutSampleSpacingStr!=null && readoutSampleSpacingStr.length()>0) {
					jsonObj.addProperty(READOUT_SAMPLE_SPACING_VAL, readoutSampleSpacingStr);
				}
			}
			*/
			
			if (!jsonObj.has(READOUT_DIRECTION_VAL)) {
				final String readoutDirection = ((XnatMrscandata)scan).getParameters_readoutdirection();
				if (readoutDirection!=null) {
					jsonObj.addProperty(READOUT_DIRECTION_VAL, readoutDirection);
				}
			}
			// fmriVersionInfo (into pulseSequenceDetails)
			final String versionInfo = getFmriVersionInfo(scan);
			if (versionInfo != null) {
				final JsonElement seqEle = jsonObj.get(PULSE_SEQUENCE_DETAILS);
				String pulseSequenceDetails = (seqEle!=null) ? seqEle.getAsString() + "; " + versionInfo : versionInfo;
				jsonObj.addProperty(PULSE_SEQUENCE_DETAILS, pulseSequenceDetails);
			}
			
		}
	}
	
	protected String getFmriVersionInfo(XnatImagescandataI scan) {
		final List<XnatAddfieldI> addParams = ((XnatMrscandata)scan).getParameters_addparam();
		for (final XnatAddfieldI addParam : addParams) {
			final String fieldName = addParam.getName();
			final String fieldValue = addParam.getAddfield();
			if (fieldName==null || fieldValue==null || fieldName.length()<1 || fieldValue.length()<1) {
				continue;
			}
			if (fieldName.equals("fMRI_Version_Sequence")) {
				return fieldValue;
			}
		}
		return null;
	}
	protected XnatResourcecatalog getOrCreateResource(XnatImagescandata destScan, DirectScanResourceImpl scanModifier, String resourceLabel,
					XnatImagescandataI srcScan, EventMetaI ci) throws ReleaseTransferException {
		
		XnatAbstractresourceI resource =  scanModifier.getResourceByLabel(resourceLabel, null);
		if (resource==null) {
			resource = createResource(destScan, resourceLabel, srcScan, ci);
		}
		return (resource instanceof XnatResourcecatalog) ? (XnatResourcecatalog)resource : null;
		
	}
	
	protected XnatResourcecatalog createResource(XnatImagescandata destScan, String resourceLabel, XnatImagescandataI srcScan,
			EventMetaI ci) throws ReleaseTransferException {
		
		final XnatProjectdata proj = _destSession.getProjectData();
		
		final File scanDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + _destSession.getLabel() +
					File.separator + "SCANS" + File.separator + destScan.getId() + File.separator + resourceLabel);
		if (!scanDir.exists()) {
			scanDir.mkdirs();
		}
		final File catFile = new File(scanDir,resourceLabel.toLowerCase().replace("_", "") + destScan.getId() + CommonConstants.CATXML_EXT);
		final CatCatalog cat = new CatCatalog();
		final XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			final FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			throw new ReleaseTransferException("Couldn't write catalog XML file",e);
		} catch (Exception e) {
			throw new ReleaseTransferException("Couldn't write catalog XML file",e);
		}
		ecat.setLabel(resourceLabel);
		final DirectScanResourceImpl srcScanMod = new DirectScanResourceImpl((XnatImagescandata) srcScan,
				(XnatImagesessiondata)XnatImagesessiondata.getXnatExperimentdatasById(
						srcScan.getImageSessionId(), _user, false), false, _user, ci);
		final XnatAbstractresourceI srcResource =  srcScanMod.getResourceByLabel(resourceLabel, null);
		if (srcResource!=null && srcResource instanceof AutoXnatResource) {
			ecat.setFormat(((AutoXnatResource)srcResource).getFormat());
			ecat.setContent(((AutoXnatResource)srcResource).getContent());
			ecat.setContent(resourceLabel);
		}
		// Save resource to scan
		//final String eventStr = "Resource " + resourceLabel + " created under scan " + destScan.getId() + " (SD=" + destScan.getSeriesDescription() + ")" ;
		try {
			destScan.addFile(ecat);
			
			//final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(_user, exp.getItem(),
			//		EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_RESOURCE, eventStr, null));
			//final EventMetaI ci = wrk.buildEvent();
			if (SaveItemHelper.authorizedSave(destScan,_user,false,true,ci)) {
				return ecat;
			}
			throw new ReleaseTransferException("ERROR:  Couldn't add resource to scan - (SCAN=" + destScan.getId() + ")." ,new Exception());
			//if (SaveItemHelper.authorizedSave(destScan,_user,false,true,ci)) {
			//	PersistentWorkflowUtils.complete(wrk, ci);
			//} else {
			//	PersistentWorkflowUtils.fail(wrk,ci);
			//}
		} catch (Exception e) {
			throw new ReleaseTransferException("ERROR:  Couldn't add resource to scan - " + e.getMessage(),new Exception());
		}
		
	}
	
}
