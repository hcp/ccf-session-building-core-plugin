package org.nrg.ccf.sessionbuilding.exception;

public class ResourceGeneratorException extends Exception{
	
	private static final long serialVersionUID = 7140541873549995315L;

	public ResourceGeneratorException(String msg,Throwable e){
		super(msg,e);
	}
	
	public ResourceGeneratorException(String msg){
		super(msg);
	}
	
}

