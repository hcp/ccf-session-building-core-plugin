package org.nrg.ccf.sessionbuilding.interfaces;

import org.nrg.ccf.sessionbuilding.exception.ReleaseTransferException;

public interface CcfReleaseFileHandlerI {
	
	public void transferFiles() throws ReleaseTransferException;
	
}

