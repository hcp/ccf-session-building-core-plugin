package org.nrg.ccf.sessionbuilding.preferences;

import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.common.utilities.abst.AbstractProjectPreferenceBean;
import org.nrg.ccf.common.utilities.components.PreferenceUtils;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.exceptions.UnknownToolId;
import org.nrg.prefs.services.NrgPreferenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Component
@NrgPreferenceBean(toolId = "ccfSessionBuilding", toolName = "CCF Session Building Plugin Preferences",
			description = "Manages CCF session building resources plugin preferences", strict = true)
public class CcfSessionBuildingPreferences extends AbstractProjectPreferenceBean {
	
	private static final long serialVersionUID = 4598961443427106028L;
	private static final Logger _logger = LoggerFactory.getLogger(CcfSessionBuildingPreferences.class);
	public static final String SESSION_BUILDING_ENABLED = "ccfSessionBuildingEnabled";
	public static final String SESSION_BUILDING_HARDOVERRIDE = "ccfSessionBuildingHardOverride";
	public static final String SESSION_BUILDING_RULESCLASS = "ccfSessionBuildingRulesClass";
	public static final String SESSION_BUILDING_SELECTABLE_RULESCLASSES = "ccfSessionBuildingSelectableRulesClasses";
	public static final String SESSION_BUILDING_FILEHANDLER = "ccfSessionBuildingFileHandler";
	public static final String SESSION_BUILDING_SELECTABLE_FILEHANDLERS = "ccfSessionBuildingSelectableFileHandlers";
	public static final String SESSION_BUILDING_RESOURCEGEN = "ccfSessionBuildingResourceGen";
	public static final String SESSION_BUILDING_SELECTABLE_RESOURCEGEN = "ccfSessionBuildingSelectableResourceGen";
	public static final String SESSION_BUILDING_BUILDPROJECT = "ccfSessionBuildingBuildProject";
	public static final String SESSION_BUILDING_SESSIONLABEL = "sessionLabel";
	
	@Autowired
	public CcfSessionBuildingPreferences(NrgPreferenceService preferenceService, PreferenceUtils preferenceUtils) {
		super(preferenceService, preferenceUtils);
	}
	
	// Make PreferenceBeanHelper happy.  It wants the annotation to be on a getter with no arguments.
	@NrgPreference
	@SuppressFBWarnings
	public Boolean getCcfSessionBuildingEnabled() {
		return null;
	}
	
	public Boolean getCcfSessionBuildingEnabled(final String entityId) {
		return this.getBooleanValue(SCOPE, entityId, SESSION_BUILDING_ENABLED);
	}
	
	public void setCcfSessionBuildingEnabled(final String entityId, final boolean enabled) {
		try {
			this.setBooleanValue(SCOPE, entityId, enabled, SESSION_BUILDING_ENABLED);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			_logger.error("Exception setting session building enabled preference", e);
		}
	}
	
	// Make PreferenceBeanHelper happy.  It wants the annotation to be on a getter with no arguments.
	@NrgPreference
	@SuppressFBWarnings
	public Boolean getCcfSessionBuildingHardOverride() {
		return null;
	}
	
	public Boolean getCcfSessionBuildingHardOverride(final String entityId) {
		return this.getBooleanValue(SCOPE, entityId, SESSION_BUILDING_HARDOVERRIDE);
	}
	
	public void setCcfSessionBuildingHardOverride(final String entityId, final boolean hardOverride) {
		try {
			this.setBooleanValue(SCOPE, entityId, hardOverride, SESSION_BUILDING_HARDOVERRIDE);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			_logger.error("Exception setting session building hard_override preference", e);
		}
	}
	
	@NrgPreference
	public String getCcfSessionBuildingRulesClass() {
		return null;
	}
	
	public String getCcfSessionBuildingRulesClass(final String entityId) {
		return this.getValue(SCOPE, entityId, SESSION_BUILDING_RULESCLASS);
	}
	
	public void setCcfSessionBuildingRulesClass(final String entityId, final String rulesClass) {
		try {
			this.set(SCOPE, entityId, rulesClass, SESSION_BUILDING_RULESCLASS);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			_logger.error("Exception setting session building default release rules class preference", e);
		}
	}
	
	@NrgPreference
	public List<String> getCcfSessionBuildingSelectableRulesClasses() {
		return null;
	}
	
	public List<String> getCcfSessionBuildingSelectableRulesClasses(final String entityId) {
		//return this.getListValue(SCOPE, entityId, SESSION_BUILDING_SELECTABLE_RULESCLASSES);
		// Workaround for getListValue currently not working in XNAT
		return getJsonValue(this.getValue(SCOPE, entityId, SESSION_BUILDING_SELECTABLE_RULESCLASSES));
	}
	
	private List<String> getJsonValue(final String value) {
		try {
			final Gson gson = new Gson();
			final List<String> gsonList = gson.fromJson(value, new TypeToken<List<String>>(){}.getType());
			return (gsonList != null) ? gsonList : new ArrayList<String>();
		} catch (Throwable t) {
			return new ArrayList<>();
		}
	}

	public void setCcfSessionBuildingSelectableRulesClasses(final String entityId, final List<String> rulesClasses) {
		try {
			this.setListValue(SCOPE, entityId, SESSION_BUILDING_SELECTABLE_RULESCLASSES, rulesClasses);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			_logger.error("Exception setting session building selectable release rules classes preference", e);
		}
	}
	
	@NrgPreference
	public String getCcfSessionBuildingFileHandler() {
		return null;
	}
	
	public String getCcfSessionBuildingFileHandler(final String entityId) {
		return this.getValue(SCOPE, entityId, SESSION_BUILDING_FILEHANDLER);
	}
	
	public void setCcfSessionBuildingFileHandler(final String entityId, final String fileHandler) {
		try {
			this.set(SCOPE, entityId, fileHandler, SESSION_BUILDING_FILEHANDLER);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			_logger.error("Exception setting session building default file handler preference", e);
		}
	}
	
	@NrgPreference
	public List<String> getCcfSessionBuildingSelectableFileHandlers() {
		return null;
	}
	
	public List<String> getCcfSessionBuildingSelectableFileHandlers(final String entityId) {
		//return this.getListValue(SCOPE, entityId, SESSION_BUILDING_SELECTABLE_FILEHANDLERS);
		// Workaround for getListValue currently not working in XNAT
		return getJsonValue(this.getValue(SCOPE, entityId, SESSION_BUILDING_SELECTABLE_FILEHANDLERS));
	}
	
	public void setCcfSessionBuildingSelectableFileHandlers(final String entityId, final List<String> fileHandlers) {
		try {
			this.setListValue(SCOPE, entityId, SESSION_BUILDING_SELECTABLE_FILEHANDLERS, fileHandlers);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			_logger.error("Exception setting session building selectable file handlers preference", e);
		}
	}
	
	@NrgPreference
	public String getCcfSessionBuildingResourceGen() {
		return null;
	}
	
	public String getCcfSessionBuildingResourceGen(final String entityId) {
		return this.getValue(SCOPE, entityId, SESSION_BUILDING_RESOURCEGEN);
	}
	
	public void setCcfSessionBuildingResourceGen(final String entityId, final String resourceGen) {
		try {
			this.set(SCOPE, entityId, resourceGen, SESSION_BUILDING_RESOURCEGEN);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			_logger.error("Exception setting session building default resource generator preference", e);
		}
	}
	
	@NrgPreference
	public List<String> getCcfSessionBuildingSelectableResourceGen() {
		return null;
	}
	
	public List<String> getCcfSessionBuildingSelectableResourceGen(final String entityId) {
		//return this.getListValue(SCOPE, entityId, SESSION_BUILDING_SELECTABLE_RESOURCEGEN);
		// Workaround for getListValue currently not working in XNAT
		return getJsonValue(this.getValue(SCOPE, entityId, SESSION_BUILDING_SELECTABLE_RESOURCEGEN));
	}
	
	public void setCcfSessionBuildingSelectableResourceGen(final String entityId, final List<String> resourceGenerators) {
		try {
			this.setListValue(SCOPE, entityId, SESSION_BUILDING_SELECTABLE_RESOURCEGEN, resourceGenerators);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			_logger.error("Exception setting session building selectable resource generators preference", e);
		}
	}
	
	@NrgPreference
	public String getCcfSessionBuildingBuildProject() {
		return null;
	}
	
	public String getCcfSessionBuildingBuildProject(final String entityId) {
		return this.getValue(SCOPE, entityId, SESSION_BUILDING_BUILDPROJECT);
	}
	
	public void setCcfSessionBuildingBuildProject(final String entityId, final String rulesClass) {
		try {
			this.set(SCOPE, entityId, rulesClass, SESSION_BUILDING_BUILDPROJECT);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			_logger.error("Exception setting session building build_project preference", e);
		}
	}
	
}
