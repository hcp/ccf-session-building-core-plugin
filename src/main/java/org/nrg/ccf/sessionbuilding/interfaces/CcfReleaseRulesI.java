package org.nrg.ccf.sessionbuilding.interfaces;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sessionbuilding.constants.SessionBuildingConstants.SessionBuildingReportFormats;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesReportException;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;

public interface CcfReleaseRulesI {
	
	public List<String> applyRulesToScans(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, Map<String,String> params, boolean errorOverride) throws ReleaseRulesException;
	
	public boolean isStructuralScan(XnatImagescandataI scan);
	
	public boolean requireScanUidAnonymization();
	
	public List<String> getSelectionCriteria();
	
	public List<String> getParametersYaml();
	
	public List<XnatMrsessiondata> filterExptList(List<XnatMrsessiondata> projectExpts,Map<String,String> params, UserI user) throws ReleaseRulesException;
	
	public String getDefaultSessionLabel(XnatProjectdata proj, XnatSubjectdata subj, Map<String,String> params);
	
	String getReport(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions, 
			CcfReleaseRulesI releaseRules, SessionBuildingReportFormats reportFormat) throws ReleaseRulesReportException;
	
	List<String[]> getReportList(List<XnatMrscandata> subjectScans, List<XnatMrsessiondata> subjectSessions)
			throws ReleaseRulesReportException;

	LinkedHashMap<String, String> getReportColMap();

	public Comparator<XnatImagescandata> getTemporalComparator(List<XnatMrsessiondata> expts);

}

