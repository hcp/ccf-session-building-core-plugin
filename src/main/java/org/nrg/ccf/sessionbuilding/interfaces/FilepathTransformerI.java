package org.nrg.ccf.sessionbuilding.interfaces;

import java.io.File;

import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;

public interface FilepathTransformerI {
	
	String transformParentDirectoryPath(String newDirectory, File sessionFile, XnatImagesessiondata srcSession, XnatImagesessiondata destSess,
			XnatImagescandataI srcSessScan, XnatImagescandata destSessScan);

	String transformFilename(String newName, XnatImagesessiondata srcSession, XnatImagesessiondata destSess,
			XnatImagescandataI srcSessScan, XnatImagescandata destSessScan);
	
}
