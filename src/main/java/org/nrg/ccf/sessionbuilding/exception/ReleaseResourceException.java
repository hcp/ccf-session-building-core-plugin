package org.nrg.ccf.sessionbuilding.exception;

public class ReleaseResourceException extends Exception {

	private static final long serialVersionUID = 6629790608778954409L;

	public ReleaseResourceException(String string, Exception e) {
		super(string, e);
	}

}
