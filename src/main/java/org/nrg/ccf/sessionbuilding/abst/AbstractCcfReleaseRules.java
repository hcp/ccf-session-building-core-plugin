package org.nrg.ccf.sessionbuilding.abst;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

import org.nrg.ccf.sessionbuilding.comparators.DefaultTemporalComparator;
import org.nrg.ccf.sessionbuilding.constants.SessionBuildingConstants.SessionBuildingReportFormats;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesReportException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.preferences.CcfSessionBuildingPreferences;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.python.google.common.collect.ImmutableList;

public abstract class AbstractCcfReleaseRules implements CcfReleaseRulesI {
	
	protected static LinkedHashMap<String, String> rptColMap = new LinkedHashMap<>();
	protected final CcfSessionBuildingPreferences _sessionBuildingPreferences = XDAT.getContextService().getBean(CcfSessionBuildingPreferences.class);
	
	
	@Override
	public List<String> getSelectionCriteria() {
		return ImmutableList.of();
	}
	
	@Override
	public List<String> getParametersYaml() {
		return ImmutableList.of();
	}
	
	@Override
	public String getReport(final List<XnatMrscandata> subjectScans, final List<XnatMrsessiondata> subjectSessions, 
			CcfReleaseRulesI releaseRules, SessionBuildingReportFormats reportFormat) throws ReleaseRulesReportException {
		if (reportFormat==null || reportFormat.equals(SessionBuildingReportFormats.CSV)) {
			return CcfReleaseRulesUtils.getReportCSV(subjectScans, subjectSessions, releaseRules, rptColMap);
		} else if (reportFormat.equals(SessionBuildingReportFormats.JSON)) {
			return CcfReleaseRulesUtils.getReportJSON(subjectScans, subjectSessions,releaseRules,  rptColMap);
		} else {
			throw new ReleaseRulesReportException("Invalid report format specified.  Valid formats are CSV and JSON.");
		}
	}
	
	@Override
	public List<String[]> getReportList(final List<XnatMrscandata> subjectScans, final List<XnatMrsessiondata> subjectSessions) throws ReleaseRulesReportException {
		return CcfReleaseRulesUtils.getReportList(subjectScans, subjectSessions, this, rptColMap, false, true);
	}
	
	@Override
	public LinkedHashMap<String, String> getReportColMap() {
			return rptColMap;
	}
	
	public XnatMrsessiondata getScanSession(final XnatMrscandata subjectScan, List<XnatMrsessiondata> subjectSessions) {
		if (subjectSessions == null) {
			return null;
		}
		for (final XnatMrsessiondata session : subjectSessions) {
			if (session.getId().equals(subjectScan.getImageSessionId())) {
				return session;
			}
			
		}
		return null;
	}
	
	@Override
	public Comparator<XnatImagescandata> getTemporalComparator(List<XnatMrsessiondata> expList) {
		return new DefaultTemporalComparator(expList);
	}
	
	public List<XnatMrscandata> getReversedScanList(List<XnatMrscandata> subjectScans) {
		List<XnatMrscandata> scanList = new ArrayList<>();
		scanList.addAll(subjectScans);
		Collections.reverse(scanList);
		return scanList;
	}
	
}
