package org.nrg.ccf.sessionbuilding.services;

import java.util.List;

import org.nrg.ccf.sessionbuilding.dao.SessionBuildingRulesErrorsDAO;
import org.nrg.ccf.sessionbuilding.entities.SessionBuildingRulesErrors;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SessionBuildingRulesErrorsService extends AbstractHibernateEntityService<SessionBuildingRulesErrors, SessionBuildingRulesErrorsDAO> {
	
	@Transactional
	public List<SessionBuildingRulesErrors> getSessionBuildingRulesErrorsForProject(String projectId, Class<? extends CcfReleaseRulesI> rulesClass) {
		return getDao().getSessionBuildingRulesErrorsForProject(projectId, rulesClass);
	}
	
	@Transactional
	public List<SessionBuildingRulesErrors> getSessionBuildingRulesErrorsForProject(String projectId, String selectionCriteria, Class<? extends CcfReleaseRulesI> rulesClass) {
		return getDao().getSessionBuildingRulesErrorsForProject(projectId, selectionCriteria, rulesClass);
	}
	
	@Transactional
	public SessionBuildingRulesErrors getSessionBuildingRulesErrors(String projectId, String subjectId, String selectionCriteria, Class<? extends CcfReleaseRulesI> rulesClass) {
		return getDao().getSessionBuildingRulesErrors(projectId, subjectId, selectionCriteria, rulesClass);
	}
	
	@Transactional
	public SessionBuildingRulesErrors getSessionBuildingRulesErrorsOrCreateNew(String projectId, String subjectId, String selectionCriteria, Class<? extends CcfReleaseRulesI> rulesClass) {
		SessionBuildingRulesErrors returnRules = getDao().getSessionBuildingRulesErrors(projectId, subjectId, selectionCriteria, rulesClass);
		if (returnRules == null) {
			returnRules = this.create(new SessionBuildingRulesErrors(projectId, subjectId, selectionCriteria, rulesClass.getName()));
		}
		
		return returnRules;
	}
	
}
