package org.nrg.ccf.sessionbuilding.utils;

import java.util.List;
import org.nrg.ccf.common.utilities.utils.PojoUtils;
import org.nrg.ccf.sessionbuilding.pojo.ReportInfo;
import org.nrg.ccf.sessionbuilding.utils.CcfSessionBuildingReportUtils;
import org.nrg.xft.security.UserI;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
public class CcfSessionBuildingReportUtils {
	
	final static String REPORT_SQL = 
			"SELECT" + 
			"   RE.project_id as projectId," +
			"   SUBJ.label as subjectId," +
			"   RE.selection_criteria as selectionCriteria," +
			"   CASE" +
			"                WHEN rules_errors like '%HiRes%invalid or scan has not been rated%' THEN 'ScanNotRated_HiRes'" +
			"                WHEN rules_errors like '%invalid or scan has not been rated%' THEN 'ScanNotRated_Struc'" +
			"                WHEN rules_errors like '%match the shim%new fieldmap group has been chosen%' THEN 'ShimNotMatchFieldMap_NewChosen'" +
			"                WHEN rules_errors like '%match the shim%fieldmap group could not be chosen%' THEN 'ShimNotMatchFieldMap_NoMatch'" +
			"                WHEN rules_errors like '%fieldmap either%new fieldmap group has been chosen%' THEN 'FieldMapNotExist_NewChosen'" +
			"                WHEN rules_errors like '%fieldmap either%fieldmap group could not be chosen%' THEN 'FieldMapNotExist_NoMatch'" +
			"                WHEN rules_errors like '%Multiple matching scans slated for release%' THEN 'MultipleScans'" +
			"                WHEN rules_errors like '%diffusion scans%different ShimGroup values%' THEN 'DifferentShimValues_Dmri'" +
			"                WHEN rules_errors like '%different SeFieldmapGroup values%' THEN 'DifferentFieldMapGroups'" +
			"                WHEN rules_errors like '%Insufficient%frames%' THEN 'InsufficientFrames'" +
			"                WHEN rules_errors like '%contain a complete%cannot be overridden%' THEN 'NoT1T2'" +
			"                WHEN rules_errors like '%more than %Resting State%' THEN 'MultipeRestingState'" +
			"                WHEN rules_errors like '%No HiRes scan marked%' THEN 'NoHiRes'" +
			"                ELSE 'OTHER'" +
			"   END as rulesErrorCode," +
			"   rules_errors as rulesErrorDesc" +
			"   FROM xhbm_session_building_rules_errors_rules_errors REL" +
			"	LEFT JOIN" +
			"	xhbm_session_building_rules_errors RE" +
			"	ON REL.session_building_rules_errors = RE.id" +
			"	LEFT JOIN" +
			"	xnat_subjectdata SUBJ" +
			"	ON SUBJ.id = RE.subject_id" +
			"  WHERE re.project_id = '@@PROJECTID@@'" +	
			"  ORDER BY projectId, subjectId, rulesErrorCode" +
			"   ;"
			;

	
	public static List<ReportInfo> getErrorsReportTable(final String projectId, final UserI sessionUser, final JdbcTemplate jdbcTemplate) {
		
		final List<ReportInfo> reportInfoList = jdbcTemplate.query(REPORT_SQL.replace("@@PROJECTID@@", projectId),
				new BeanPropertyRowMapper<ReportInfo>(ReportInfo.class));
		return reportInfoList;
	}

	public static String getErrorsReportCSV(final String projectId,final UserI sessionUser,final JdbcTemplate jdbcTemplate) {
		
		final List<ReportInfo> reportList = getErrorsReportTable(projectId, sessionUser, jdbcTemplate); 
		if (reportList.size()<1) {
			return "";
		}
		return PojoUtils.pojoListToCsvStr(reportList);
        
    }
	
}
