package org.nrg.ccf.sessionbuilding.http;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipResponseBody implements StreamingResponseBody {
	
	File _directory;
    public static final String MEDIA_TYPE = "application/zip";

	public ZipResponseBody(File directory) {
		this._directory = directory;
	}

	@Override
	public void writeTo(OutputStream output) throws IOException {
        byte[] _buffer = new byte[1000*1024];
		final List<File> fileList = new ArrayList<>();
		if (_directory.isDirectory()) {
			fileList.addAll(FileUtils.listFiles(_directory, TrueFileFilter.TRUE, TrueFileFilter.TRUE));
		} else {
			fileList.add(_directory);
		}
        final ZipOutputStream zip = new ZipOutputStream(output);
        final URI dirUri = _directory.toURI();
        for (final File f : fileList) {
        	final ZipEntry entry = new ZipEntry(dirUri.relativize(f.toURI()).getPath());
            entry.setTime(f.lastModified());
            //long total = 0;
            zip.putNextEntry(entry);
            try (final InputStream input = new FileInputStream(f)) {
                int len;
				while ((len = input.read(_buffer)) > 0) {
                    zip.write(_buffer, 0, len);
                    //total += len;
                }
            }
            zip.closeEntry();
        }
        zip.close();
        output.flush();
	}

}
