package org.nrg.ccf.sessionbuilding.interfaces;


import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.pojo.ResourceValidationResults;

public interface CcfResourceGeneratorI {
	
	public ResourceValidationResults validateUnprocResources() throws ReleaseResourceException;
	
	public void generateResources() throws ReleaseResourceException;
	
}

