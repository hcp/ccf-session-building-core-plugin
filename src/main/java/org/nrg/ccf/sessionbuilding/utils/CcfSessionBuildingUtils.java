package org.nrg.ccf.sessionbuilding.utils;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.action.ClientException;
import org.nrg.ccf.sessionbuilding.entities.SessionBuildingRulesErrors;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.exception.SessionBuildingException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.pojo.ProcessingResults;
import org.nrg.ccf.sessionbuilding.services.SessionBuildingRulesErrorsService;
import org.nrg.ccf.sessionbuilding.utils.CcfSessionBuildingUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXReader;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXWriter;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CcfSessionBuildingUtils {
	
	// TODO:  The code here was pulled from other classes (SessionBuildingSubmitter and CcfSessionBuildingController).  It
	// would be good to refactor those classes to use this code if readily doable.  This utility was created to provide
	// so staging project sanity checkers could do a "rebuild" of the session from intake and create a session object
	// that could be compared to the staging project to find any changes in intake that may need to be applied to staging.
	
	private static final String SESSION_LABEL_PARAM = "sessionLabel";
	private static SessionBuildingRulesErrorsService _rulesErrorsService; 
	
	public static ProcessingResults processSubject(XnatProjectdata proj, XnatSubjectdata subj, CcfReleaseRulesI releaseRules,
			Map<String, String> params, UserI user) throws SessionBuildingException {
		
		final List<XnatMrsessiondata> sessionList = new ArrayList<>();
		final CriteriaCollection criteria=new CriteriaCollection("AND");
		final CriteriaCollection cc= new CriteriaCollection("OR");
		//// Currently we'll only retrieve MR session types
		cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME+"/project", proj.getId());
		cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME+"/sharing/share/project", proj.getId());
		criteria.addClause(cc);
		criteria.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/subject_ID", subj.getId());
		sessionList.addAll(XnatMrsessiondata.getXnatMrsessiondatasByField(criteria, user, false));
		final Iterator<XnatMrsessiondata> i = sessionList.iterator();
		while (i.hasNext()) {
			final XnatImagesessiondata session = i.next();
			if (!(session instanceof XnatMrsessiondata)) {
				i.remove();
			}
		}
		if (sessionList.size()<1) {
			final String errMsg = "ERROR:  No MR sessions found for this subject (PROJECT=" + proj.getId() + ",SUBJECT=" + subj.getLabel() + ").  " + 
					"Session building currently supports only MR session data.";
			throw new SessionBuildingException(errMsg);
		}
		final List<XnatMrsessiondata> filteredList = new ArrayList<>();
		try {
			filteredList.addAll((Collection<XnatMrsessiondata>) releaseRules.filterExptList(sessionList, params, user));
		} catch (ReleaseRulesException e) {
			log.error("ERROR",e);
		}
		final List<String> errorList = processSessionList(proj, subj, filteredList, params, releaseRules, user);
		return new ProcessingResults(errorList, filteredList);
		
	}
	
	public static List<XnatMrsessiondata> getFilteredExperimentList(XnatProjectdata proj, XnatSubjectdata subj, CcfReleaseRulesI releaseRules,
			Map<String, String> params, UserI user) throws SessionBuildingException, ReleaseRulesException {
		
		final List<XnatMrsessiondata> sessionList = new ArrayList<>();
		final CriteriaCollection criteria=new CriteriaCollection("AND");
		final CriteriaCollection cc= new CriteriaCollection("OR");
		//// Currently we'll only retrieve MR session types
		cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME+"/project", proj.getId());
		cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME+"/sharing/share/project", proj.getId());
		criteria.addClause(cc);
		criteria.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/subject_ID", subj.getId());
		sessionList.addAll(XnatMrsessiondata.getXnatMrsessiondatasByField(criteria, user, false));
		final Iterator<XnatMrsessiondata> i = sessionList.iterator();
		while (i.hasNext()) {
			final XnatImagesessiondata session = i.next();
			if (!(session instanceof XnatMrsessiondata)) {
				i.remove();
			}
		}
		if (sessionList.size()<1) {
			final String errMsg = "ERROR:  No MR sessions found for this subject (PROJECT=" + proj.getId() + ",SUBJECT=" + subj.getLabel() + ").  " + 
					"Session building currently supports only MR session data.";
			throw new SessionBuildingException(errMsg);
		}
		return releaseRules.filterExptList(sessionList, params, user);
		
	}

	public static List<String> processSessionList(XnatProjectdata proj, XnatSubjectdata subj, List<XnatMrsessiondata> sessionList,
			Map<String, String> params, CcfReleaseRulesI releaseRules, UserI user) throws SessionBuildingException {
		
		// First, create ordered list of scans with their associated sessions
		final List<XnatMrsessiondata> subjectSessions = sessionList;
		final List<XnatMrscandata> subjectScans = CcfReleaseRulesUtils.getOrderedScanList(sessionList, releaseRules);
		//log.debug("subjectSessions found=" + subjectSessions.size());
		//log.debug("subjectScans found=" + subjectScans.size());
		try {
			return processScans(proj, subj, params, subjectScans, subjectSessions, releaseRules, user);
		} catch (Exception e) {
			log.debug(ExceptionUtils.getFullStackTrace(e));
			throw new SessionBuildingException("Exception thrown processing scans:", e);
		}
	}
	
	private static List<String> processScans(XnatProjectdata proj, XnatSubjectdata subj, 
				Map<String, String> params, List<XnatMrscandata> subjectScans,
					List<XnatMrsessiondata> subjectSessions, CcfReleaseRulesI releaseRules, UserI user) throws Exception {
		final List<String> warningList;
		final Boolean overrideStatus = params.containsKey("errorOverride") && params.get("errorOverride").matches("(?i)^[TY].*$");
		final String selectionCriteria = params.get("selectionCriteria");
		try {
			if (overrideStatus) {
				warningList = releaseRules.applyRulesToScans(subjectScans,subjectSessions,params,true);
			} else {
				warningList = releaseRules.applyRulesToScans(subjectScans,subjectSessions,params,false);
			}
			persistRulesErrors(proj, subj, selectionCriteria, releaseRules, warningList);
		} catch (ReleaseRulesException e) {
			log.error("ReleaseRulesException thrown:  ", e);
			persistRulesErrors(proj, subj, selectionCriteria, releaseRules, e.getErrorList());
			throw e;
		} catch (Exception e) {
			log.error("RulesProcessor threw exception:  ");
			log.error(ExceptionUtils.getStackTrace(e));
			throw e;
		}
		return warningList;
	}

	
	public static XnatMrsessiondata buildAndReturnCombinedSession(XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, CcfReleaseRulesI releaseRules, List<XnatMrsessiondata> expts, UserI user) throws Exception {
		final XnatMrsessiondata combSess = new XnatMrsessiondata();
		if (params.containsKey("exptID")) {
			combSess.setId(params.get("exptID"));
		}
		if (params.containsKey("date")) {
			final String dateStr = params.get("date");
			try {
				parseAndSetDate(dateStr, combSess);
			} catch (Exception e) {
				combSess.setDate(null);

			}
		} else {
			combSess.setDate(null);
		}
		combSess.setDcmpatientname(CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj));
		// This doesn't seem to be used
		//combSess.setDcmpatientid(combSessLabel);
		combSess.setDcmpatientbirthdate(null);
		
		//boolean anyRelease = false;
		// Per meeting 2012-12-18, ordering all scans by acquisition time
		final List<XnatMrscandata> scanList = CcfReleaseRulesUtils.getOrderedScanList(expts, releaseRules);
		XnatMrsessiondata prevSess = null;
		String intraLabel = null;
		for (final XnatMrscandata intraScan : scanList) {
			final XnatMrsessiondata intraSess = getScanSession(intraScan, expts);
			if (intraLabel == null) {
				intraLabel = intraScan.getDbsession();
			}
			if (!(intraSess == null || intraSess.equals(prevSess))) {
				prevSess = intraSess;
				if (combSess.getAcquisitionSite() == null
						|| combSess.getAcquisitionSite().length() < 1) {
					combSess.setAcquisitionSite((String) getCombinedAttr(
							combSess.getAcquisitionSite(),
							intraSess.getAcquisitionSite(), "Acqusition Site"));
				}
				if (combSess.getScanner() == null
						|| combSess.getScanner().length() < 1) {
					combSess.setScanner((String) getCombinedAttr(
							combSess.getScanner(), intraSess.getScanner(),
							"Scanner"));
				}
				if (combSess.getOperator() == null
						|| combSess.getOperator().length() < 1) {
					combSess.setOperator((String) getCombinedAttr(
							combSess.getOperator(), intraSess.getOperator(),
							"Operator"));
				}
			}

			// TO BE SET LATER
			// combSess.setId()
			// whole data release
			// Should vary - leave blank
			// combSess.setDcmaccessionnumber()
			// combSess.setDcmpatientid()
			// combSess.setDcmpatientname()
			// combSess.setTime()
			// Not typically set - leave blank
			// combSess.setDuration()
			// HIPAA: Leave blank
			// combSess.setDcmpatientbirthdate()

			// continue only if targeted for export
			final Boolean isTarget = intraScan.getTargetforrelease();
			final Integer isOverride;
			if (!releaseRules.isStructuralScan(intraScan) || intraScan.getReleaseoverride()==null) {
				isOverride = 0;
			} else {
				isOverride = intraScan.getReleaseoverride();
			}
			// Override is currently only applicable for rated structural scans 
			// Newly added conditional for ACP project 2024/08/15, where we're releasing all structurals 
			if (!isAlternateScan(intraScan)) {
			 if (!((isOverride>0) || (isOverride>=0 && (isTarget!=null && isTarget)))) {
				continue;
			 }
			}
			//anyRelease = true;
			
			// Need to remove file objects from dbScan to permit upload (references Intradb file path)
			// These two lines are important, otherwise the scan will be MOVED.
			final XnatMrscandata dbScan = new XnatMrscandata((ItemI)intraScan.getItem().clone());
			if (dbScan.getFile().size()>0) {
				while (true) {
					try { 
						dbScan.removeFile(0);
					} catch (Exception e) {
						break;
					}
				}
			}
			try {
				dbScan.setId(dbScan.getDbid());
			} catch (NumberFormatException e) {
				throw new ClientException("ERROR:  Couldn't generate scan ID", e);
			}
			dbScan.setImageSessionId(combSess.getId());
			if (releaseRules.requireScanUidAnonymization()) {
				dbScan.setUid(CcfUidUtils.anonymizeUid(dbScan.getUid()));
			}
			// This seems to no longer be getting set
			//dbScan.setDatarelease(dataRelease);
			dbScan.setReleasecountscan(null);
			dbScan.setReleasecountscanoverride(null);
			dbScan.setTargetforrelease(null);
			dbScan.setReleaseoverride(null);
			dbScan.setDbsession(null);
			dbScan.setDbid(null);
			// Keep Intradb description for now
			final String idbDesc = dbScan.getSeriesDescription();
			dbScan.setSeriesDescription(dbScan.getDbdesc());
			dbScan.setDbdesc(idbDesc);
			dbScan.setType(dbScan.getDbtype());
			dbScan.setDbtype(null);
			// Per meeting, for now we won't populate scan notes
			dbScan.setNote(null);
			// We need to exclude all hidden fields, so let's convert to XML and back. 
			// TODO:  This could certainly be done more efficiently by removing the hidden fields from the item directly.
			final StringWriter sw = new StringWriter();
			SAXWriter writer = null;
			// I don't know why, but an exception is occasionally thrown getting the SAXWriter.  It's usually successful
			// on reruns of the same session.  Just putting it in a loop to try a few times before giving up.
			for (int i=0; i<5; i++) {
				try {
					writer = new SAXWriter(sw,false);
					break;
				} catch (Exception e) {
					log.debug("NOTE:  Exception thrown obtaining SAXWriter:  ", e);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException intE) {
						// Do nothing
					}
				}
			}
			if (writer == null) {
				throw new SessionBuildingException("Couldn't obtain SAXWriter");
			}
			writer.setWriteHiddenFields(false);
			writer.write(dbScan.getItem());
			final SAXReader reader = new SAXReader(user);
			final XFTItem newItem = reader.parse(new ByteArrayInputStream(sw.toString().getBytes()));
			sw.close();
			//sr.close();
			combSess.setScans_scan(newItem);
		}
		combSess.setLabel(params.containsKey(SESSION_LABEL_PARAM) ? params.get(SESSION_LABEL_PARAM) : releaseRules.getDefaultSessionLabel(proj, subj, params));
		return combSess;
	}
	
	private static boolean isAlternateScan(XnatMrscandata intraScan) {
		final String type = intraScan.getDbtype();
		final String desc = intraScan.getDbdesc();
		if (type == null || desc == null) {
			return false;
		}
		return (type.endsWith("_Alt") && desc.endsWith("_Alt"));
	}

	private static void parseAndSetDate(String dateStr, XnatSubjectassessordata sess) {
		// Per Sandy, 2013-01-17, do not set anything as a session date
		sess.setDate(null);
	}

	private static XnatMrsessiondata getScanSession(XnatMrscandata intraScan, List<XnatMrsessiondata> expts) {
		for (final XnatMrsessiondata expt : expts) {
			if (intraScan.getImageSessionId().equals(expt.getId())) {
				return (XnatMrsessiondata)expt;
			}
		}
		return null;
	}
	
	private static Object getCombinedAttr(Object combVal, Object mrVal,
			String attrString) {
		if (combVal == null) {
			combVal = mrVal;
		} else if (!combVal.equals(mrVal)) {
			final String returnString = "WARNING:  " + attrString
					+ " value differs between sessions (CURRENT="
					+ mrVal.toString() + ",PREVIOUS=" + combVal.toString()
					+ ").";
			log.warn(returnString);
		}
		return combVal;
	}
	
	public static void persistRulesErrors(XnatProjectdata proj, XnatSubjectdata subj, String selectionCriteria, CcfReleaseRulesI releaseRules, List<String> errorList) {
		persistRulesErrors(proj.getId(), subj.getId(), selectionCriteria, releaseRules.getClass(), errorList);
	}
	
	public static void persistRulesErrors(XnatProjectdata proj, XnatSubjectdata subj, String selectionCriteria, Class<? extends CcfReleaseRulesI> rulesClass, List<String> errorList) {
		persistRulesErrors(proj.getId(), subj.getId(), selectionCriteria, rulesClass, errorList);
	}
	
	public static void persistRulesErrors(String projectId, String subjectId, String selectionCriteria, CcfReleaseRulesI releaseRules, List<String> errorList) {
		persistRulesErrors(projectId, subjectId, selectionCriteria, releaseRules.getClass(), errorList);
	}
	
	public static void persistRulesErrors(String projectId, String subjectId, String selectionCriteria, Class<? extends CcfReleaseRulesI> rulesClass, List<String> errorList) {
		final SessionBuildingRulesErrors rulesErrors = 
				getRulesService().getSessionBuildingRulesErrorsOrCreateNew(projectId, subjectId, selectionCriteria, rulesClass);
		rulesErrors.getRulesErrors().clear();
		rulesErrors.getRulesErrors().addAll(errorList);
		_rulesErrorsService.update(rulesErrors);
	}
	
	private static SessionBuildingRulesErrorsService getRulesService() {
		if (_rulesErrorsService == null) {
			_rulesErrorsService = XDAT.getContextService().getBean(SessionBuildingRulesErrorsService.class);
		}
		return _rulesErrorsService;
	}

}
