package org.nrg.ccf.sessionbuilding.exception;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class ReleaseRulesException extends Exception{
	
	private static final long serialVersionUID = -2330121416551251652L;
	private static final Gson gson = new Gson();
	private List<String> _errorList = new ArrayList<>();

	public ReleaseRulesException(List<String> errorList,Throwable e){
		super((errorList!=null) ? gson.toJson(errorList) : null,e);
		if (errorList != null) {
			_errorList.addAll(errorList);
		}
	}
	
	public ReleaseRulesException(List<String> errorList){
		super((errorList!=null) ? gson.toJson(errorList) : null);
		if (errorList != null) {
			_errorList.addAll(errorList);
		}
	}

	public ReleaseRulesException(String msg,Throwable e){
		super(msg,e);
	}
	
	public ReleaseRulesException(String msg){
		super(msg);
	}
	
	public List<String> getErrorList() {
		return _errorList;
	}
	
}

