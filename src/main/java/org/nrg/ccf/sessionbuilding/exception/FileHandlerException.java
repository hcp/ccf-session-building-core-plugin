package org.nrg.ccf.sessionbuilding.exception;

public class FileHandlerException extends Exception{
	
	private static final long serialVersionUID = -8317054868410646532L;

	public FileHandlerException(String msg,Throwable e){
		super(msg,e);
	}
	
	public FileHandlerException(String msg){
		super(msg);
	}
	
}

