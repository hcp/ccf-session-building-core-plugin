package org.nrg.ccf.sessionbuilding.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.nrg.ccf.sessionbuilding.entities.SessionBuildingRulesErrors;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;

@Repository
public class SessionBuildingRulesErrorsDAO extends AbstractHibernateDAO<SessionBuildingRulesErrors> {
	
	public List<SessionBuildingRulesErrors> getSessionBuildingRulesErrorsForProject(String projectId, Class<? extends CcfReleaseRulesI> rulesClass) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("projectId",projectId));
		criteria.add(Restrictions.eq("rulesClass",rulesClass.getName()));
		@SuppressWarnings("unchecked")
		final List<SessionBuildingRulesErrors> rulesErrors = criteria.list();
		return rulesErrors;
	}
	
	public List<SessionBuildingRulesErrors> getSessionBuildingRulesErrorsForProject(String projectId, String selectionCriteria, Class<? extends CcfReleaseRulesI> rulesClass) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("projectId",projectId));
		criteria.add(Restrictions.eq("selectionCriteria",selectionCriteria));
		criteria.add(Restrictions.eq("rulesClass",rulesClass.getName()));
		@SuppressWarnings("unchecked")
		final List<SessionBuildingRulesErrors> rulesErrors = criteria.list();
		return rulesErrors;
	}
	
	public SessionBuildingRulesErrors getSessionBuildingRulesErrors(String projectId, String subjectId, String selectionCriteria, Class<? extends CcfReleaseRulesI> rulesClass) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("projectId",projectId));
		criteria.add(Restrictions.eq("subjectId",subjectId));
		criteria.add(Restrictions.eq("selectionCriteria",selectionCriteria));
		criteria.add(Restrictions.eq("rulesClass",rulesClass.getName()));
		final SessionBuildingRulesErrors rulesErrors = (SessionBuildingRulesErrors)criteria.uniqueResult();
		return rulesErrors;
	}
	
	public SessionBuildingRulesErrors getSessionBuildingRulesErrors(String projectId, String subjectId, String selectionCriteria, String rulesClass) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("projectId",projectId));
		criteria.add(Restrictions.eq("subjectId",subjectId));
		criteria.add(Restrictions.eq("selectionCriteria",selectionCriteria));
		criteria.add(Restrictions.eq("rulesClass",rulesClass));
		final SessionBuildingRulesErrors rulesErrors = (SessionBuildingRulesErrors)criteria.uniqueResult();
		return rulesErrors;
	}

}
