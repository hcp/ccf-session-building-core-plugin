package org.nrg.ccf.sessionbuilding.runner.result;

public class SessionBuilderRunResults {
	
	private Boolean success = false;
	private String status = "";
	
	public SessionBuilderRunResults() {
		super();
	}
	
	public SessionBuilderRunResults(Boolean success, String status) {
		super();
		this.success = success;
		this.status = status;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
