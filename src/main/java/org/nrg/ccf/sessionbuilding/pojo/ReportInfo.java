package org.nrg.ccf.sessionbuilding.pojo;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonPropertyOrder({ "projectId", "subjectId", "selectionCriteria", "rulesErrorCode", "rulesErrorDesc" })
public class ReportInfo {
	
	private String projectId;
	private String subjectId;
	private String selectionCriteria;
	//private String rulesClass;
	private String rulesErrorCode;
	private String rulesErrorDesc;
	
}