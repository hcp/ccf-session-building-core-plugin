package org.nrg.ccf.sessionbuilding.utils;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.nrg.ccf.sessionbuilding.constants.SessionBuildingConstants;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesReportException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.framework.constants.Scope;
import org.nrg.prefs.services.NrgPreferenceService;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatProjectparticipantI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.search.CriteriaCollection;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import au.com.bytecode.opencsv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CcfReleaseRulesUtils {
	
	final NrgPreferenceService _prefService;
	// TODO: Does this need to be configurable?  By Project?
	private static final String[] reportCheckFields = { "xnat:imageScanData/dbID", "xnat:imageScanData/dbDesc",
			"xnat:mrScanData/dbID", "xnat:mrScanData/dbDesc" };
	
	@Lazy
	@Autowired
	public CcfReleaseRulesUtils(final NrgPreferenceService prefService) {
		this._prefService = prefService;
	}
	
	public class ReleaseRulesResponse {
		
		public final CcfReleaseRulesI _releaseRules;
		public final ResponseEntity<String> _responseEntity;
		
		public ReleaseRulesResponse(CcfReleaseRulesI releaseRules, ResponseEntity<String> responseEntity) {
			this._releaseRules = releaseRules;
			this._responseEntity = responseEntity;
		}
		
		public ReleaseRulesResponse(CcfReleaseRulesI releaseRules) {
			this(releaseRules, null);
		}
		
		public ReleaseRulesResponse(ResponseEntity<String> responseEntity) {
			this(null, responseEntity);
		}
		
		public CcfReleaseRulesI getReleaseRules() {
			return _releaseRules;
		}
		
		public ResponseEntity<String> getResponseEntity() {
			return _responseEntity;
		}
		
	}
	
	public ReleaseRulesResponse getReleaseRulesForProject(final String projectId) {
		return getReleaseRulesForProject(projectId, null);
	}
	
	public ReleaseRulesResponse getReleaseRulesForProject(final String projectId, final String requestedRulesClass) {
		
		final String rulesClass = (requestedRulesClass!=null && requestedRulesClass.length()>0) ? requestedRulesClass :
				getRequestedRulesClassfromPreferences(projectId);
		if (rulesClass==null || rulesClass.length()<1) {
			return new ReleaseRulesResponse(new ResponseEntity<>("A release rules class has not been configured/specified for this project",
					HttpStatus.BAD_REQUEST));
		}
		try {
			final Object releaseRulesO = Class.forName(rulesClass).newInstance();
			final CcfReleaseRulesI releaseRules;
			if (releaseRulesO instanceof CcfReleaseRulesI) {
				releaseRules = (CcfReleaseRulesI) releaseRulesO;
				return new ReleaseRulesResponse(releaseRules);
			} else {
				return new ReleaseRulesResponse(new ResponseEntity<>("The release rules class specified in the configuration for this " + 
								"project is invalid (" + SessionBuildingConstants.PREFERENCE_VALUE_RULESCLASS + " is not an instance of " +
								"org.nrg.hcp.utils.ReleaseRulesI)", HttpStatus.BAD_REQUEST));
				
			}
		} catch (InstantiationException | IllegalAccessException e1) {
			return new ReleaseRulesResponse(new ResponseEntity<>("Could not instantiate release rules class (" + 
						SessionBuildingConstants.PREFERENCE_VALUE_RULESCLASS + ")", HttpStatus.INTERNAL_SERVER_ERROR));
		} catch (ClassNotFoundException e1) {
			return new ReleaseRulesResponse(new ResponseEntity<>("ClassNotFoundException - Could not find release rules class (" + 
					SessionBuildingConstants.PREFERENCE_VALUE_RULESCLASS + ")" , HttpStatus.INTERNAL_SERVER_ERROR));
		} catch (Exception e1) {
			return new ReleaseRulesResponse(new ResponseEntity<>("Exception - " + e1.toString()
							, HttpStatus.INTERNAL_SERVER_ERROR));
		}
		
	}
	
	public ReleaseRulesResponse getReleaseRulesForProject(final XnatProjectdata proj, final String requestedRulesClass) {
		return getReleaseRulesForProject(proj.getId(), requestedRulesClass);
	}

	private String getRequestedRulesClassfromPreferences(String projectId) {
		final Properties props = _prefService.getToolProperties(SessionBuildingConstants.TOOL_ID, Scope.Project, projectId);
		if (!(props.containsKey(SessionBuildingConstants.PREFERENCE_VALUE_ENABLED) &&
				props.containsKey(SessionBuildingConstants.PREFERENCE_VALUE_RULESCLASS) &&
				Boolean.valueOf(props.getProperty(SessionBuildingConstants.PREFERENCE_VALUE_ENABLED)).equals(true) &&
				SessionBuildingConstants.PREFERENCE_VALUE_RULESCLASS.length()>0)) {
			return null;
		}
		return props.getProperty(SessionBuildingConstants.PREFERENCE_VALUE_RULESCLASS);
	}

	public static String getSubjectLabelForProject(final XnatSubjectdata subj, final XnatProjectdata proj) {
		// TODO:  Implement configuration here to eliminate hardcoding
		if (subj.getProject().contains("_MDD_")) {
			// Special handling for MDD project
			String label = subj.getLabel();
			label = (label.length()>=5) ? label.toLowerCase().replaceAll("_", "").substring(0,5) : label.toLowerCase();
			return label;
		}
		// Is there a specified nda_subject_label for this subject?
		Object ndaLabelObj = subj.getFieldByName("nda_subject_label");
		if (ndaLabelObj != null && ndaLabelObj instanceof String && ((String)ndaLabelObj).length()>0) {
			return ndaLabelObj.toString();
		}
		if (subj.getProject().equals(proj.getId())) {
			return subj.getLabel();
		} else {
			for (final XnatProjectparticipantI pp : subj.getSharing_share()) {
				if (pp.getProject().equals(proj.getId())) {
					if (pp.getLabel()!=null && pp.getLabel().length()>0) {
						return pp.getLabel();
					}
				}
			}
		}
		return subj.getLabel();
	}

	public static XnatSubjectdata getSourceSubjectForProject(final XnatMrsessiondata stgSession, final XnatProjectdata sourceProj, final UserI user) {
		// From staging session, find the source project subject label
		final String stgSubjLabel = stgSession.getSubjectData().getLabel();
		XnatSubjectdata sourceSubject = XnatSubjectdata.GetSubjectByProjectIdentifier(sourceProj.getId(), stgSubjLabel, null, false);
		if (sourceSubject != null) { 
			return sourceSubject;
		}
		final org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
      	cc.addClause("xnat:subjectData.project",sourceProj.getId());
      	final List<XnatSubjectdata> subjects = XnatSubjectdata.getXnatSubjectdatasByField(cc, user, false);
      	for (final XnatSubjectdata subject : subjects) {
      		Object ndaLabelObj = subject.getFieldByName("nda_subject_label");
      		if (ndaLabelObj != null && ndaLabelObj instanceof String && ((String)ndaLabelObj).length()>0 && stgSubjLabel.equals(ndaLabelObj.toString())) {
      			return subject;
      		}
      	}
      	return null;
	}

	public static String getSourceSubjectLabelForProject(final XnatMrsessiondata stgSession, final XnatProjectdata sourceProj, final UserI user) {
		XnatSubjectdata subject = getSourceSubjectForProject(stgSession, sourceProj, user);
		return (subject != null) ? subject.toString() : null; 
	}
	
	public static List<XnatMrscandata> getOrderedScanList(final List<XnatMrsessiondata> expts, CcfReleaseRulesI releaseRules) {
		
		final Comparator<XnatImagescandata> scanCompare = releaseRules.getTemporalComparator(expts); 
		
		final List<XnatMrscandata> returnList = new ArrayList<>();
		for (final XnatImagesessiondata expt : expts) {
			if (expt instanceof XnatMrsessiondata) {
				final XnatMrsessiondata mrSess = (XnatMrsessiondata) expt;
				for (final XnatImagescandataI scanI : mrSess.getScans_scan()) {
					if (scanI instanceof XnatMrscandata) {
						returnList.add((XnatMrscandata)scanI);
					}
				}
			}
		}
		
		Collections.sort(returnList,scanCompare);
		return returnList;
	}
	
	public static String getReportCSV(final List<XnatMrsessiondata> subjectSessions, CcfReleaseRulesI releaseRules,
			final LinkedHashMap<String, String> rptColMap) throws ReleaseRulesReportException {
		return getReportCSV(getOrderedScanList(subjectSessions, releaseRules), subjectSessions, releaseRules, rptColMap, true);
	}
	
	public static String getReportCSV(final List<XnatMrsessiondata> subjectSessions, CcfReleaseRulesI releaseRules,
			final LinkedHashMap<String, String> rptColMap, boolean includeUnreleasedScans) throws ReleaseRulesReportException {
		return getReportCSV(getOrderedScanList(subjectSessions, releaseRules), subjectSessions, releaseRules, rptColMap, includeUnreleasedScans);
	}
	
	public static String getReportCSV(final List<XnatMrscandata> subjectScans, final List<XnatMrsessiondata> subjectSessions,
			final CcfReleaseRulesI releaseRules, final LinkedHashMap<String, String> rptColMap) throws ReleaseRulesReportException {
		return getReportCSV(subjectScans, subjectSessions, releaseRules, rptColMap, true);
	}
	
	public static String getReportCSV(final List<XnatMrscandata> subjectScans, final List<XnatMrsessiondata> subjectSessions,
			final CcfReleaseRulesI releaseRules, final LinkedHashMap<String, String> rptColMap, boolean includeUnreleasedScans) throws ReleaseRulesReportException {
		try {
			final StringWriter sw = new StringWriter();
			final CSVWriter csvWriter = new CSVWriter(sw, ',');
			csvWriter.writeAll(getReportList(subjectScans, subjectSessions, releaseRules,rptColMap, true, includeUnreleasedScans));
			csvWriter.close();
			sw.close();
			return sw.toString();
		} catch (IOException e) {
			throw new ReleaseRulesReportException("Exception occurred generating combiend session report", e);
		} 
		
	}
	
	public static String getReportJSON(final List<XnatMrscandata> subjectScans, final List<XnatMrsessiondata> subjectSessions,
			final CcfReleaseRulesI releaseRules, final LinkedHashMap<String, String> rptColMap) throws ReleaseRulesReportException {
		return getReportJSON(subjectScans, subjectSessions, releaseRules, rptColMap, true);
	}
	
	public static String getReportJSON(final List<XnatMrscandata> subjectScans, final List<XnatMrsessiondata> subjectSessions,
			final CcfReleaseRulesI releaseRules, final LinkedHashMap<String, String> rptColMap, boolean includeUnreleasedScans) throws ReleaseRulesReportException {
		final Gson gson = new Gson();
		return gson.toJson(getReportList(subjectScans, subjectSessions, releaseRules, rptColMap, false, includeUnreleasedScans));
	}
	
	public static List<String[]> getReportList(final List<XnatMrscandata> subjectScans, final List<XnatMrsessiondata> subjectSessions, CcfReleaseRulesI releaseRules,
			final LinkedHashMap<String, String> rptColMap, boolean formatForExcel, boolean includeUnreleasedScans) throws ReleaseRulesReportException {
		if (rptColMap.size()<1) {
			throw new ReleaseRulesReportException("ERROR:  No columns have been specified for the report.  See release rules for this project.");
		}
		final List<String[]> rptTableList  = new ArrayList<>();
		final List<String> rptHeaderList = new ArrayList<>();
		for (final String colKey : rptColMap.keySet()) {
			final String colHeader = rptColMap.get(colKey);
			rptHeaderList.add(colHeader);
		}
		rptTableList.add(rptHeaderList.toArray(new String[0]));
		boolean hasCheckValue = false;
		for (final XnatMrscandata scan : subjectScans) {
			if (!includeUnreleasedScans && !scan.getTargetforrelease()) {
				continue;
			}
			final List<String> rptRowList = new ArrayList<>();
			for (final String colKey : rptColMap.keySet()) {
				Object colVal = null;
				try {
					colVal = scan.getItem().getProperty(colKey);
					if (!hasCheckValue && colVal!= null && colVal.toString().length()>0 &&
							Arrays.asList(reportCheckFields).contains(colKey)) {
						hasCheckValue = true;
					}
				} catch (ElementNotFoundException | FieldNotFoundException e) {
					log.debug("SessionBuilding Report - Field/Element not found (EXPERIMENT=" + getScanSession(scan, subjectSessions).getLabel() +
							", SCAN=" + scan.getId() + ", FIELD=" + colKey + ")");
				} catch (Exception e) {
					throw new ReleaseRulesReportException("Exception occurred generating combiend session report", e);
				}
				rptRowList.add((colVal != null) ? ((formatForExcel) ? formatForExcel(colVal.toString()) : colVal.toString()) : "");
			}
			rptTableList.add(rptRowList.toArray(new String[0]));
		}
		if (!hasCheckValue) {
			throw new ReleaseRulesReportException("ERROR:  It appears that no combined session has been generated for this subject " +
					"using the specified rules class.");
		}
		return rptTableList;
	}

	private static XnatMrsessiondata getScanSession(XnatMrscandata scan, List<XnatMrsessiondata> subjectSessions) {
		for (final XnatMrsessiondata session : subjectSessions) {
			if (scan.getImageSessionId().equals(session.getId())) {
				return session;
			}
		}
		return null;
	}


	private static String formatForExcel(final String ins) {
		if (ins.startsWith("+") || ins.startsWith("-")) {
			return "'" + ins;
		}
		return ins;
	}
		
}
