package org.nrg.ccf.sessionbuilding.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CcfSessionBuildingExecUtils {
	
	private final static ExecutorService _executor = Executors.newCachedThreadPool();
	
	public static void execute(Runnable runnable) {
		_executor.execute(runnable);
	}
	
	public static Future<?> submit(Runnable runnable) {
		return _executor.submit(runnable);
	}
	
	public static ExecutorService getSingleUseExecutorService() {
		return Executors.newSingleThreadExecutor();
	}
}
