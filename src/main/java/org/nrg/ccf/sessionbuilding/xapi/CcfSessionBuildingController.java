package org.nrg.ccf.sessionbuilding.xapi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.io.FileUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;
import org.springframework.mail.MailSendException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXReader;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXWriter;
import org.nrg.action.ClientException;
import org.nrg.ccf.common.utilities.utils.MiscUtils;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.sessionbuilding.constants.SessionBuildingConstants.SessionBuildingReportFormats;
import org.nrg.ccf.sessionbuilding.enums.ProcessingType;
import org.nrg.ccf.sessionbuilding.exception.FileHandlerException;
import org.nrg.ccf.sessionbuilding.exception.ReleaseRulesException;
import org.nrg.ccf.sessionbuilding.exception.ResourceGeneratorException;
import org.nrg.ccf.sessionbuilding.exception.SessionBuildingException;
import org.nrg.ccf.sessionbuilding.http.ZipResponseBody;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseFileHandlerI;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.interfaces.CcfResourceGeneratorI;
import org.nrg.ccf.sessionbuilding.pojo.RulesProcessingResults;
import org.nrg.ccf.sessionbuilding.preferences.CcfSessionBuildingPreferences;
import org.nrg.ccf.sessionbuilding.runner.BuildSessionRunner;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils.ReleaseRulesResponse;
import org.nrg.ccf.sessionbuilding.utils.CcfSessionBuildingExecUtils;
import org.nrg.ccf.sessionbuilding.utils.CcfSessionBuildingUtils;
import org.nrg.ccf.sessionbuilding.utils.CcfUidUtils;
import org.nrg.ccf.sessionbuilding.xapi.http.StringStreamingResponseBody;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.mail.services.MailService;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;

import org.nrg.xdat.preferences.SiteConfigPreferences;
//import org.nrg.xdat.rest.AbstractXapiRestController;
import org.nrg.xdat.security.helpers.Permissions;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xft.utils.zip.ZipUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.xml.sax.SAXException;
import com.google.common.collect.Lists;
import com.google.gson.Gson;

/**
 *
 * @author Mike Hodge
 */

@XapiRestController
@Api(description = "CCF session building API")
public class CcfSessionBuildingController extends AbstractXapiRestController {
	
	public static final String SUBJECT_PARAM = "createOrShareSubj";
	public static final String SHARE_VALUE = "share";
	public static final String OVERWRITE_PARAM = "overwriteExisting";
	public static final String OVERRIDE_AS_WARNING_PARAM = "overrideAsWarning";
	public static final String RELEASE_RULES_PARAM = "rulesClass";
	public static final String RESOURCE_GEN_PARAM = "resourceGenerator";
	public static final String FILE_HANDLER_PARAM = "fileHandler";
	private static final String BUILD_PROJECT_PARAM = "buildProject";
	public static final String SESSION_LABEL_PARAM = "sessionLabel";
	public static final String SELECTION_CRITERIA_PARAM = "selectionCriteria";
	public static final String REPORT_FORMAT_PARAM = "reportFormat";
	public static final String PCP_SESSION_BUILDING_PIPELINE = "SessionBuilding";
	public static final String PCP_SELECTION_CRITERIA = "PCP";
	
	final CcfReleaseRulesUtils _rulesUtils;
	final CcfSessionBuildingPreferences _preferences;
	private final SiteConfigPreferences _sitePreferences;
	// TODO:  Do we want to make this configurable?
	private int MAX_CONCURRENT_BUILDS = 5;
	private MailService _mailService;
	private final PcpStatusEntityService _statusEntityService;
	//private static final SimpleDateFormat timeformatter = new SimpleDateFormat("HH:mm:ss");
	//private static final SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd");
	private static final Logger _logger = LoggerFactory.getLogger(CcfSessionBuildingController.class);
	
	private static final List<String> currentlyBuildingList = new ArrayList<>();

	@Lazy
	@Autowired
	public CcfSessionBuildingController(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
				final CcfSessionBuildingPreferences preferences, CcfReleaseRulesUtils rulesUtils,
				final SiteConfigPreferences sitePreferences, final MailService mailService,
				final PcpStatusEntityService statusEntityService) { 
		super(userManagementService, roleHolder);
		this._preferences = preferences;
		this._sitePreferences = sitePreferences;
		this._rulesUtils = rulesUtils;
		this._mailService = mailService;
		this._statusEntityService = statusEntityService;
	}
	
	@ApiOperation(value = "Builds combined session XML for a subject", response = String.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/subjects/{subjectId}/ccfSessionBuilding/buildSession",
					produces = {MediaType.TEXT_PLAIN_VALUE}, /* headers={"Cache-Control = must-revalidate"}, */ method = RequestMethod.POST)
	public ResponseEntity<String> buildSession(@PathVariable("projectId") String projectId, @PathVariable("subjectId") String subjectId, @RequestParam Map<String, String> params) {
		//if (!params.containsKey)
		ResponseEntity<RulesProcessingResults> setScanValuesResponse = null;
		if (params.get(SELECTION_CRITERIA_PARAM).equals(PCP_SELECTION_CRITERIA)) {
			return ResponseEntity.badRequest().body(
				"ERROR:  The PCP SelectionCriteria is not yet supported for this API.  It's only supported for project-level session reports");
		}
	 	if (!(params.containsKey("skipRulesProcessing") && params.get("skipRulesProcessing").matches("(?i)^[TY].*$"))) {
	 		setScanValuesResponse = processScanValueRequest(projectId, subjectId, params, getSessionUser());
	 		if (!setScanValuesResponse.getStatusCode().equals(HttpStatus.OK)) {
	 			return new ResponseEntity<String>(setScanValuesResponse.getBody().getStatus(),setScanValuesResponse.getStatusCode());
	 		} 
	 	}
		return processUserRequest(projectId, subjectId, params, ProcessingType.BUILD_SESSION, getSessionUser(), setScanValuesResponse);
	}
	
	@ApiOperation(value = "Transfer session files for a subject", response = String.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/subjects/{subjectId}/ccfSessionBuilding/transferFiles",
					produces = {MediaType.TEXT_PLAIN_VALUE}, /* headers={"Cache-Control = must-revalidate"}, */ method = RequestMethod.POST)
	public ResponseEntity<String> transferFiles(@PathVariable("projectId") String projectId, @PathVariable("subjectId") String subjectId, @RequestParam Map<String, String> params) {
		if (params.get(SELECTION_CRITERIA_PARAM).equals(PCP_SELECTION_CRITERIA)) {
			return ResponseEntity.badRequest().body(
				"ERROR:  The PCP SelectionCriteria is not yet supported for this API.  It's only supported for project-level session reports");
		}
		return processUserRequest(projectId, subjectId, params, ProcessingType.FILE_TRANSFER, getSessionUser(), null);
	}
	
	@ApiOperation(value = "Returns combined session XML for a subject", response = String.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/subjects/{subjectId}/ccfSessionBuilding/getSessionXml",
					produces = {MediaType.APPLICATION_XML_VALUE}, /* headers={"Cache-Control = must-revalidate"}, */ method = RequestMethod.GET)
	public ResponseEntity<String> getSessionXml(@PathVariable("projectId") String projectId, @PathVariable("subjectId") String subjectId, @RequestParam Map<String, String> params) {
		if (params.get(SELECTION_CRITERIA_PARAM).equals(PCP_SELECTION_CRITERIA)) {
			return ResponseEntity.badRequest().body(
				"ERROR:  The PCP SelectionCriteria is not yet supported for this API.  It's only supported for project-level session reports");
		}
		return processUserRequest(projectId, subjectId, params, ProcessingType.GET_XML, getSessionUser(), null);
	}
	
	@ApiOperation(value = "Returns combined session XML for all subjects", response = Void.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/ccfSessionBuilding/getSessionBuildingReport",
					produces = ZipResponseBody.MEDIA_TYPE,   method = RequestMethod.GET)
	public ResponseEntity<Void> getSessionReports(final @PathVariable("projectId") String projectId, 
			final @RequestParam Map<String, String> params) throws IOException {
		// Convert response entity based on reportFormat
		final String buildProject = _preferences.getCcfSessionBuildingBuildProject(projectId);
        final CriteriaCollection cc = new CriteriaCollection("OR");
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", buildProject);
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", buildProject);
		final List<XnatMrsessiondata> exps=XnatMrsessiondata.getXnatMrsessiondatasByField(cc, getSessionUser(), false);
		final List<String> buildSubjLbls = new ArrayList<>();
		for (final XnatMrsessiondata sess : exps) {
			final String subjLbl = sess.getSubjectData().getLabel();
			if (!buildSubjLbls.contains(subjLbl)) {
				buildSubjLbls.add(subjLbl);
			}
		}
		final List<String> subjLbls = new ArrayList<>();
		final Map<String,String> subjLblMap = new HashMap<>();
        final CriteriaCollection srcCC = new CriteriaCollection("OR");
        srcCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", projectId);
        srcCC.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", projectId);
		final List<XnatMrsessiondata> srcExps=XnatMrsessiondata.getXnatMrsessiondatasByField(srcCC, getSessionUser(), false);
		for (final XnatMrsessiondata sess : srcExps) {
			final String subjLbl = sess.getSubjectData().getLabel();
			final XnatSubjectdata subject = sess.getSubjectData();
			final String destSubjLbl = CcfReleaseRulesUtils.getSubjectLabelForProject(subject, subject.getPrimaryProject(false));
			if (buildSubjLbls.contains(destSubjLbl) && !subjLblMap.values().contains(destSubjLbl) && !subjLbls.contains(subjLbl)) {
				subjLbls.add(subjLbl);
				subjLblMap.put(subjLbl,destSubjLbl);
			}
		}
		final ExecutorService executor = CcfSessionBuildingExecUtils.getSingleUseExecutorService();
		final String adminEmail = _sitePreferences.getAdminEmail();
		final UserI user = getSessionUser();
		final String userName = user.getUsername();
		final String userFirstname = user.getFirstname();
		final String userLastname = user.getLastname();
		final String userEmail = user.getEmail();
		_logger.error("USER INFO:  " + adminEmail + " - " + userName + " - " + userFirstname + " - " + userLastname + " - " + userEmail);
		executor.execute(
				new Runnable() {
					@Override
					public void run() {
						_logger.debug("Begin process execution");
						File tempDirFile = null;
						File zipDirFile = null;
						try {
							Path tempDir = Files.createTempDirectory("CCFCSVS");
							Path zipDir = Files.createTempDirectory("CCFCSVS");
							tempDirFile = tempDir.toFile();
							zipDirFile = zipDir.toFile();
							final String selectionCriteria = params.get(SELECTION_CRITERIA_PARAM); 
							for (final String subjLbl : subjLbls) {
								final ArrayList<String> selectionList = new ArrayList<>();
								if (!selectionCriteria.equals(PCP_SELECTION_CRITERIA)) {
									selectionList.add(selectionCriteria);
								} else {
									final List<PcpStatusEntity> entities = _statusEntityService.getStatusEntities(projectId, PCP_SESSION_BUILDING_PIPELINE, subjLbl);
									for (final PcpStatusEntity entity : entities) {
											selectionList.add(entity.getSubGroup());
									}
								}
								for (final String sc : selectionList) {
									params.put(SELECTION_CRITERIA_PARAM, sc);
									try {
										final String currentSelection = params.get(SELECTION_CRITERIA_PARAM);
										final ResponseEntity<String> stringResponse = processUserRequest(projectId, subjLbl, params, ProcessingType.GET_REPORT, user, null);
										if (stringResponse.getStatusCode().is2xxSuccessful()) {
											final String subjCsv = stringResponse.getBody();
											FileUtils.writeStringToFile(new File(tempDirFile,subjLblMap.get(subjLbl) +
											((currentSelection != null && currentSelection.length()>0) ? "_" + currentSelection : "") +
											"_report.csv"),subjCsv,StandardCharsets.ISO_8859_1);
										}
									} catch (IOException e1) {
										_logger.error("Exception building session report for subject" + subjLbl + ":",e1);
									}
								}
							}
							final File zipFile = new File(zipDirFile,projectId + "_sessionCSVs" +
									((selectionCriteria != null && selectionCriteria.length()>0) ?
											"_" + selectionCriteria : "") + ".zip");
							zipFile.createNewFile();
							final ZipUtils zipUtils = new ZipUtils();
							final OutputStream os = new FileOutputStream(zipFile);
							zipUtils.setOutputStream(os,ZipUtils.DEFAULT_COMPRESSION);
							Iterator<File> iter = FileUtils.iterateFiles(tempDirFile, null, true);
							while (iter.hasNext()) {
								File f = iter.next();
								zipUtils.write(f.getName(), f);
							}
							zipUtils.close();
							final Map<String,File> attachmentMap = new HashMap<>();
							attachmentMap.put(zipFile.getName(), zipFile);
							_mailService.sendHtmlMessage(adminEmail, new String[] {userEmail}, null, null, 
									"All Subjects Session Building CSV Report (" + projectId +
											((selectionCriteria != null && selectionCriteria.length()>0) ?
													", " + selectionCriteria + ")" : ")")
									, 
									"Dear " + userFirstname + " " + userLastname + 
									",<br><br> Your session building CSV reports for project " + projectId + " are attached.<br><br>" +
											"Regards,<br><br>IntraDB Administrator" 
									, null, attachmentMap);
							try {
								if (tempDirFile != null) {
									TimeUnit.MINUTES.sleep(5);
									FileUtils.deleteDirectory(tempDirFile);
									if (zipDirFile != null) {
										FileUtils.deleteDirectory(zipDirFile);
									}
								}
							} catch (IOException | InterruptedException e) {
								// Do nothing
							}
						} catch (MessagingException|MailSendException me) {
							_logger.error("ERROR: Error sending combined session report to " + userName + ":", me);
							try {
								_mailService.sendHtmlMessage(adminEmail, new String[] {userEmail}, null, null, 
										"Failed to send All Subjects Session Building CSV Report"
										, 
										"Dear " + userFirstname + " " + userLastname + 
										",<br><br> The session building CSV reports for project " + projectId + " failed to send.  This " +
												" is likely because the report size exceeded the size allowed by the mail server.  Please " +
												"contact your IntraDB Administrator, who may be able to retrieve the file and get it to " +
												"you.<br><br>TempDirectory:  " + 
												((tempDirFile!=null) ? tempDirFile.getAbsolutePath() : "null") + "<br><br>" +
												"Regards,<br><br>IntraDB Administrator" 
										, null, null);
							} catch (MessagingException|MailSendException e) {
								_logger.error("ERROR: Also failed to send 'failure' message to " + userName + ":", me);
							}
						} catch (Exception e) {
							try {
								_mailService.sendHtmlMessage(adminEmail, new String[] {userEmail}, null, null, 
										"Error generating session building CSV report", 
										"Dear " + userFirstname + " " + userLastname + 
										",<br><br>There was an error building your session CSV report.  See exception trace below.<br><br>" +
												"Regards,<br><br>IntraDB Administrator<br><br><b>Exception trace:</b><br><br>" +  
												ExceptionUtils.getFullStackTrace(e)
										, null);
							} catch (MessagingException|MailSendException me) {
								_logger.error("ERROR: Error sending combined session report to " + userName + ":", me);
							}
							_logger.error("ERROR: Error generating combined session report for " + userName + ":", e);
						} 
						_logger.debug("End process execution");
					}
				}
		);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value = "Returns combined session XML for a subject", response = StringStreamingResponseBody.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/subjects/{subjectId}/ccfSessionBuilding/getSessionBuildingReport",
					/*produces = {StringStreamingResponseBody.MEDIA_TYPE}, */  method = RequestMethod.GET)
	public ResponseEntity<StringStreamingResponseBody> getSessionReport(@PathVariable("projectId") String projectId, @PathVariable("subjectId") String subjectId, @RequestParam Map<String, String> params) {
		// Convert response entity based on reportFormat
		final XnatSubjectdata subj = XnatSubjectdata.getXnatSubjectdatasById(subjectId, getSessionUser(), false);
		final String subjLbl = (subj!=null) ? subj.getLabel() : subjectId;
		
		final String selectionCriteria = params.get(SELECTION_CRITERIA_PARAM); 
		if (selectionCriteria.equals(PCP_SELECTION_CRITERIA)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(HttpHeaders.CONTENT_TYPE, StringStreamingResponseBody.MEDIA_TYPE_TEXT_PLAIN)
					.body(new StringStreamingResponseBody("ERROR:  The PCP SelectionCriteria is not yet supported for this API.  It's only supported for project-level session reports"));
		}
		final ResponseEntity<String> stringResponse = processUserRequest(projectId, subjectId, params, ProcessingType.GET_REPORT, getSessionUser(), null);
		if (!stringResponse.getStatusCode().equals(HttpStatus.OK)) {
			return ResponseEntity.status(stringResponse.getStatusCode()).header(HttpHeaders.CONTENT_TYPE, StringStreamingResponseBody.MEDIA_TYPE_TEXT_PLAIN)
					.body(new StringStreamingResponseBody(stringResponse.getBody()));
		} else if (params.containsKey(REPORT_FORMAT_PARAM) && params.get(REPORT_FORMAT_PARAM).equalsIgnoreCase("CSV")) {
			return ResponseEntity.status(stringResponse.getStatusCode()).header(HttpHeaders.CONTENT_TYPE, StringStreamingResponseBody.MEDIA_TYPE_TEXT_CSV)
					.header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=" + subjLbl +
							((selectionCriteria != null && selectionCriteria.length()>0) ? "_" + selectionCriteria : "") +
							"_report.csv")
					.body(new StringStreamingResponseBody(stringResponse.getBody()));
		} else {
			return ResponseEntity.status(stringResponse.getStatusCode()).header(HttpHeaders.CONTENT_TYPE, StringStreamingResponseBody.MEDIA_TYPE_APPLICATION_JSON)
					.body(new StringStreamingResponseBody(stringResponse.getBody()));
		}
	}
	
	@ApiOperation(value = "Sets combined session scan-level values for subject sessions", response = String.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/subjects/{subjectId}/ccfSessionBuilding/setScanValues", method = RequestMethod.POST)
	public ResponseEntity<RulesProcessingResults> setScanValues(@PathVariable("projectId") String projectId, @PathVariable("subjectId") String subjectId, @RequestParam Map<String, String> params) {
		return processScanValueRequest(projectId, subjectId, params, getSessionUser());
	}
	
	@ApiOperation(value = "Clear currently building list", response = Void.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/ccfSessionBuilding/clearCurrentlyBuildingList", method = RequestMethod.POST)
	public ResponseEntity<Void> clearCurrentlyBuildingList() {
		currentlyBuildingList.clear();
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value = "Return currently building list", response = List.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/ccfSessionBuilding/clearCurrentlyBuildingList", produces = MediaType.APPLICATION_JSON_VALUE,  method = RequestMethod.GET)
	public ResponseEntity<List<String>> returnCurrentlyBuildingList() {
		return new ResponseEntity<List<String>>(currentlyBuildingList,HttpStatus.OK);
	}
	
	private ResponseEntity<String> processUserRequest(String projectId, String subjectId, Map<String, String> params, ProcessingType processingType, UserI user, ResponseEntity<RulesProcessingResults> setScanValuesResponse) {
		
		XnatProjectdata proj=null;
		XnatSubjectdata subj=null;
		
		if (projectId != null) {
			proj = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
		}
		
		if (proj == null) {
			return new ResponseEntity<>("Project not specified or does not exist (PROJECT_ID=" + projectId + ")", HttpStatus.BAD_REQUEST);
		}
		
		final ReleaseRulesResponse rulesResponse = _rulesUtils.getReleaseRulesForProject(proj,params.get(RELEASE_RULES_PARAM));
		final CcfReleaseRulesI releaseRules = (rulesResponse!=null) ? rulesResponse.getReleaseRules() : null;
		if (releaseRules == null) {
			return rulesResponse.getResponseEntity();
		}

		subj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjectId, user, true);

		if (subj == null) {
			subj = XnatSubjectdata.getXnatSubjectdatasById(subjectId, user, true);
			if (subj != null
					&& (proj != null && !subj.hasProject(proj.getId()))) {
				subj = null;
			}
		}

		if (subj == null) {
			return new ResponseEntity<>("Subject not specified or does not exist (SUBJECT_ID=" + subjectId + ")", HttpStatus.BAD_REQUEST);
		}
		
		try {
			if (!Permissions.canRead(user,subj)) {
				return new ResponseEntity<>("Specified user account has insufficient privileges for subjects in this project.", HttpStatus.FORBIDDEN);
			}
		} catch (Exception e) {
			_logger.error("SERVER EXCEPTION:  " + ExceptionUtils.getStackTrace(e));
			return new ResponseEntity<>("Server threw exception:  " + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		final String combSessProjId = getRequestedOrConfiguredBuildProject(proj,params.get(BUILD_PROJECT_PARAM));
		
		try {
			if (!Permissions.canRead(user,  proj)) {
				return new ResponseEntity<>("The destination project for this session either does not exist or your account doesn't have access to it (PROJECT=" +
						combSessProjId + ").", HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e1) {
			// Do nothing, we'll just try to continue.  Process will fail if user doesn't have appropriate access.
		}
		
		try {
			if (processingType.equals(ProcessingType.GET_XML)) {
				return new ResponseEntity<>(getMrSessionXml(proj, subj, params, releaseRules, user), HttpStatus.OK);
			} else if (processingType.equals(ProcessingType.BUILD_SESSION)) {
				return continueProcessing(proj, subj, params, releaseRules, combSessProjId, user, processingType, setScanValuesResponse);
			} else if (processingType.equals(ProcessingType.FILE_TRANSFER)) {
				return continueProcessing(proj, subj, params, releaseRules, combSessProjId, user, processingType, null);
			} else if (processingType.equals(ProcessingType.GET_REPORT)) {
				return getSessionReport(proj, subj, params, releaseRules, user);
			} else {
				return new ResponseEntity<>(getMrSessionXml(proj, subj, params, releaseRules, user), HttpStatus.OK);
			}
		} catch (Exception e) {
			_logger.error("SERVER EXCEPTION:  " + ExceptionUtils.getStackTrace(e));
			return new ResponseEntity<>(e.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
		}

	}
	
	private ResponseEntity<String> continueProcessing(XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, CcfReleaseRulesI releaseRules, 
			String combSessProjId, UserI user, ProcessingType processingType, ResponseEntity<RulesProcessingResults> setScanValuesResponse) {
		
		try {
			if (!Permissions.canEdit(user,  proj)) {
				return new ResponseEntity<>("Your account does not have write permission on the destination project (PROJECT=" +
						combSessProjId + ").", HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e1) {
			// Do nothing, we'll just try to build the session.  It will fail if the user doesn't have access.
		}
		
		if (currentlyBuildingList.contains(subj.getId())) {
				return new ResponseEntity<>("Session building is currently running for this subject (SUBJECT=" + 
						CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + "). " +
						"Only one session building process can run at a time for a subject.", HttpStatus.BAD_REQUEST);
		}
		if (currentlyBuildingList.size()>MAX_CONCURRENT_BUILDS) {
				return new ResponseEntity<>("The session builder is currently running the maximum number of build processes (" + 
						MAX_CONCURRENT_BUILDS + ").  Note that this may include session building for other projects.  " +
						"Please try your request again later.", HttpStatus.BAD_REQUEST);
		}
		currentlyBuildingList.add(subj.getId());
		try {
			final ResponseEntity<String> response = doProcess(proj, subj, params, releaseRules, user, processingType, setScanValuesResponse);
			if (!response.getStatusCode().equals(HttpStatus.OK)) {
				currentlyBuildingList.remove(subj.getId());
			}
			return response;
		} catch (Throwable e) {
			currentlyBuildingList.remove(subj.getId());
			throw e;
		}
	}
	
	private ResponseEntity<String> doProcess(XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, 
			CcfReleaseRulesI releaseRules, UserI user, ProcessingType processingType, ResponseEntity<RulesProcessingResults> setScanValuesResponse) {
	
		final List<XnatMrsessiondata> expts = new ArrayList<>(); 
		try {
			ArrayList<XnatMrsessiondata> subjSessions = new ArrayList<>();
			for (final XnatSubjectassessordata assessor : subj.getExperiments_experiment(XnatMrsessiondata.SCHEMA_ELEMENT_NAME)) {
				subjSessions.add((XnatMrsessiondata)assessor);
			}
			expts.addAll(releaseRules.filterExptList(getProjectExpts(proj, subjSessions),params,user));
		} catch (ReleaseRulesException e) {
			_logger.error("Could not retrieve sessions from release rules: ", e);
			return new ResponseEntity<>("ERROR: Could not retrieve sessions from release rules: " + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		XnatMrsessiondata combSess = null;
		try {
			if (processingType.equals(ProcessingType.BUILD_SESSION)) {
				combSess = buildAndReturnCombinedSession(proj, subj, params, releaseRules, expts, user);
				if (combSess == null) {
					throw new ReleaseRulesException("Could not build combined session - null session returned");
				}
			} else {  // FILE_TRANSFER
				final String buildProjectParam = params.get(BUILD_PROJECT_PARAM);
				final String sessionLabelParam = params.get(SESSION_LABEL_PARAM);
				final List<XnatMrsessiondata> currentSessions = XnatMrsessiondata.getXnatMrsessiondatasByField("xnat:mrSessionData/label",sessionLabelParam, user, false);
				for (final XnatMrsessiondata currentSession : currentSessions) {
					if (currentSession.getProject().equals(buildProjectParam)) {
						combSess = currentSession;
						break;
					}
				}
				if (combSess == null) {
					throw new ReleaseRulesException("Could not obtain session for file transfer");
				}
			}
		} catch (Exception e) {
			if (processingType.equals(ProcessingType.BUILD_SESSION)) {
				_logger.error("Could not build combined session: ", e);
				return new ResponseEntity<>("ERROR: Could not build session: " + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
			} else { // FILE_TRANSFER
				_logger.error("Could not obtain session for file transfer: ", e);
				return new ResponseEntity<>("ERROR: Could not obtain session for file transfer: " + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		CcfReleaseFileHandlerI fileHandler;
		try {
			fileHandler = getRequestedOrConfiguredFileHandler(proj,expts,combSess,releaseRules,params.get(FILE_HANDLER_PARAM),user); 
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException | FileHandlerException e) {
			_logger.error("Could not retrieve file handler: ", e);
			return new ResponseEntity<>("ERROR: Could not instantiate file handler: " + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (fileHandler==null) {
			return new ResponseEntity<>("ERROR: Could not instantiate file handler", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		final CcfResourceGeneratorI resourceGenerator;
		try {
			resourceGenerator = getRequestedOrConfiguredResourceGenerator(proj,expts,combSess,releaseRules,params.get(RESOURCE_GEN_PARAM),user);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException | ResourceGeneratorException e) {
			_logger.error("Could not instansiate unprocessed resource generator: ", e);
			return new ResponseEntity<>("ERROR: Could not instantiate unprocessed resource generator: " + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (resourceGenerator==null) {
			return new ResponseEntity<>("ERROR: Could not instantiate unprocessed resource generator", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		final String combSessProjId = getRequestedOrConfiguredBuildProject(proj,params.get(BUILD_PROJECT_PARAM));
		if (combSessProjId == null) {
			return new ResponseEntity<>("ERROR: No project has been specified or configured to store the session", HttpStatus.BAD_REQUEST);
		}
		
		combSess.setProject(combSessProjId);
		final BuildSessionRunner runner = new BuildSessionRunner(subj, combSess, user, fileHandler, 
				resourceGenerator, params, currentlyBuildingList, _sitePreferences.getSiteUrl(), processingType, 
				(setScanValuesResponse!=null) ? setScanValuesResponse.getBody() : null);
		Thread buildThread = new Thread(runner);
		buildThread.start();
		
	 	if (!(params.containsKey("skipRulesProcessing") && params.get("skipRulesProcessing").matches("(?i)^[TY].*$"))) {
	 		return new ResponseEntity<>("The rules checks and validation checks have passed for this session (SUBJECT=" + 
	 				CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + 
	 				((processingType.equals(ProcessingType.BUILD_SESSION)) ?
	 						").  The session has been submitted to the session builder.  You will receive an e-mail when session building is complete." :
	 							").  The session has been submitted to the file transfer handler.  You will receive an e-mail when transfer is complete."
					)
	 				, HttpStatus.OK);
	 	} else {
	 		return new ResponseEntity<>("Rules processing and validation checks <em>have been skipped for this session</em>, " +
	 				" per request (SUBJECT=" + 
	 				CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj) + 
	 				((processingType.equals(ProcessingType.BUILD_SESSION)) ?
	 						").  The session has been submitted to the session builder.  You will receive an e-mail when session building is complete." :
	 							").  The session has been submitted to the file transfer handler.  You will receive an e-mail when transfer is complete."
					)
	 				, HttpStatus.OK);
	 	} 

	}
	
	private String getRequestedOrConfiguredBuildProject(XnatProjectdata proj, String buildProject) {
		if (buildProject!=null && buildProject.length()>0) {
			return buildProject;
		}
		return _preferences.getCcfSessionBuildingBuildProject(proj.getId());
	}

	private CcfResourceGeneratorI getRequestedOrConfiguredResourceGenerator(XnatProjectdata proj,
			List<XnatMrsessiondata> expts, XnatMrsessiondata combSess, CcfReleaseRulesI releaseRules, String requestedResourceGenerator, UserI user) 
					throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
						SecurityException, ClassNotFoundException, ResourceGeneratorException {
		String genClassStr;
		if (requestedResourceGenerator != null && requestedResourceGenerator.length()>0) {
			if (!_preferences.getCcfSessionBuildingSelectableResourceGen(proj.getId()).contains(requestedResourceGenerator)) {
				throw new ResourceGeneratorException("ERROR:  The requested resource generator is not allowed for this project"); 
			}
			genClassStr = requestedResourceGenerator;
		} else {
			genClassStr = _preferences.getCcfSessionBuildingResourceGen(proj.getId());
		}
		return (CcfResourceGeneratorI) Class.forName(genClassStr)
				.getDeclaredConstructor(List.class, XnatImagesessiondata.class, CcfReleaseRulesI.class, UserI.class)
				.newInstance(expts, combSess, releaseRules, user);
	}

	private CcfReleaseFileHandlerI getRequestedOrConfiguredFileHandler(XnatProjectdata proj,
			List<XnatMrsessiondata> expts, XnatMrsessiondata combSess, CcfReleaseRulesI releaseRules, String requestedFileHandler, UserI user)
					throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
						SecurityException, ClassNotFoundException, FileHandlerException {
		
		String fileHandlerStr;
		if (requestedFileHandler != null && requestedFileHandler.length()>0) {
			if (!_preferences.getCcfSessionBuildingSelectableFileHandlers(proj.getId()).contains(requestedFileHandler)) {
				throw new FileHandlerException("ERROR:  The requested file handler is not allowed for this project"); 
			}
			fileHandlerStr = requestedFileHandler;
		} else {
			fileHandlerStr = _preferences.getCcfSessionBuildingFileHandler(proj.getId());
		}
		return	(CcfReleaseFileHandlerI) Class.forName(fileHandlerStr)
				.getDeclaredConstructor(List.class, XnatImagesessiondata.class, CcfReleaseRulesI.class, UserI.class)
				.newInstance(expts, combSess, releaseRules, user);
	}

	private String getMrSessionXml(XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, CcfReleaseRulesI releaseRules, UserI user) throws Exception {
		final List<XnatMrsessiondata> expts = new ArrayList<>(); 
		try {
			ArrayList<XnatMrsessiondata> subjSessions = new ArrayList<>();
			for (final XnatSubjectassessordata assessor : subj.getExperiments_experiment(XnatMrsessiondata.SCHEMA_ELEMENT_NAME)) {
				subjSessions.add((XnatMrsessiondata)assessor);
			}
			expts.addAll(releaseRules.filterExptList(getProjectExpts(proj, subjSessions),params,user));
		} catch (ReleaseRulesException e) {
			_logger.error("Could not retrieve sessions from release rules: ", e);
		}
		final XnatImagesessiondata combSess = buildAndReturnCombinedSession(proj, subj, params, releaseRules, expts, user);
		return itemToXmlString(combSess.getItem());
		
	}

	private ResponseEntity<String> getSessionReport(XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, CcfReleaseRulesI releaseRules, UserI user) throws Exception {
		final List<XnatMrsessiondata> expts = new ArrayList<>(); 
		try {
			ArrayList<XnatMrsessiondata> subjSessions = new ArrayList<>();
			for (final XnatSubjectassessordata assessor : subj.getExperiments_experiment(XnatMrsessiondata.SCHEMA_ELEMENT_NAME)) {
				subjSessions.add((XnatMrsessiondata)assessor);
			}
			expts.addAll(releaseRules.filterExptList(getProjectExpts(proj, subjSessions),params,user));
		} catch (ReleaseRulesException e) {
			_logger.error("Could not retrieve sessions from release rules: ", e);
		}
		return buildSessionReport(proj, subj, params, releaseRules, expts);
	}
	
	private String itemToXmlString(XFTItem item) throws IllegalArgumentException, SAXException, IOException {
		final StringWriter strout = new StringWriter();
		item.toXML(strout, false);
		final String rtStr = strout.toString();
		strout.close();
		return rtStr;
	}

	private ResponseEntity<String> buildSessionReport(XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, CcfReleaseRulesI releaseRules, List<XnatMrsessiondata> expts) throws Exception {
		final List<XnatMrsessiondata> sessionList = new ArrayList<>();
		sessionList.addAll(expts);
		final List<XnatMrscandata> scanList = CcfReleaseRulesUtils.getOrderedScanList(sessionList, releaseRules);
		final String requestedReportType = params.get(REPORT_FORMAT_PARAM);
		final SessionBuildingReportFormats reportType = 
				(requestedReportType != null && (requestedReportType.toUpperCase().equals("CSV") ||
						requestedReportType.toUpperCase().equals("EMAIL"))) ?
						SessionBuildingReportFormats.CSV : SessionBuildingReportFormats.JSON;
		//ResponseEntity<String> response = new ResponseEntity<String>(
		//		releaseRules.getReport(scanMap, params.get(SELECTION_CRITERIA_PARAM), reportType), HttpStatus.OK);
		//return response;
		//return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=" + subj + "_report.csv").body(releaseRules.getReport(scanMap, params.get(SELECTION_CRITERIA_PARAM), reportType));
		return new ResponseEntity<String>(releaseRules.getReport(scanList, sessionList, releaseRules, reportType), HttpStatus.OK);
	}

	private XnatMrsessiondata buildAndReturnCombinedSession(XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, CcfReleaseRulesI releaseRules, List<XnatMrsessiondata> expts, UserI user) throws Exception {
		final XnatMrsessiondata combSess = new XnatMrsessiondata();
		if (params.containsKey("exptID")) {
			combSess.setId(params.get("exptID"));
		}
		if (params.containsKey("date")) {
			final String dateStr = params.get("date");
			try {
				parseAndSetDate(dateStr, combSess);
			} catch (Exception e) {
				combSess.setDate(null);

			}
		} else {
			combSess.setDate(null);
		}
		combSess.setDcmpatientname(CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj));
		// This doesn't seem to be used
		//combSess.setDcmpatientid(combSessLabel);
		combSess.setDcmpatientbirthdate(null);
		
		boolean anyRelease = false;
		// Per meeting 2012-12-18, ordering all scans by acquisition time
		final List<XnatMrsessiondata> sessionList = new ArrayList<>();
		sessionList.addAll(expts);
		final List<XnatMrscandata> scanList = CcfReleaseRulesUtils.getOrderedScanList(sessionList, releaseRules);
		XnatMrsessiondata prevSess = null;
		String intraLabel = null;
		for (final XnatMrscandata intraScan : scanList) {
			final XnatMrsessiondata intraSess = getScanSession(intraScan,expts);
			if (intraLabel == null) {
				intraLabel = intraScan.getDbsession();
			}
			if (!(intraSess == null || intraSess.equals(prevSess))) {
				prevSess = intraSess;
				if (combSess.getAcquisitionSite() == null
						|| combSess.getAcquisitionSite().length() < 1) {
					combSess.setAcquisitionSite((String) getCombinedAttr(
							combSess.getAcquisitionSite(),
							intraSess.getAcquisitionSite(), "Acqusition Site"));
				}
				if (combSess.getScanner() == null
						|| combSess.getScanner().length() < 1) {
					combSess.setScanner((String) getCombinedAttr(
							combSess.getScanner(), intraSess.getScanner(),
							"Scanner"));
				}
				if (combSess.getOperator() == null
						|| combSess.getOperator().length() < 1) {
					combSess.setOperator((String) getCombinedAttr(
							combSess.getOperator(), intraSess.getOperator(),
							"Operator"));
				}
			}

			// TO BE SET LATER
			// combSess.setId()
			// whole data release
			// Should vary - leave blank
			// combSess.setDcmaccessionnumber()
			// combSess.setDcmpatientid()
			// combSess.setDcmpatientname()
			// combSess.setTime()
			// Not typically set - leave blank
			// combSess.setDuration()
			// HIPAA: Leave blank
			// combSess.setDcmpatientbirthdate()

			// continue only if targeted for export
			final Boolean isTarget = intraScan.getTargetforrelease();
			final Integer isOverride;
			if (!releaseRules.isStructuralScan(intraScan) || intraScan.getReleaseoverride()==null) {
				isOverride = 0;
			} else {
				isOverride = intraScan.getReleaseoverride();
			}
			// Override is currently only applicable for rated structural scans 
			// Newly added conditional for ACP project 2024/08/15, where we're releasing all structurals 
			if (!isAlternateScan(intraScan)) {
			 if (!((isOverride>0) || (isOverride>=0 && (isTarget!=null && isTarget)))) {
				continue;
			 }
			}
			anyRelease = true;
			
			// Need to remove file objects from dbScan to permit upload (references Intradb file path)
			// These two lines are important, otherwise the scan will be MOVED.
			final XnatMrscandata dbScan = new XnatMrscandata((ItemI)intraScan.getItem().clone());
			if (dbScan.getFile().size()>0) {
				while (true) {
					try { 
						dbScan.removeFile(0);
					} catch (Exception e) {
						break;
					}
				}
			}
			try {
				dbScan.setId(dbScan.getDbid());
			} catch (NumberFormatException e) {
				throw new ClientException("ERROR:  Couldn't generate scan ID", e);
			}
			dbScan.setImageSessionId(combSess.getId());
			if (releaseRules.requireScanUidAnonymization()) {
				dbScan.setUid(CcfUidUtils.anonymizeUid(dbScan.getUid()));
			}
			// This seems to no longer be getting set
			//dbScan.setDatarelease(dataRelease);
			dbScan.setReleasecountscan(null);
			dbScan.setReleasecountscanoverride(null);
			dbScan.setTargetforrelease(null);
			dbScan.setReleaseoverride(null);
			dbScan.setDbsession(null);
			dbScan.setDbid(null);
			// Keep Intradb description for now
			final String idbDesc = dbScan.getSeriesDescription();
			dbScan.setSeriesDescription(dbScan.getDbdesc());
			dbScan.setDbdesc(idbDesc);
			dbScan.setType(dbScan.getDbtype());
			dbScan.setDbtype(null);
			// Per meeting, for now we won't populate scan notes
			dbScan.setNote(null);
			// We need to exclude all hidden fields, so let's convert to XML and back. 
			// TODO:  This could certainly be done more efficiently by removing the hidden fields from the item directly.
			final StringWriter sw = new StringWriter();
			SAXWriter writer = null;
			// I don't know why, but an exception is occasionally thrown getting the SAXWriter.  It's usually successful
			// on reruns of the same session.  Just putting it in a loop to try a few times before giving up.
			for (int i=0; i<5; i++) {
				try {
					writer = new SAXWriter(sw,false);
					break;
				} catch (Exception e) {
					_logger.debug("NOTE:  Exception thrown obtaining SAXWriter:  ", e);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException intE) {
						// Do nothing
					}
				}
			}
			if (writer == null) {
				throw new SessionBuildingException("Couldn't obtain SAXWriter");
			}			
			writer.setWriteHiddenFields(false);
			writer.write(dbScan.getItem());
			final SAXReader reader = new SAXReader(user);
			final XFTItem newItem = reader.parse(new ByteArrayInputStream(sw.toString().getBytes()));
			sw.close();
			//sr.close();
			combSess.setScans_scan(newItem);
		}
		if (params.containsKey("label")) {
			combSess.setLabel(params.get("label"));
		} else if (intraLabel!=null && intraLabel.length()>0) {
			combSess.setLabel(intraLabel);
			
		} else {
			combSess.setLabel(params.containsKey(SESSION_LABEL_PARAM) ? params.get(SESSION_LABEL_PARAM) : releaseRules.getDefaultSessionLabel(proj, subj, params));
		}
		if (!anyRelease) {
			/*
			if (_logger.isDebugEnabled()) {
				try { 
					_logger.debug("Session could not be built (no scans marked for export) \n" + itemToXmlString(combSess.getItem()));
				} catch (Exception e) {
					_logger.debug("Session could not be built (no scans marked for export), but session XML could not be logged.\n" + e.toString());
				}
				for (final XnatMrscandata intraScan : scanMap.keySet()) {
					try { 
						_logger.debug("Scan ID=" + intraScan.getId() + ", TYPE=" + intraScan.getType() + ", " + intraScan.getSeriesDescription() + ":  \n"+ itemToXmlString(intraScan.getItem()));
					} catch (Exception e) {
						_logger.debug("Scan ID=" + intraScan.getId() + ", TYPE=" + intraScan.getType() + ", " + intraScan.getSeriesDescription() + ":  \nCould not print scan XML" + e.toString());
					}
				}
			}
			*/
			throw new ClientException(
					"ERROR:  Session includes no scans marked for export");
		}
		return combSess;
	}
	
	private boolean isAlternateScan(XnatMrscandata intraScan) {
		final String type = intraScan.getDbtype();
		final String desc = intraScan.getDbdesc();
		if (type == null || desc == null) {
			return false;
		}
		return (type.endsWith("_Alt") && desc.endsWith("_Alt"));
	}

	private XnatMrsessiondata getScanSession(XnatMrscandata intraScan, List<XnatMrsessiondata> expts) {
		for (final XnatImagesessiondata expt : expts) {
			if (!(expt instanceof XnatMrsessiondata)) {
				continue;
			}
			if (intraScan.getImageSessionId().equals(expt.getId())) {
				return (XnatMrsessiondata)expt;
			}
		}
		return null;
	}

	private List<XnatMrsessiondata> getProjectExpts(XnatProjectdata proj, ArrayList<XnatMrsessiondata> expts) {
		final Iterator<XnatMrsessiondata> i = expts.iterator();
		while (i.hasNext()) {
			final XnatMrsessiondata expt = i.next();
			if (!expt.getProject().equals(proj.getId())) {
				i.remove();
			}
		}
		return expts;
	}

	private void parseAndSetDate(String dateStr, XnatSubjectassessordata sess) {
		// Per Sandy, 2013-01-17, do not set anything as a session date
		sess.setDate(null);
	}

	private Object getCombinedAttr(Object combVal, Object mrVal,
			String attrString) {
		if (combVal == null) {
			combVal = mrVal;
		} else if (!combVal.equals(mrVal)) {
			final String returnString = "WARNING:  " + attrString
					+ " value differs between sessions (CURRENT="
					+ mrVal.toString() + ",PREVIOUS=" + combVal.toString()
					+ ").";
			_logger.warn(returnString);
		}
		return combVal;
	}
	
	private ResponseEntity<RulesProcessingResults> processScanValueRequest(String projectId, String subjectId, Map<String, String> params, UserI user) {
		
		XnatProjectdata proj=null;
		XnatSubjectdata subj=null;
		final List<XnatMrsessiondata> sessionList = Lists.newArrayList();
		if (projectId!=null) {
			proj = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
			if(proj != null && subjectId!=null){
				subj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjectId, user, false);
				if(subj==null){
					subj = XnatSubjectdata.getXnatSubjectdatasById(subjectId, user, false);
					if (subj != null && (proj != null && !subj.hasProject(proj.getId()))) {
						subj = null;
					}
				}
			}
		}
		if (proj == null) {
			return new ResponseEntity<>(new RulesProcessingResults(false, "Invalid project specified (" + projectId + ")"),  HttpStatus.BAD_REQUEST);
		}
		if (subj == null) {
			return new ResponseEntity<>(new RulesProcessingResults(false, "Invalid subject specified (PROJECT=" + projectId + ",SUBJECT=" + subjectId + ")."),  HttpStatus.BAD_REQUEST);
		}
		
		final ReleaseRulesResponse rrResponse = _rulesUtils.getReleaseRulesForProject(proj, params.get(RELEASE_RULES_PARAM));
		final CcfReleaseRulesI releaseRules = rrResponse.getReleaseRules();
		if (releaseRules == null) {
			return new ResponseEntity<>(new RulesProcessingResults(false, "Could not obtain release rules class:  " +
					rrResponse.getResponseEntity().getBody()), rrResponse.getResponseEntity().getStatusCode());
		}
		
		final CriteriaCollection criteria=new CriteriaCollection("AND");
		final CriteriaCollection cc= new CriteriaCollection("OR");
		//// Currently we'll only retrieve MR session types
		cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME+"/project", proj.getId());
		cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME+"/sharing/share/project", proj.getId());
		criteria.addClause(cc);
		criteria.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/subject_ID", subj.getId());
		sessionList.addAll(XnatMrsessiondata.getXnatMrsessiondatasByField(criteria, user, false));
				
		final Iterator<XnatMrsessiondata> i = sessionList.iterator();
		while (i.hasNext()) {
			final XnatSubjectassessordata session = i.next();
			if (!(session instanceof XnatMrsessiondata)) {
				i.remove();
			}
		}
		
		final List<XnatMrsessiondata> filteredList = new ArrayList<>();
		try {
			//filteredList.addAll
			final List<? extends XnatSubjectassessordata> filteredSubjAssessors = releaseRules.filterExptList(sessionList, params, user);
			for (final XnatSubjectassessordata assessor: filteredSubjAssessors) {
				if (assessor instanceof XnatMrsessiondata) {
					filteredList.add((XnatMrsessiondata)assessor);
				}
			}
		} catch (ReleaseRulesException e) {
			_logger.error(e.toString());
		}



			
		if (filteredList.size()<1) {
			final String selectionCriteria = params.get(SELECTION_CRITERIA_PARAM); 
			return new ResponseEntity<>(new RulesProcessingResults(false, "No MR sessions found matching selection criteria for this subject " +
							"(PROJECT=" + projectId + ",SUBJECT=" + subjectId +
							((selectionCriteria != null) ? ",CRITERIA=" + selectionCriteria : "") +
							").  " + 
							"Session building currently supports only MR session data."),  HttpStatus.BAD_REQUEST);
		}
		
		return processSessionList(proj, subj, params, filteredList, releaseRules, user);

	}

	private ResponseEntity<RulesProcessingResults> processSessionList(XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, 
			List<XnatMrsessiondata> subjectSessions, CcfReleaseRulesI releaseRules, UserI user) {
		
		// First, create ordered list of scans with their associated sessions
		final List<XnatMrscandata> subjectScans = CcfReleaseRulesUtils.getOrderedScanList(subjectSessions, releaseRules);
		final List<String> warningList = new ArrayList<>();
		RulesProcessingResults results;
		try {
			warningList.addAll(processScans(proj, subj, params, subjectScans, subjectSessions, releaseRules, user));
			results = new RulesProcessingResults(true, (warningList.size()>0) ? MiscUtils.listToHtml(warningList) : 
					"Release target processing complete.  No warnings or errors reported.", warningList, null); 
			return new ResponseEntity<>(results, HttpStatus.OK);
		} catch (ReleaseRulesException e) {
			_logger.debug(ExceptionUtils.getStackTrace(e));
			results = new RulesProcessingResults(false, "Rules exception thrown during processing:  " +
					ExceptionUtils.getStackTrace(e), null, e);
			return new ResponseEntity<>(results,HttpStatus.CONFLICT);
		} catch (Exception e) {
			final String stackTrace = ExceptionUtils.getStackTrace(e);
			_logger.debug("Unexpected exception occurred during processing:  \n" + stackTrace);
			results = new RulesProcessingResults(false, "Unexpected exception thrown during processing:  " +
					stackTrace, warningList, e);
			return new ResponseEntity<>(results,HttpStatus.INTERNAL_SERVER_ERROR); 
					
		}
		
	}

	private List<String> processScans(XnatProjectdata proj, XnatSubjectdata subj, Map<String, String> params, List<XnatMrscandata>
					subjectScans, List<XnatMrsessiondata> subjectSessions, CcfReleaseRulesI releaseRules, UserI user) throws ReleaseRulesException, Exception {
		final List<String> warningList;
		String selectionCriteria = params.get(SELECTION_CRITERIA_PARAM);
		if (selectionCriteria == null || selectionCriteria.trim().length()<1) {
			List<String> criteriaList = releaseRules.getSelectionCriteria();
			if (criteriaList.size()==1) {
				selectionCriteria = criteriaList.get(0);
			}
		}
		try {
			if (params.containsKey("errorOverride") && params.get("errorOverride").matches("(?i)^[TY].*$")) {
				warningList = releaseRules.applyRulesToScans(subjectScans,subjectSessions,params,true);
			} else {
				warningList = releaseRules.applyRulesToScans(subjectScans,subjectSessions,params,false);
			}
			CcfSessionBuildingUtils.persistRulesErrors(proj, subj, selectionCriteria, releaseRules, warningList);
		} catch (ReleaseRulesException e) {
			CcfSessionBuildingUtils.persistRulesErrors(proj, subj, selectionCriteria, releaseRules, e.getErrorList());
			throw e;
		} catch (Exception e) {
			_logger.error("RulesProcessor threw exception:  ");
			_logger.error(ExceptionUtils.getStackTrace(e));
			throw e;
		}
	 	/*
	 	if (_logger.isDebugEnabled()) {
	 		_logger.debug("DEBUG OUTPUT:  subjectScans XML\n");
	 		for (XnatMrscandata scan : subjectScans.keySet()) {
				try { 
					_logger.debug("subjectScans ID=" + scan.getId() + ", TYPE=" + scan.getType() + ", " + scan.getSeriesDescription() + ":  \n"+ itemToXmlString(scan.getItem()));
				} catch (Exception e) {
					_logger.debug("subjectScans ID=" + scan.getId() + ", TYPE=" + scan.getType() + ", " + scan.getSeriesDescription() + ":  \nCould not print scan XML" + e.toString());
				}
	 		}
	 	}
	 	*/
	 	final String dbLabel = params.containsKey(SESSION_LABEL_PARAM) ? params.get(SESSION_LABEL_PARAM) : releaseRules.getDefaultSessionLabel(proj, subj, params);
	 	final String dataRelease = params.containsKey("dataRelease") ? params.get("dataRelease") : "Not Specified";
		for (XnatMrscandata scan : subjectScans) {
			scan.setDatarelease(dataRelease);
			scan.setDbsession(dbLabel);
			PersistentWorkflowI wrk;
			try {
				wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, scan.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.MODIFY_VIA_WEB_SERVICE, null, null));
				final EventMetaI ci = wrk.buildEvent();
				if (SaveItemHelper.authorizedSave(scan,user,false,true,ci)) {
					PersistentWorkflowUtils.complete(wrk, ci);
				} else {
					PersistentWorkflowUtils.fail(wrk,ci);
				}
			} catch (Exception e) {
				_logger.error("SERVER EXCEPTION:  " + ExceptionUtils.getStackTrace(e));
				throw(e);
			}
		}
		return warningList;
	}
	
	private String listString(List<String> l) {
		final Gson gson = new Gson();
		return gson.toJson(l);
		//final StringBuilder sb = new StringBuilder();
		//for (final String it : l) {
		//	sb.append(it + System.getProperty("line.separator"));
		//}
		//return sb.toString();
	}

}
