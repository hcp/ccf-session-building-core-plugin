package org.nrg.ccf.sessionbuilding.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "ccfSessionBuildingCorePlugin",
			name = "CCF Session Building Core Plugin",
			entityPackages = "org.nrg.ccf.sessionbuilding.entities",
			log4jPropertiesFile = "/META-INF/resources/sessionBuildingLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.ccf.sessionbuilding.conf",
		"org.nrg.ccf.sessionbuilding.dao",
		"org.nrg.ccf.sessionbuilding.preferences",
		"org.nrg.ccf.sessionbuilding.services",
		"org.nrg.ccf.sessionbuilding.utils",
		"org.nrg.ccf.sessionbuilding.xapi"
	})
public class SessionBuildingCorePlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(SessionBuildingCorePlugin.class);

	/**
	 * Instantiates a new ccf subject ids plugin.
	 */
	public SessionBuildingCorePlugin() {
		logger.info("Configuring CCF session building core plugin");
	}
	
}
