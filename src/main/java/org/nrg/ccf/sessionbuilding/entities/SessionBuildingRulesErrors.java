package org.nrg.ccf.sessionbuilding.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("serial")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
@Table(uniqueConstraints = {
			@UniqueConstraint(columnNames = {"projectId", "subjectId", "selectionCriteria", "rulesClass"})
		})
public class SessionBuildingRulesErrors extends AbstractHibernateEntity {
	
	public static final int FIELD_LENGTH = 2048;
	
	@NonNull
	private String projectId;
	@NonNull
	private String subjectId;
	@NonNull
	private String selectionCriteria;
	@NonNull
	private String rulesClass;
	
	private List<String> rulesErrors = new ArrayList<>();
	
    // Must provide the public getRulesErrors() method explicitly (i.e. without Lombok) in order for Hibernate
    // to properly determine the type of the list.
	@ElementCollection(fetch = FetchType.EAGER)
	@Column(length=FIELD_LENGTH)
	public List<String> getRulesErrors() {
		return rulesErrors;
	}

}
