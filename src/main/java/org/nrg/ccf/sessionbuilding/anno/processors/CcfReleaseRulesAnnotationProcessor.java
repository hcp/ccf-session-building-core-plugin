package org.nrg.ccf.sessionbuilding.anno.processors;

import org.kohsuke.MetaInfServices;

import com.google.common.collect.Maps;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.framework.processors.NrgAbstractAnnotationProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.util.Map;
import java.util.Random;

@MetaInfServices(Processor.class)
@SupportedAnnotationTypes("org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules")
public class CcfReleaseRulesAnnotationProcessor extends NrgAbstractAnnotationProcessor<CcfReleaseRules> {
	
	final static Random random = new Random();

	@Override
	protected Map<String, String> processAnnotation(TypeElement element, CcfReleaseRules annotation) {
		final Map<String, String> properties = Maps.newLinkedHashMap();
		properties.put(CcfReleaseRules.RULES_CLASS, element.getQualifiedName().toString());
		return properties;
	}

	@Override
	protected String getPropertiesName(TypeElement element, CcfReleaseRules annotation) {
        return String.format("sessionbuilding/%s-sessionbuilding.properties", element.getSimpleName());
	}

}
