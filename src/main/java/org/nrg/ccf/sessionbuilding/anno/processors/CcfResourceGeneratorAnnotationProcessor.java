package org.nrg.ccf.sessionbuilding.anno.processors;

import org.kohsuke.MetaInfServices;

import com.google.common.collect.Maps;

import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.framework.processors.NrgAbstractAnnotationProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.util.Map;
import java.util.Random;

@MetaInfServices(Processor.class)
@SupportedAnnotationTypes("org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator")
public class CcfResourceGeneratorAnnotationProcessor extends NrgAbstractAnnotationProcessor<CcfResourceGenerator> {
	
	final static Random random = new Random();

	@Override
	protected Map<String, String> processAnnotation(TypeElement element, CcfResourceGenerator annotation) {
		final Map<String, String> properties = Maps.newLinkedHashMap();
		properties.put(CcfResourceGenerator.RESOURCE_GENERATOR, element.getQualifiedName().toString());
		return properties;
	}

	@Override
	protected String getPropertiesName(TypeElement element, CcfResourceGenerator annotation) {
        return String.format("sessionbuilding/%s-sessionbuilding.properties", element.getSimpleName());
	}

}
