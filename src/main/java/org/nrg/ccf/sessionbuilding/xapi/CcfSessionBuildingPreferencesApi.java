package org.nrg.ccf.sessionbuilding.xapi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseRules;
import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.preferences.CcfSessionBuildingPreferences;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils.ReleaseRulesResponse;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.model.XnatProjectparticipantI;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.collect.Lists;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(description = "CCF Session Building Preferences API")
@XapiRestController
@RequestMapping(value = "/ccfSessionBuildingPreferences")
public class CcfSessionBuildingPreferencesApi extends AbstractXapiRestController {

	private final CcfSessionBuildingPreferences _preferences;
    private List<Resource> _resourceList; 
    private CcfReleaseRulesUtils _rulesUtils;
	private JsonFactory YAML_FACTORY = new YAMLFactory();

	@Lazy
	@Autowired
	public CcfSessionBuildingPreferencesApi(UserManagementServiceI userManagementService, RoleHolder roleHolder,
				final CcfSessionBuildingPreferences preferences, final CcfReleaseRulesUtils rulesUtils) {
		super(userManagementService, roleHolder);
		_preferences = preferences;
		_rulesUtils = rulesUtils;
	}
	
    @ApiOperation(value = "Gets the CCF session building project configuration.", notes = "Returns project-level CCF session building configuration.", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/project/{projectId}/settings"}, produces = {MediaType.APPLICATION_JSON_VALUE}, 
    				restrictTo=AccessLevel.Read, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String,Object>> getCcfSessionBuildingProjectPreferences(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
    	return getCcfSessionBuildingProjectPreferences(projectId, null);
    }
	
    @ApiOperation(value = "Gets the CCF session building project configuration.", notes = "Returns project-level CCF session building configuration.", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/project/{projectId}/subject/{subjectId}/settings"}, produces = {MediaType.APPLICATION_JSON_VALUE}, 
    	restrictTo=AccessLevel.Read, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String,Object>> getCcfSessionBuildingProjectPreferences(@PathVariable("projectId") @ProjectId final String projectId,
    		@PathVariable("subjectId") final String subjectId) throws NrgServiceException {
    	try {
			final Map<String,Object> conMap = new HashMap<>();
			final Boolean enabledVal = _preferences.getCcfSessionBuildingEnabled(projectId);
			final String rulesClassVal = _preferences.getCcfSessionBuildingRulesClass(projectId);
			final List<String> selRulesClassVal = _preferences.getCcfSessionBuildingSelectableRulesClasses(projectId);
			final String fileHandlerVal = _preferences.getCcfSessionBuildingFileHandler(projectId);
			final List<String> selFileHandlerVal = _preferences.getCcfSessionBuildingSelectableFileHandlers(projectId);
			final String resourceGenVal = _preferences.getCcfSessionBuildingResourceGen(projectId);
			final List<String> selResourceGenVal = _preferences.getCcfSessionBuildingSelectableResourceGen(projectId);
			final String buildProjectVal = _preferences.getCcfSessionBuildingBuildProject(projectId);
			final Boolean hardOverrideVal = _preferences.getCcfSessionBuildingHardOverride(projectId);
			conMap.put(CcfSessionBuildingPreferences.SESSION_BUILDING_ENABLED, (enabledVal!=null) ? enabledVal.toString() : "false");
			if (rulesClassVal != null) {
				conMap.put(CcfSessionBuildingPreferences.SESSION_BUILDING_RULESCLASS, rulesClassVal);
			}
			if (selRulesClassVal != null) {
				conMap.put(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_RULESCLASSES, selRulesClassVal);
			}
			if (fileHandlerVal != null) {
				conMap.put(CcfSessionBuildingPreferences.SESSION_BUILDING_FILEHANDLER, fileHandlerVal);
			}
			if (selFileHandlerVal != null) {
				conMap.put(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_FILEHANDLERS, selFileHandlerVal);
			}
			if (resourceGenVal != null) {
				conMap.put(CcfSessionBuildingPreferences.SESSION_BUILDING_RESOURCEGEN, resourceGenVal);
			}
			if (selResourceGenVal != null) {
				conMap.put(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_RESOURCEGEN, selResourceGenVal);
			}
			if (buildProjectVal != null) {
				conMap.put(CcfSessionBuildingPreferences.SESSION_BUILDING_BUILDPROJECT, buildProjectVal);
			}
			conMap.put(CcfSessionBuildingPreferences.SESSION_BUILDING_HARDOVERRIDE, (hardOverrideVal!=null) ? hardOverrideVal.toString() : "false");
			final Map<String, List<String>> parameterMap = new HashMap<>();
			for (final String classStr : selRulesClassVal) {
				@SuppressWarnings("rawtypes")
				final Class clazz = Class.forName(classStr);
				if (clazz != null) {
					final CcfReleaseRulesI rulesInst = (CcfReleaseRulesI) clazz.newInstance();
					if (rulesInst.getParametersYaml() != null) {
						parameterMap.put(classStr,convertYaml(rulesInst.getParametersYaml()));
					}
				}
			}
			conMap.put("ccfSessionBuildingRulesClassParameters", parameterMap);
			if (subjectId!=null && subjectId.length()>0) {
				final ReleaseRulesResponse rrResponse = _rulesUtils.getReleaseRulesForProject(projectId, null);
				final CcfReleaseRulesI releaseRules = (rrResponse!=null) ? rrResponse.getReleaseRules() : null;
				if (releaseRules!=null) {
					final XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(projectId, getSessionUser(), false);
					List<XnatSubjectdata> subjList = XnatSubjectdata.getXnatSubjectdatasByField("xnat:subjectData.ID", subjectId, getSessionUser(), false);
					if (subjList==null || subjList.size()<1) {
						subjList = XnatSubjectdata.getXnatSubjectdatasByField("xnat:subjectData/label", subjectId, getSessionUser(), false);
					}
					XnatSubjectdata matchedSubj = null;
					for (final XnatSubjectdata subj : subjList) {
						if (subj.getProject().equals(projectId)) {
							matchedSubj = subj;
						}
					}
					if (matchedSubj==null) {
						for (final XnatSubjectdata subj : subjList) {
							for (XnatProjectparticipantI pp : subj.getSharing_share()) {
								if (pp.getProject().matches(projectId)) {
									matchedSubj = subj;
								}
							}
						}
					}
					if (matchedSubj!=null) {
						conMap.put(CcfSessionBuildingPreferences.SESSION_BUILDING_SESSIONLABEL, 
								releaseRules.getDefaultSessionLabel(proj, matchedSubj, new HashMap<String,String>()));
					}
				}
			}
			
        	return new ResponseEntity<>(conMap, HttpStatus.OK);
		} catch (Exception e) {
        	throw new NrgServiceException("Could not retrieve CCF session building project preferences", e);
		}
    }

    private List<String> convertYaml(List<String> parametersYaml) {
    	final List<String> returnList = new ArrayList<>();
    	if (parametersYaml == null) {
    		return returnList;
    	}
    	for (final String yamlStr : parametersYaml) {
    		if (yamlStr == null || yamlStr.length()<1) {
    			continue;
    		}
			try {
				final ObjectMapper yamlReader = new ObjectMapper(YAML_FACTORY);
				final Object obj = yamlReader.readValue(yamlStr, Object.class);
				final ObjectMapper jsonWriter = new ObjectMapper();
				returnList.add(jsonWriter.writeValueAsString(obj));
			} catch (Throwable e) {
				// Do nothing for now.
			}
    	}
    	return returnList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation(value = "Sets the CCF session building project configuration.", notes = "Sets project-level CCF session building configuration.", response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/project/{projectId}/settings"}, consumes = MediaType.APPLICATION_JSON_VALUE,
    		restrictTo=AccessLevel.Owner, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setCcfSessionBuildingProjectPreferences(@PathVariable("projectId") @ProjectId final String projectId,
    		@RequestBody final Map<String, Object> config) throws NrgServiceException {
    	try {
			if (config.containsKey(CcfSessionBuildingPreferences.SESSION_BUILDING_ENABLED)) {
				_preferences.setCcfSessionBuildingEnabled(projectId, Boolean.valueOf(config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_ENABLED).toString()));
			}
			if (config.containsKey(CcfSessionBuildingPreferences.SESSION_BUILDING_RULESCLASS)) {
				_preferences.setCcfSessionBuildingRulesClass(projectId, config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_RULESCLASS).toString());
			}
			if (config.containsKey(CcfSessionBuildingPreferences.SESSION_BUILDING_FILEHANDLER)) {
				_preferences.setCcfSessionBuildingFileHandler(projectId, config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_FILEHANDLER).toString());
			}
			if (config.containsKey(CcfSessionBuildingPreferences.SESSION_BUILDING_RESOURCEGEN)) {
				_preferences.setCcfSessionBuildingResourceGen(projectId, config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_RESOURCEGEN).toString());
			}
			if (config.containsKey(CcfSessionBuildingPreferences.SESSION_BUILDING_BUILDPROJECT)) {
				_preferences.setCcfSessionBuildingBuildProject(projectId, config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_BUILDPROJECT).toString());
			}
			if (config.containsKey(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_RULESCLASSES)) {
				final Object selConObj = config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_RULESCLASSES);
				if (selConObj instanceof List) {
					_preferences.setCcfSessionBuildingSelectableRulesClasses(projectId,
						(List)config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_RULESCLASSES));
				}
				
			}
			if (config.containsKey(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_FILEHANDLERS)) {
				final Object selConObj = config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_FILEHANDLERS);
				if (selConObj instanceof List) {
					_preferences.setCcfSessionBuildingSelectableFileHandlers(projectId,
						(List)config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_FILEHANDLERS));
				}
			}
			if (config.containsKey(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_RESOURCEGEN)) {
				final Object selConObj = config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_RESOURCEGEN);
				if (selConObj instanceof List) {
					_preferences.setCcfSessionBuildingSelectableResourceGen(projectId,
						(List)config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_SELECTABLE_RESOURCEGEN));
				}
			}
			if (config.containsKey(CcfSessionBuildingPreferences.SESSION_BUILDING_HARDOVERRIDE)) {
				_preferences.setCcfSessionBuildingHardOverride(projectId, Boolean.valueOf(config.get(CcfSessionBuildingPreferences.SESSION_BUILDING_HARDOVERRIDE).toString()));
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
        	throw new NrgServiceException("CCF session building project preferences assignment failed", e);
		}
    }
	
	@ApiOperation(value = "Returns available configuration classes",  response = Map.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@XapiRequestMapping(value = "/configClasses", produces = MediaType.APPLICATION_JSON_VALUE, 
			restrictTo=AccessLevel.Authenticated, method = RequestMethod.GET)
	public ResponseEntity<Map<String,List<String>>> getRulesClasses() {
		try {
			final Map<String,List<String>> classMap = new HashMap<>();
			classMap.put("rulesClasses", getRulesClassList());
			classMap.put("fileHandlerClasses", getFileHandlerClassList());
			classMap.put("resourceGenerators", getResourceGeneratorClassList());
			return new ResponseEntity<>(classMap,  HttpStatus.OK);
		}catch (Exception  exception) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	private List<String> getRulesClassList() throws IOException {
		final List<String> rulesClasses = Lists.newArrayList();
		for (final Resource resource : getResourceList()) {
			final Properties properties = PropertiesLoaderUtils.loadProperties(resource);
			if (!properties.containsKey(CcfReleaseRules.RULES_CLASS)) {
				continue;
			}
			rulesClasses.add(properties.get(CcfReleaseRules.RULES_CLASS).toString());
		}
        return rulesClasses;
	}

	private List<String> getFileHandlerClassList() throws IOException {
		final List<String> fileHandlerClasses = Lists.newArrayList();
		for (final Resource resource : getResourceList()) {
			final Properties properties = PropertiesLoaderUtils.loadProperties(resource);
			if (!properties.containsKey(CcfReleaseFileHandler.FILE_HANDLER)) {
				continue;
			}
			fileHandlerClasses.add(properties.get(CcfReleaseFileHandler.FILE_HANDLER).toString());
		}
        return fileHandlerClasses;
	}
	
	private List<String> getResourceGeneratorClassList() throws IOException {
		final List<String> resourceGeneratorClasses = Lists.newArrayList();
		for (final Resource resource : getResourceList()) {
			final Properties properties = PropertiesLoaderUtils.loadProperties(resource);
			if (!properties.containsKey(CcfResourceGenerator.RESOURCE_GENERATOR)) {
				continue;
			}
			resourceGeneratorClasses.add(properties.get(CcfResourceGenerator.RESOURCE_GENERATOR).toString());
		}
        return resourceGeneratorClasses;
	}

	private List<Resource> getResourceList() throws IOException {
		return (_resourceList!=null) ? _resourceList : BasicXnatResourceLocator.getResources("classpath*:META-INF/xnat/sessionbuilding/*-sessionbuilding.properties");
	}

}
