package org.nrg.ccf.sessionbuilding.exception;

public class ReleaseTransferException extends Exception {

	private static final long serialVersionUID = 6629790608778954409L;

	public ReleaseTransferException(String string, Exception e) {
		super(string, e);
	}

}
