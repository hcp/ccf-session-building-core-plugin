package org.nrg.ccf.sessionbuilding.comparators;

import java.text.DateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;

public class DefaultTemporalComparator implements Comparator<XnatImagescandata> {
	
	private List<XnatMrsessiondata> _sessionList;
	private DateFormat YYMMDD10_FORMAT = CommonConstants.getFormatYYMMDD10();
	private DateFormat TIME_FORMAT = CommonConstants.getFormatTIME();
	
	public DefaultTemporalComparator(List<XnatMrsessiondata> sessionList) {
		super();
		this._sessionList = sessionList;
	}

	@Override
	public int compare(XnatImagescandata arg0, XnatImagescandata arg1) {
		
		try {
			arg0.getImageSessionData();
			final Date d0 = YYMMDD10_FORMAT.parse(getScanSession(arg0).getDate().toString());
			final Date d1 = YYMMDD10_FORMAT.parse(getScanSession(arg1).getDate().toString());
			if (d0.before(d1)) {
				return -1;
			} else if (d1.before(d0)) {
				return 1;
			} 
		} catch (Exception e) {
			// Do nothing for now
		}
		try {
			final Date t0 = TIME_FORMAT.parse(arg0.getStarttime().toString());
			final Date t1 = TIME_FORMAT.parse(arg1.getStarttime().toString());
			if (t0.before(t1)) {
				return -1;
			} else if (t1.before(t0)) {
				return 1;
			}
		} catch (Exception e) {
			// Do nothing for now
		}
		try {
			if (Integer.parseInt(arg0.getId()) <  Integer.parseInt(arg1.getId())) {
				return -1;
			} else if (Integer.parseInt(arg1.getId()) >  Integer.parseInt(arg0.getId())) {
				return 1;
			}
		} catch (Exception e) {
			// Do nothing for now
		}
		return 0;
	}

	protected XnatImagesessiondata getScanSession(XnatImagescandata arg0) {
		for (XnatImagesessiondata sess : _sessionList) {
			if (sess.getId().equals(arg0.getImageSessionId())) {
				return sess;
			}
		}
		return null;
	}
	
}
