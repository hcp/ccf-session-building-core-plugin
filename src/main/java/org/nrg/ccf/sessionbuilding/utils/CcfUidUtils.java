package org.nrg.ccf.sessionbuilding.utils;

import java.util.Random;

public class CcfUidUtils {
	
	private static final Random randomizer = new Random();
	
	public static String anonymizeUid(final String uid) {
		if (uid == null) {
			return null;
		}
		if (!uid.matches("^.*[.]20[0123][0-9][01][0-9][0-3][0-9][0-9][0-9][0-9][0-9].*$")) {
			return uid;
		}
		final StringBuffer repl = new StringBuffer("."); 
		for (int i=1; i<=12; i++) {
			repl.append(randomizer.nextInt(10));
		}
		return uid.replaceFirst("[.]20[0123][0-9][01][0-9][0-3][0-9][0-9][0-9][0-9][0-9]",repl.toString());
	}

}
