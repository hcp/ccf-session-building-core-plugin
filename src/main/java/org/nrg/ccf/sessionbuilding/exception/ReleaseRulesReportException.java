package org.nrg.ccf.sessionbuilding.exception;

import java.util.List;

import com.google.gson.Gson;

public class ReleaseRulesReportException extends Exception{
	
	private static final long serialVersionUID = 7069339200061783047L;
	private static final Gson gson = new Gson();

	public ReleaseRulesReportException(List<String> errorList,Throwable e){
		super((errorList!=null) ? gson.toJson(errorList) : null,e);
	}
	
	public ReleaseRulesReportException(List<String> errorList){
		super((errorList!=null) ? gson.toJson(errorList) : null);
	}

	public ReleaseRulesReportException(String msg,Throwable e){
		super(msg,e);
	}
	
	public ReleaseRulesReportException(String msg){
		super(msg);
	}
	
}

