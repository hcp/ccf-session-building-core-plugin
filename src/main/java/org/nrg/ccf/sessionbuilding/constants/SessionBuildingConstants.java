package org.nrg.ccf.sessionbuilding.constants;

public class SessionBuildingConstants {
	
	public static final String TOOL_ID = "ccfSessionBuilding";
	public static final String TOOL_NAME = "CCF Session Building Plugin Preferences";
	public static final String TOOL_DESC = "Manages project configuration and settings for the CCF session building plugin";
	public static final String PREFERENCE_VALUE_ENABLED = "ccfSessionBuildingEnabled";
	public static final String PREFERENCE_VALUE_RULESCLASS = "ccfSessionBuildingRulesClass";
	public static final String[] PROPERTIES_EXCLUSIONS = new String[] { "XNAT_CSRF" };
	public enum SessionBuildingReportFormats { JSON, CSV };
	
}
