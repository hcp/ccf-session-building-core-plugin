package org.nrg.ccf.sessionbuilding.enums;

public enum ProcessingType {
	
	BUILD_SESSION, FILE_TRANSFER, GET_XML, GET_REPORT

}
