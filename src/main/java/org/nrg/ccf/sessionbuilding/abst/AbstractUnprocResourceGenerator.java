package org.nrg.ccf.sessionbuilding.abst;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.interfaces.CcfResourceGeneratorI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.CatCatalog;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.helpers.file.StoredFile;
import org.nrg.xnat.helpers.resource.XnatResourceInfo;
import org.nrg.xnat.helpers.resource.direct.DirectExptResourceImpl;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import lombok.extern.slf4j.Slf4j;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
public abstract class AbstractUnprocResourceGenerator implements CcfResourceGeneratorI {
	
	final protected XnatImagesessiondata _combSession;
	final protected List<XnatImagesessiondata> _srcSessions;
	final protected CcfReleaseRulesI _releaseRules;
	final UserI _user;
	public static final String UNPROC_APPEND = "_unproc";
	private DirectExptResourceImpl sessionModifier;
	
	public AbstractUnprocResourceGenerator(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, CcfReleaseRulesI releaseRules, UserI user) {
		super();
		_srcSessions = srcSessions;
		_combSession = combSession;
		_releaseRules = releaseRules;
		_user = user;
	}

	public XnatImagesessiondata getSession() {
		return _combSession;
	}

	public UserI getUser() {
		return _user;
	}
	
	public Collection<? extends XnatImagescandataI> getMatchingBiasScansByType(XnatImagescandataI scan, String[] biasScanTypes) {
		final List<XnatImagescandataI> returnList = new ArrayList<>();
		if (!(scan instanceof XnatMrscandata)) {
			return returnList;
		}
		final XnatMrscandata mrScan = (XnatMrscandata)scan;
		for (final XnatImagescandataI cScan : _combSession.getScans_scan()) {
			if (Arrays.asList(biasScanTypes).contains(cScan.getType())) {
				if (!(cScan instanceof XnatMrscandata)) {
					continue;
				}
				final XnatMrscandata mrcScan = (XnatMrscandata)cScan;
				if (
						(
						mrcScan.getParameters_biasgroup() != null &&
						mrScan.getParameters_biasgroup() != null &&
						mrcScan.getParameters_biasgroup().equals(mrScan.getParameters_biasgroup())
								)
						|| (mrScan.getParameters_biasgroup() == null &&
								mrScan.getId().substring(0,1).equals(mrcScan.getId().substring(0,1)))
				    ) {
					returnList.add(mrcScan);
				}
			}
		}
		return returnList;
	}
	
	public Collection<? extends XnatImagescandataI> getAssociatedStructuralScans(XnatImagescandataI scan) {
		final List<XnatImagescandataI> returnList = new ArrayList<>();
		if (!(scan instanceof XnatMrscandata)) {
			return returnList;
		}
		final XnatMrscandata mrScan = (XnatMrscandata)scan;
		for (final XnatImagescandataI cScan : _combSession.getScans_scan()) {
			if (!mrScan.equals(cScan)) {
				if (!(cScan instanceof XnatMrscandata)) {
					continue;
				}
				// In case we're passing in the normalized scan, let's compress that out.
				if (cScan.getType().startsWith(mrScan.getType().replace("_Norm",""))) {
					returnList.add(cScan);
				}
			}
		}
		return returnList;
	}
	
	public Collection<? extends XnatImagescandataI> getScansMatchingOnSeFieldmapGroup(XnatImagescandataI scan, String[] scanTypes) {
		final List<XnatImagescandataI> returnList = new ArrayList<>();
		if (!(scan instanceof XnatMrscandata)) {
			return returnList;
		}
		final XnatMrscandata mrScan = (XnatMrscandata)scan;
		for (final XnatImagescandataI cScan : _combSession.getScans_scan()) {
			if (Arrays.asList(scanTypes).contains(cScan.getType())) {
				if (!(cScan instanceof XnatMrscandata)) {
					continue;
				}
				final XnatMrscandata mrcScan = (XnatMrscandata)cScan;
				if (
						(mrcScan.getParameters_sefieldmapgroup() != null &&
							mrScan.getParameters_sefieldmapgroup() != null &&
							mrcScan.getParameters_sefieldmapgroup().equals(mrScan.getParameters_sefieldmapgroup())
						)
							|| (mrScan.getParameters_sefieldmapgroup() == null &&
								mrScan.getId().substring(0,1).equals(mrcScan.getId().substring(0,1)))
					) {
					returnList.add(mrcScan);
				}
			}
		}
		return returnList;
	}
	
	public Collection<? extends XnatImagescandataI> getScansMatchingOnGeFieldmapGroup(XnatImagescandataI scan, String[] scanTypes) {
		final List<XnatImagescandataI> returnList = new ArrayList<>();
		if (!(scan instanceof XnatMrscandata)) {
			return returnList;
		}
		final XnatMrscandata mrScan = (XnatMrscandata)scan;
		for (final XnatImagescandataI cScan : _combSession.getScans_scan()) {
			if (Arrays.asList(scanTypes).contains(cScan.getType())) {
				if (!(cScan instanceof XnatMrscandata)) {
					continue;
				}
				final XnatMrscandata mrcScan = (XnatMrscandata)cScan;
				if (
						(mrcScan.getParameters_gefieldmapgroup() != null &&
							mrScan.getParameters_gefieldmapgroup() != null &&
							mrcScan.getParameters_gefieldmapgroup().equals(mrScan.getParameters_gefieldmapgroup())
						)
							|| (mrScan.getParameters_gefieldmapgroup() == null &&
								mrScan.getId().substring(0,1).equals(mrcScan.getId().substring(0,1)))
					) {
					returnList.add(mrcScan);
				}
			}
		}
		return returnList;
	}
	
	public Collection<? extends XnatImagescandataI> getMatchingSefieldmapScansByType(XnatImagescandataI scan, String[] scanTypes) {
		return getScansMatchingOnSeFieldmapGroup(scan,scanTypes);
	}
	
	public Collection<? extends XnatImagescandataI> getMatchingGefieldmapScansByType(XnatImagescandataI scan, String[] scanTypes) {
		return getScansMatchingOnGeFieldmapGroup(scan,scanTypes);
	}
	
	public Collection<? extends XnatImagescandataI> getMatchingSbrefScans(XnatImagescandataI scan) {
		final List<XnatImagescandataI> returnList = new ArrayList<>();
		if (!(scan instanceof XnatMrscandata)) {
			return returnList;
		}
		final XnatMrscandata mrScan = (XnatMrscandata)scan;
		for (final XnatImagescandataI cScan : _combSession.getScans_scan()) {
			if (cScan.getSeriesDescription().equals(scan.getSeriesDescription() + "_SBRef")) {
				if (!(cScan instanceof XnatMrscandata)) {
					continue;
				}
				final XnatMrscandata mrcScan = (XnatMrscandata)cScan;
				if ((mrcScan.getParameters_sefieldmapgroup() != null && 
					mrScan.getParameters_sefieldmapgroup() != null &&
					mrcScan.getParameters_sefieldmapgroup().equals(mrScan.getParameters_sefieldmapgroup())) ||
						(mrcScan.getParameters_sefieldmapgroup() == null && mrScan.getParameters_sefieldmapgroup() == null)) {
					returnList.add(mrcScan);
				}
			}
		}
		return returnList;
	}
	
	public void createSessionUnprocResourcesFromMap(Map<String, List<XnatImagescandataI>> assignMap, String[] includedResources, String[] includedResourceLabels, EventMetaI ci) throws ReleaseResourceException {
		createSessionUnprocResourcesFromMap(assignMap, includedResources, includedResourceLabels, null, ci);
	}

	public void createSessionUnprocResourcesFromMap(Map<String, List<XnatImagescandataI>> assignMap, String[] includedResources, String[] includedResourceLabels, Map<String,List<String>> subdirMap, EventMetaI ci) throws ReleaseResourceException {
		for (final String key : assignMap.keySet()) {
			final List<XnatImagescandataI> scans = assignMap.get(key);
			if (scans.size()<1) {
				continue;
			}
			final XnatResourcecatalog catalog = getOrCreateResource(key + UNPROC_APPEND, ci);
			catalog.clearCountAndSize();
			final String catalogPath = catalog.getCatalogFile(CommonConstants.ROOT_PATH).getParentFile().getAbsolutePath();
			Collections.sort(scans,new Comparator<XnatImagescandataI>(){
				public int compare(XnatImagescandataI s1,XnatImagescandataI s2){
					return s1.getId().compareTo(s2.getId());
				}
			});
			final Map<String,List<String>> fileScanMap = new HashMap<>();
			for (final XnatImagescandataI scan : scans) {
				for (final XnatAbstractresourceI resourceI : scan.getFile()) {
					if (!Arrays.asList(includedResources).contains(resourceI.getLabel()) || !(resourceI instanceof XnatResourcecatalog)) {
						continue;
					}
					final XnatResourcecatalog resource = (XnatResourcecatalog)resourceI;
					// IMPORTANT:  Need to clear out stored file variable, so they're reread.
					resource.clearFiles();
					for (final ResourceFile rFile : resource.getFileResources(CommonConstants.ROOT_PATH)) {
						final File file = rFile.getF();
						String resourcePath = resource.getFullPath(CommonConstants.ROOT_PATH);
						resourcePath = resourcePath.replaceFirst("[/]$", "");
						resourcePath = resourcePath.substring(0,resourcePath.lastIndexOf(File.separator));
						String linkDir = file.getParentFile().getAbsolutePath().replace(resourcePath, "");
						if (Arrays.asList(includedResourceLabels).contains(resourceI.getLabel())) {
							linkDir = resourceI.getLabel() + (linkDir.startsWith(File.separator) ? "" : File.separator) + linkDir;
						}
						if (subdirMap!=null) {
							final String catFilePath = (linkDir.length()>0) ? linkDir + File.separator + file.getName() : file.getName();
							breakto:
							for (final String subdir : subdirMap.keySet()) {
								for (final String matcher : subdirMap.get(subdir)) {
									if (catFilePath.matches(matcher)) {
										linkDir = linkDir + ((linkDir.endsWith(File.separator) || subdir.startsWith(File.separator)) ? subdir : File.separator + subdir);
										break breakto;
									}
								}
							}
						}
						final File linkDirF = new File(FileSystems.getDefault().getPath(catalogPath,linkDir).toString());
						if (!linkDirF.exists()) {
							linkDirF.mkdirs();
						}
						Path linkPath = null;
						Path dest = null;
						String linkPathStr = null;
						try {
							final Path src = FileSystems.getDefault().getPath(file.getAbsolutePath());
							dest = FileSystems.getDefault().getPath(catalogPath,linkDir,file.getName());
							if (dest != null && dest.getParent()!= null && src != null) {
								final Path relSrc = dest.getParent().relativize(src);
								linkPathStr = (linkDir!=null && linkDir.length()>0) ? linkDir + File.separator + file.getName() : file.getName();
								if (!fileScanMap.containsKey(scan.getId())) {
									fileScanMap.put(scan.getId(), new ArrayList<String>());
								}
								fileScanMap.get(scan.getId()).add((linkPathStr.startsWith(File.separator)) ? linkPathStr.substring(1) : linkPathStr);
								linkPath = Files.createSymbolicLink(dest, relSrc);
							}
						} catch (FileAlreadyExistsException e) {
							linkPath = dest;
							log.warn("WARNING:  Skipping unproc resource symlink creation because file already exists (FILE=" + file.getName() + ")");
						} catch (IOException e) {
							throw new ReleaseResourceException("ERROR:  Exception thrown creating symbolic link.",e);
						}
						if (linkPath==null || linkPathStr==null) {
							return;
						}
						final ArrayList<StoredFile> fws = new ArrayList<>();
						fws.add(new StoredFile(new File(linkPath.toString()), false));
						try {
							getSessionModifier(ci).addFile(fws, key + UNPROC_APPEND, null, linkPathStr, new XnatResourceInfo(_user, new Date(), new Date()), false);
						} catch (Exception e1) {
							throw new ReleaseResourceException("ERROR:  Exception thrown saving file to resource.",e1);
						}
					}
				}
			}
			//createFileScansCsv(ci,key,fileScanMap);
			catalog.calculate(CommonConstants.ROOT_PATH);
			catalog.setFileCount(catalog.getCount(CommonConstants.ROOT_PATH));
			catalog.setFileSize(catalog.getSize(CommonConstants.ROOT_PATH));
			try {
				SaveItemHelper.authorizedSave(catalog,_user,false,false,ci);
			} catch (Exception e) {
				// Do nothing, counts just won't be updated
			}
		}
	}
	
	/*
	private void createFileScansCsv(EventMetaI ci, String key, Map<String, List<String>> fileScanMap) {
			final StringBuffer filescans_b = new StringBuffer("'Scan','FilePath'\n");
			File tmpFile = null;
			try {
				tmpFile = File.createTempFile("filescans", ".csv");
				final SortedSet<String> scanKeys = new TreeSet<>(fileScanMap.keySet());
				for (final String scanKey : scanKeys) {
					final List<String> flList = fileScanMap.get(scanKey);
					Collections.sort(flList);
					for (final String fl : flList) {
						filescans_b.append("'" + scanKey + "','" + fl + "'\n");
					}
				}
				FileUtils.writeStringToFile(tmpFile, filescans_b.toString());
				final ArrayList<StoredFile> fws = new ArrayList<>();
				fws.add(new StoredFile(tmpFile, false));
				getSessionModifier(ci).addFile(fws, key + UNPROC_APPEND, null, "filescans.csv", new XnatResourceInfo(_user, new Date(), new Date()), false);
			} catch (Exception e) {
				logger.warn("WARNING:  Couldn't write filescans.csv for catalog " + key + UNPROC_APPEND + ".");
			} finally {
				if (tmpFile!= null && tmpFile.exists()) {
					tmpFile.delete();
				}
			}
	}
	*/

	protected DirectExptResourceImpl getSessionModifier(final EventMetaI ci) {
		if (sessionModifier == null) {
			sessionModifier =  new DirectExptResourceImpl(_combSession.getProjectData(),_combSession,true,_user,ci);
		}
		return sessionModifier;
	}

	private XnatResourcecatalog getOrCreateResource(String resourceLabel, EventMetaI ci) throws ReleaseResourceException {
		
		XnatAbstractresourceI resource =  getSessionModifier(ci).getResourceByLabel(resourceLabel, null);
		if (resource==null) {
			resource = createSessionResource(resourceLabel, ci);
		}
		return (resource instanceof XnatResourcecatalog) ? (XnatResourcecatalog)resource : null;
	}

	private XnatAbstractresourceI createSessionResource(String resourceLabel, EventMetaI ci) throws ReleaseResourceException {
		
		final XnatProjectdata proj = _combSession.getProjectData();
		
		final File sessionDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + _combSession.getLabel() +
					File.separator + "RESOURCES" + File.separator + resourceLabel);
		if (!sessionDir.exists()) {
			sessionDir.mkdirs();
		}
		final File catFile = new File(sessionDir,resourceLabel + CommonConstants.CATXML_EXT);
		final CatCatalog cat = new CatCatalog();
		final XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			final FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			throw new ReleaseResourceException("Couldn't write catalog XML file",e);
		} catch (Exception e) {
			throw new ReleaseResourceException("Couldn't write catalog XML file",e);
		}
		ecat.setLabel(resourceLabel);
		ecat.setFormat("MISC");
		ecat.setContent("MISC");
		// Save resource to session
		//final String eventStr = "Resource " + resourceLabel + " created under session " + _combSession.getId();
		try {
			_combSession.addResources_resource(ecat);
			if (SaveItemHelper.authorizedSave(_combSession,_user,false,true,ci)) {
				return ecat;
			} 
			throw new ReleaseResourceException("ERROR:  Couldn't add resource to session - (SESSION=" + _combSession  +")", new Exception());
		} catch (Exception e) {
			throw new ReleaseResourceException("ERROR:  Couldn't add resource to session - " + e.getMessage(),new Exception());
		}
		
	}
	
}
