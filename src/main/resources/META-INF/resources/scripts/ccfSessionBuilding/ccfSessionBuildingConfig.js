
if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.sessionbuildingconfig === 'undefined') {
	CCF.sessionbuildingconfig = { 	PREF_URL: "/xapi/ccfSessionBuildingPreferences/project/" + XNAT.data.context.project + "/settings",
					CONFIGCLASSES_URL: "/xapi/ccfSessionBuildingPreferences/configClasses",
			 		CONFIG_UI_URL: "/xapi/spawner/resolve/ccfSessionBuildingCorePlugin:projectConfig/configPanel",
			 		updated: false };
}

CCF.sessionbuildingconfig.initialize = function(projectId) {
	if (typeof CCF.sessionbuildingconfig.configuration === 'undefined') {

		$.ajax({
			type : "GET",
			url:serverRoot+CCF.sessionbuildingconfig.CONFIGCLASSES_URL,
			cache: false,
			async: true,
			context: this,
			dataType: 'json'
		 })
		.done( function( data, textStatus, jqXHR ) {
			CCF.sessionbuildingconfig.rulesClasses = {};
			CCF.sessionbuildingconfig.resourceGenerators = {};
			CCF.sessionbuildingconfig.fileHandlerClasses = {};
			//console.log(data);
			var rulesClassLabels = [];
			var rulesClassDups = [];
			var resourceGeneratorLabels = [];
			var resourceGeneratorDups = [];
			var fileHandlerLabels = [];
			var fileHandlerDups = [];
			if (typeof data["rulesClasses"]!=='undefined' && data["rulesClasses"].length>0) {
				var rcl = data["rulesClasses"];
				for (var i=0;i<rcl.length;i++) {
					var className = rcl[i];
					if (typeof className !== 'undefined' && className.length>0) {
						var classLabel = className.substring(className.lastIndexOf('.')+1);
						if ($.inArray(classLabel,rulesClassLabels)>=0 && $.inArray(classLabel,rulesClassDups)<0) {
							rulesClassDups.push(classLabel);
						}
						rulesClassLabels.push(classLabel);
					} 
				}
				for (var i=0;i<rcl.length;i++) {
					var className = rcl[i];
					if (typeof className !== 'undefined' && className.length>0) {
						var classLabel = className;
						if ($.inArray(classLabel,rulesClassDups)<0) {
							CCF.sessionbuildingconfig.rulesClasses["rc" + i] = { label: rulesClassLabels[i], value: rcl[i] };
						} else {
							CCF.sessionbuildingconfig.rulesClasses["rc" + i] = { label: rcl[i], value: rcl[i] };
						}
					}
				}
			}
			if (typeof data["resourceGenerators"]!=='undefined' && data["resourceGenerators"].length>0) {
				var rcl = data["resourceGenerators"];
				for (var i=0;i<rcl.length;i++) {
					var className = rcl[i];
					if (typeof className !== 'undefined' && className.length>0) {
						var classLabel = className.substring(className.lastIndexOf('.')+1);
						if ($.inArray(classLabel,resourceGeneratorLabels)>=0 && $.inArray(classLabel,resourceGeneratorDups)<0) {
							resourceGeneratorDups.push(classLabel);
						}
						resourceGeneratorLabels.push(classLabel);
					} 
				}
				for (var i=0;i<rcl.length;i++) {
					var className = rcl[i];
					if (typeof className !== 'undefined' && className.length>0) {
						var classLabel = className;
						if ($.inArray(classLabel,resourceGeneratorDups)<0) {
							CCF.sessionbuildingconfig.resourceGenerators["rc" + i] = { label: resourceGeneratorLabels[i], value: rcl[i] };
						} else {
							CCF.sessionbuildingconfig.resourceGenerators["rc" + i] = { label: rcl[i], value: rcl[i] };
						}
					}
				}
			}
			if (typeof data["fileHandlerClasses"]!=='undefined' && data["fileHandlerClasses"].length>0) {
				var rcl = data["fileHandlerClasses"];
				for (var i=0;i<rcl.length;i++) {
					var className = rcl[i];
					if (typeof className !== 'undefined' && className.length>0) {
						var classLabel = className.substring(className.lastIndexOf('.')+1);
						if ($.inArray(classLabel,fileHandlerLabels)>=0 && $.inArray(classLabel,fileHandlerDups)<0) {
							fileHandlerDups.push(classLabel);
						}
						fileHandlerLabels.push(classLabel);
					} 
				}
				for (var i=0;i<rcl.length;i++) {
					var className = rcl[i];
					if (typeof className !== 'undefined' && className.length>0) {
						var classLabel = className;
						if ($.inArray(classLabel,fileHandlerDups)<0) {
							CCF.sessionbuildingconfig.fileHandlerClasses["rc" + i] = { label: fileHandlerLabels[i], value: rcl[i] };
						} else {
							CCF.sessionbuildingconfig.fileHandlerClasses["rc" + i] = { label: rcl[i], value: rcl[i] };
						}
					}
				}
			}
			CCF.sessionbuildingconfig.spawnConfig();
		})
		.fail( function( data, textStatus, error ) {
			console.log("WARNING:  The configuration call for the CCF session building core plugin returned an error - (", error,")"); 
			CCF.sessionbuildingconfig.configuration = {};
			$("#session_building_div").html("<h3>ERROR:  Could not retrieve configuration for this plugin</h3>");
		});

	}
}

CCF.sessionbuildingconfig.getRulesClassOptionValues = function() {
	return CCF.sessionbuildingconfig.rulesClasses;
}

CCF.sessionbuildingconfig.getResourceGeneratorOptionValues = function() {
	return CCF.sessionbuildingconfig.resourceGenerators;
}

CCF.sessionbuildingconfig.getFileHandlerOptionValues = function() {
	return CCF.sessionbuildingconfig.fileHandlerClasses;
}

CCF.sessionbuildingconfig.spawnConfig = function() {

		$.ajax({
			type : "GET",
			url:serverRoot+CCF.sessionbuildingconfig.CONFIG_UI_URL,
			cache: false,
			async: true,
			context: this,
			dataType: 'json'
		 })
		.done( function( data, textStatus, jqXHR ) {
			data.configPanel.url = CCF.sessionbuildingconfig.PREF_URL;
			data.configPanel.contents.ccfSessionBuildingRulesClass.options = CCF.sessionbuildingconfig.getRulesClassOptionValues();
			data.configPanel.contents.ccfSessionBuildingSelectableRulesClasses.options = CCF.sessionbuildingconfig.getRulesClassOptionValues();
			data.configPanel.contents.ccfSessionBuildingResourceGen.options = CCF.sessionbuildingconfig.getResourceGeneratorOptionValues();
			data.configPanel.contents.ccfSessionBuildingSelectableResourceGen.options = CCF.sessionbuildingconfig.getResourceGeneratorOptionValues();
			data.configPanel.contents.ccfSessionBuildingFileHandler.options = CCF.sessionbuildingconfig.getFileHandlerOptionValues();
			data.configPanel.contents.ccfSessionBuildingSelectableFileHandlers.options = CCF.sessionbuildingconfig.getFileHandlerOptionValues();
			//console.log(data);
			XNAT.spawner.spawn(data).render($("#session_building_div")[0]);
		})
		.fail( function( data, textStatus, error ) {
			console.log("WARNING:  The configuration call for the CCF session building core plugin returned an error - (", error,")"); 
			CCF.sessionbuildingconfig.configuration = {};
			$("#session_building_div").html("<h3>ERROR:  Could not retrieve configuration for this plugin</h3>");
		});

/*
	function configPanel(contents) {
		return {
			id: 'ccfSessionBuildingPanel',
			kind: 'panel.form',
			label: 'CCF Session Building Core Plugin Configuration',
			header: false,
			method: "POST",
			contentType: "json",
			url: CCF.sessionbuildingconfig.PREF_URL,
			contents: {
				"Enabled": enabled(),
				"Release Rules Class": rulesclass(),
				"Default Build Project": buildProject()
			}
		}
	}
	function enabled() {
		return {
			id: 'ccfSessionBuildingEnabled',
			kind: 'panel.input.switchbox',
			name: 'ccfSessionBuildingEnabled',
			label: 'Enabled?',
			value: true,
			onText: "Enabled",
			offText: "Disabled"
		}
	}
	function rulesclass() {
		return {
			id: 'ccfSessionBuildingRulesClass',
			kind: 'panel.select.init',
			name: 'ccfSessionBuildingRulesClass',
			label: 'Session Building Rules Class',
			options: CCF.sessionbuildingconfig.getRulesClassOptionValues()
		}
	}
	function buildProject() {
		return {
			id: 'ccfSessionBuildingBuildProject',
			kind: 'panel.input.text',
			name: 'ccfSessionBuildingBuildProject',
			label: 'Session Building Build Project',
			value: ''
		}
	}
	return {
		root: configPanel()
	};	
*/

}


