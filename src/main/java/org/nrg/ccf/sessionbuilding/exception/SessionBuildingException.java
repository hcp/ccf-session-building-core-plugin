package org.nrg.ccf.sessionbuilding.exception;

public class SessionBuildingException extends Exception{
	
	private static final long serialVersionUID = -8317054868410646532L;

	public SessionBuildingException(String msg,Throwable e){
		super(msg,e);
	}
	
	public SessionBuildingException(String msg){
		super(msg);
	}
	
}

