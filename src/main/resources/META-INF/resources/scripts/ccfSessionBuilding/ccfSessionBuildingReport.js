//# sourceURL=ccfSessionBuilding/ccfSessionBuildingReport.js


if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.sessionbuildingreport === 'undefined') {
	CCF.sessionbuildingreport = { };
}


if (typeof CCF.sessionbuildingreport.project === 'undefined' || CCF.sessionbuildingreport.project == "") {
   var queryParams = new URLSearchParams(window.location.search);
   CCF.sessionbuildingreport.project = queryParams.get("project");
}

CCF.sessionbuildingreport.initErrorsReportTable = function() {

	$("a[href='#errors-tab']").click(function() {
		setTimeout(function(){
			CCF.sessionbuildingreport.populateRowCount();
		},10);
	});

  $('#sbReport-contents').html("<div id='sb-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='sbErrorsReport-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>" +
         "<div id='sbreports-row-count-container''></div>");

    var columnVar = 
           {
              subjectId: {
                label: 'subjectId',
                filter: true,
            	td: {'className': 'subjectId center rowcount'},
                sort: true
              },
              selectionCriteria: {
                label: 'visit',
                filter: true,
            	td: {'className': 'selectionCriteria center'},
                sort: true
              },
              rulesErrorCode: {
                label: 'srulesErrorCode',
                filter: true,
            	td: {'className': 'rulesErrorCode center'},
                sort: true
              },
              rulesErrorDesc: {
                label: 'srulesErrorDesc',
                filter: true,
            	td: {'className': 'rulesErrorDesc center'},
                sort: true
              }

           }
       ;

 setTimeout(function(){
        $("li[data-tab='qc-summary-tab']").removeClass("active");
        $("#session-building-group").find("li").removeClass("active");
        $("li[data-tab='qc-summary-tab']").addClass("active");
  },100)
  XNAT.xhr.getJSON({
     url: '/xapi/sessionBuildingReports/project/' + CCF.sessionbuildingreport.project +
          '/errorsReport',
     success: function(data) {
	CCF.sessionbuildingreport.failureData = data;
        XNAT.table.dataTable(data, {
           table: {
              id: 'sb-qc-datatable',
              className: 'sb-qc-table highlight'
           },
           width: 'fit-content',
           overflowY: 'hidden',
           overflowX: 'hidden',
           columns: columnVar
        }).render($('#sbErrorsReport-table'))
        $("#sbErrorsReport-table").find(".data-table-wrapper").css("display","table");
     	$("#sb-building-div").html("<div style='width:100%;text-align:right'><a id='sb-download-csv' download='SessionBuildingErrorsReport.csv' onclick='CCF.sessionbuildingreport.doDownload()' type='text/csv'>Download CSV</a></div>");
        setTimeout(function(){
        	CCF.sessionbuildingreport.populateRowCount();
        },100);
        $('input.filter-data').keyup(function() {
            setTimeout(function(){
               CCF.sessionbuildingreport.populateRowCount();
           },50)
        });
/*
        setTimeout(function(){
    		$('.highlight').on("click",function() {
			$(this).closest('tr').find('td').each(function(){
				if ($(this).css("background-color").toString().indexOf('238')>=0) {
					$(this).css("background-color","");
				} else {
					$(this).css("background-color","rgb(238,238,238)");
				}
			});
		});
        },100);
*/
     },
     failure: function(data) {
     	$("#sb-building-div").html("<h3>Failed to build report</h3>");
     }
  });

}

CCF.sessionbuildingreport.doDownload = function() {
	var csv = CCF.sessionbuildingreport.generateCSV();
	var data = new Blob([csv]);
	var ele = document.getElementById("sb-download-csv");
	ele.href = URL.createObjectURL(data);
}

CCF.sessionbuildingreport.generateCSV = function() {

    var dta = CCF.sessionbuildingreport.failureData;
    var result = "SubjectId,Visit,RulesErrorCode,RulesErrorDesc\n";
    dta.forEach(function(obj){
	if (obj.hasOwnProperty("subjectId") && typeof obj["subjectId"] !== 'undefined' && obj["subjectId"]) {
        	result += ((obj["subjectId"].indexOf(",")<0) ? obj["subjectId"] : "\"" + obj["subjectId"].replace('"','\\"') + "\"");
	}
        result += ",";
	if (obj.hasOwnProperty("selectionCriteria") && typeof obj["selectionCriteria"] !== 'undefined' && obj["selectionCriteria"]) {
        	result += ((obj["selectionCriteria"].indexOf(",")<0) ? obj["selectionCriteria"] : "\"" + obj["selectionCriteria"].replace('"','\\"') + "\"");
	}
        result += ",";
	if (obj.hasOwnProperty("rulesErrorCode") && typeof obj["rulesErrorCode"] !== 'undefined' && obj["rulesErrorCode"]) {
        	result += ((obj["rulesErrorCode"].indexOf(",")<0) ? obj["rulesErrorCode"] : "\"" + obj["rulesErrorCode"].replace('"','\\"') + "\"");
	}
        result += ",";
	if (obj.hasOwnProperty("rulesErrorDesc") && typeof obj["rulesErrorDesc"] !== 'undefined' && obj["rulesErrorDesc"]) {
        	result += ((obj["rulesErrorDesc"].indexOf(",")<0) ? obj["rulesErrorDesc"] : "\"" + obj["rulesErrorDesc"].replace('"','\\"') + "\"");
	}
        result += "\n";
    });

    return result;

}


CCF.sessionbuildingreport.populateRowCount = function() {
   var rowCount = ($('.rowcount').closest('tr').filter(":visible")).length;
    if ($("#sb-building-div").find('img').length<1) {
    	$('#sbreports-row-count-container').html("<em>" + rowCount + " rows match query filters.</em>");
    } else {
    	$('#sbreports-row-count-container').empty();
    }
}



