package org.nrg.ccf.sessionbuilding.pojo;

import java.util.ArrayList;
import java.util.List;

import org.nrg.xdat.om.XnatMrsessiondata;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
public class RulesProcessingResults {
	
	private @NonNull Boolean success;
	private @NonNull String status;
	private List<String> warningList = new ArrayList<>();
	private Exception caughtException = null;
	
}
