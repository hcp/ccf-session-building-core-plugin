
if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.sessionbuilding === 'undefined') {
	CCF.sessionbuilding = { 	
					// NOTE HERE:  XNAT.data.context.projectName seems to be the project ID of the page the user is on.  If the subject is shared, the other project
					// contexts are for the owning project of the subject.
					PREF_URL: "/xapi/ccfSessionBuildingPreferences/project/" + XNAT.data.context.projectName + "/subject/" + XNAT.data.context.subjectID + "/settings",
					SESSION_BUILD_URL : '/xapi/projects/' + XNAT.data.context.projectName + '/subjects/' +  XNAT.data.context.subjectID + '/ccfSessionBuilding/buildSession',
					FILE_TRANSFER_URL : '/xapi/projects/' + XNAT.data.context.projectName + '/subjects/' +  XNAT.data.context.subjectID + '/ccfSessionBuilding/transferFiles',
					SESSION_REPORT_URL : '/xapi/projects/' + XNAT.data.context.projectName + '/subjects/' +  XNAT.data.context.subjectID + '/ccfSessionBuilding/getSessionBuildingReport',
					ALL_SESSION_REPORT_URL : '/xapi/projects/' + XNAT.data.context.projectName + '/ccfSessionBuilding/getSessionBuildingReport'
			 	};
}

CCF.sessionbuilding.initialize = function(projectId) {
	if (typeof CCF.sessionbuilding.configuration === 'undefined') {

		$.ajax({
			type : "GET",
			url:serverRoot+CCF.sessionbuilding.PREF_URL,
			cache: false,
			async: true,
			context: this,
			dataType: 'json'
		 })
		.done( function( data, textStatus, jqXHR ) {
			var enabled = data['ccfSessionBuildingEnabled'];
			if (typeof enabled !== 'undefined' && enabled == 'true') {
				CCF.sessionbuilding.configuration = data;
				$(".sessionBuildingLink").show();
			} else {
				$(".sessionBuildingLink").hide();
			} 
		})
		.fail( function( data, textStatus, error ) {
			console.log("WARNING:  The configuration call for the CCF session building core plugin returned an error - (", error,")"); 
			CCF.sessionbuilding.configuration = {};
			$(".sessionBuildingLink").hide();
		});

	}
}


CCF.sessionbuilding.spawnOptions = function(buildSession) {

	function getRulesClassOptions() {
		var config = CCF.sessionbuilding.configuration;
		var returnOpts = [];
		if (typeof config.ccfSessionBuildingSelectableRulesClasses !== 'undefined' && config.ccfSessionBuildingSelectableRulesClasses.length>0) {
			for (var i=0;i<config.ccfSessionBuildingSelectableRulesClasses.length;i++) {
				var rulesClass = config.ccfSessionBuildingSelectableRulesClasses[i];
				var rulesLabel = rulesClass.substring(rulesClass.lastIndexOf('.')+1);
				returnOpts.push({ label: rulesLabel, value: rulesClass }); 
			}
		} else {
			var rulesClass = config.ccfSessionBuildingRulesClass;
			var rulesLabel = rulesClass.substring(rulesClass.lastIndexOf('.')+1);
			returnOpts.push({ label: rulesLabel, value: rulesClass, selected: true }); 
		}
		return returnOpts;
	}


	function getResourceGeneratorOptions() {
		var config = CCF.sessionbuilding.configuration;
		var returnOpts = [];
		if (typeof config.ccfSessionBuildingSelectableResourceGen !== 'undefined' && config.ccfSessionBuildingSelectableResourceGen.length>0) {
			for (var i=0;i<config.ccfSessionBuildingSelectableResourceGen.length;i++) {
				var rulesClass = config.ccfSessionBuildingSelectableResourceGen[i];
				var rulesLabel = rulesClass.substring(rulesClass.lastIndexOf('.')+1);
				returnOpts.push({ label: rulesLabel, value: rulesClass }); 
			}
		} else {
			var rulesClass = config.ccfSessionBuildingResourceGen;
			var rulesLabel = rulesClass.substring(rulesClass.lastIndexOf('.')+1);
			returnOpts.push({ label: rulesLabel, value: rulesClass }); 
		}
		return returnOpts;
	}

	function getFileHandlerOptions() {
		var config = CCF.sessionbuilding.configuration;
		var returnOpts = [];
		if (typeof config.ccfSessionBuildingSelectableFileHandlers !== 'undefined' && config.ccfSessionBuildingSelectableFileHandlers.length>0) {
			for (var i=0;i<config.ccfSessionBuildingSelectableFileHandlers.length;i++) {
				var rulesClass = config.ccfSessionBuildingSelectableFileHandlers[i];
				var rulesLabel = rulesClass.substring(rulesClass.lastIndexOf('.')+1);
				returnOpts.push({ label: rulesLabel, value: rulesClass }); 
			}
		} else {
			var rulesClass = config.ccfSessionBuildingFileHandler;
			var rulesLabel = rulesClass.substring(rulesClass.lastIndexOf('.')+1);
			returnOpts.push({ label: rulesLabel, value: rulesClass }); 
		}
		return returnOpts;
	}

	function configPanel(contents) {
		if (buildSession) {
			return {
				id: 'ccfSessionBuildingPanel',
				kind: 'panel.form',
				width: '640px',
				height: '530px',
				label: 'CCF Session Building Core Plugin Configuration',
				header: false,
				footer: false,
				contents: {
					"Release Rules Class": rulesclass(),
					"Release Rules Parameters": rulesparameters(),
					"File Handler": filehandler(),
					"Resource Generator": resourcegenerator(),
					"Destination Project": destinationProject(),
					"Session Label": sessionLabel(),
					"Error Override": errorOverride(),
					"Treat Override As Warning?": overrideAsWarning(),
					"Overwrite Existing": overwriteExisting(true),
					"Skip Rules Processing": skipRulesProcessing()
				}
			}
		} else {
			return {
				id: 'ccfSessionBuildingPanel',
				kind: 'panel.form',
				width: '640px',
				height: '530px',
				label: 'CCF Session Building Core Plugin Configuration',
				header: false,
				footer: false,
				contents: {
					"Release Rules Class": rulesclass(),
					"Release Rules Parameters": rulesparameters(),
					"File Handler": filehandler(),
					"Resource Generator": resourcegenerator(),
					"Destination Project": destinationProject(),
					"Session Label": sessionLabel()
					//,"Overwrite Existing": overwriteExisting(false)
				}
			}
		}
	}
	function rulesclass() {
		return {
			id: 'ccfSessionBuildingRulesClass',
			kind: 'panel.select.init',
			name: 'ccfSessionBuildingRulesClass',
			label: 'Release Rules Class',
			description: 'Validates source-side sessions and applies rules used to build the destination session.',
			options: getRulesClassOptions()
		}
	}
	function rulesparameters() {
		return {
			id: 'ccfSessionBuildingRulesClassParameters',
			kind: 'panel',
			header: false,
			footer: false,
			name: 'ccfSessionBuildingRulesClassParameters'
		}
	}
	function resourcegenerator() {
		return {
			id: 'ccfSessionBuildingResourceGenerator',
			kind: 'panel.select.init',
			name: 'ccfSessionBuildingResourceGenerator',
			label: 'Resource Generator',
			description: 'Builds additional scan- or session-level resources.',
			options: getResourceGeneratorOptions()
		}
	}
	function filehandler() {
		return {
			id: 'ccfSessionBuildingFileHandler',
			kind: 'panel.select.init',
			name: 'ccfSessionBuildingFileHandler',
			laber: 'File Handler',
			description: 'Transfers files from source sessions to destination session.',
			options: getFileHandlerOptions()
		}
	}
	function destinationProject() {
		return {
			id: 'ccfSessionBuildingBuildProject',
			kind: 'panel.input.text',
			name: 'ccfSessionBuildingBuildProject',
			label: 'Destination Project',
			value: ((typeof CCF.sessionbuilding.configuration.ccfSessionBuildingBuildProject !== 'undefined') ? CCF.sessionbuilding.configuration.ccfSessionBuildingBuildProject : "")
		}
	}
	function sessionLabel() {
		return {
			id: 'sessionLabel',
			kind: 'panel.input.text',
			name: 'sessionLabel',
			label: 'Session Label',
			value: ((typeof CCF.sessionbuilding.configuration.sessionLabel !== 'undefined') ? CCF.sessionbuilding.configuration.sessionLabel : "")
		}
	}
	function skipRulesProcessing() {
		return {
			id: 'skipRulesProcessing',
			kind: 'panel.input.checkbox',
			name: 'skipRulesProcessing',
			label: 'Skip Rules Processing',
			description: 'This option skips the rules processing and uses the values already set in the scans.  Care must be taken when using this' +
					' option to ensure that the desired rules have been applied to the scans, however this option can be useful ' +
					' when the values set during the rules generation process have to be manually overridden.  This might be ' +
					' the case, for example, when a scan\'s shim values don\'t quite match those of the fieldmap scans but ' +
					' are close enough that the fieldmaps can be applied to the scan, making it usable. ',
			value: false
		}
	}
	function errorOverride() {
		return {
			id: 'errorOverride',
			kind: 'panel.input.checkbox',
			name: 'errorOverride',
			label: 'Error Override',
			description: 'This option will attempt to override rules errors and build sessions in spite of raised errors.  This can override validation type errors, however' +
					' some other types of errors, such as lack of structural scan quality ratings, may still prevent sessions from being built.',
			value: false
		}
	}
	function overrideAsWarning() {
		return {
			id: 'overrideAsWarning',
			kind: 'panel.input.checkbox',
			name: 'overrideAsWarning',
			label: 'Treat Override As Warning?',
			description: 'Indicates that override is not to be treated as an error condition and the session should proceed as normal (i.e. without QC failureflags, etc.).',
			value: false
		}
	}
	function overwriteExisting(buildSession) {
		if (buildSession) {
			return {
				id: 'overwriteExisting',
				kind: 'panel.input.checkbox',
				name: 'overwriteExisting',
				label: 'Overwrite Existing Session',
				description: 'Overwrite existing destination session, if one exists.',
				value: false
			}
		} else {
			return {
				id: 'overwriteExisting',
				kind: 'panel.input.checkbox',
				name: 'overwriteExisting',
				label: 'Overwrite Existing Files?',
				description: 'Overwrite existing files if they exist.  Otherwise, only new files will be sent.',
				value: false
			}
		} 
	}
	return {
		root: configPanel()
	};	

}

CCF.sessionbuilding.buildSession = function(subjectLabel) {

	var confirmModal = xModalConfirm;
	confirmModal.okClose = false,
	confirmModal.okAction = function(){ 
		var confirmUrl = serverRoot+CCF.sessionbuilding.SESSION_BUILD_URL + "?rulesClass=" + 
				$("#ccfSessionBuildingRulesClass").val() + "&resourceGenerator=" + $("#ccfSessionBuildingResourceGenerator").val() +
				"&fileHandler=" + $("#ccfSessionBuildingFileHandler").val() + "&buildProject=" + $("#ccfSessionBuildingBuildProject").val() + 
				"&errorOverride=" + $("#errorOverride").is(":checked") + 
				"&overrideAsWarning=" + $("#overrideAsWarning").is(":checked") +
				"&skipRulesProcessing=" + $("#skipRulesProcessing").is(":checked") + 
				"&overwriteExisting=" + $("#overwriteExisting").is(":checked") +
				"&sessionLabel=" + $("#sessionLabel").val()
			;
		$(".body.scroll").scrollTop(0);
		$(".body.scroll").css("overflow","hidden");
		confirmUrl = confirmUrl + CCF.sessionbuilding.addParameters(confirmUrl);
		$("#confirm-modal-overlay").show();
		$("#confirm-modal-overlay").show();
		$("#buildSessionConfirmModal-cancel-button").hide();
		// NOTE HERE:  XNAT.data.context.projectName seems to be the project ID of the page the user is on.  If the subject is shared, the other project
		// contexts are for the owning project of the subject.
		$.ajax({
			type : "POST",
			url:confirmUrl,
			cache: false,
			async: true,
			context: this,
			dataType: 'text'
		 })
		.done( function( data, textStatus, jqXHR ) {
 			var msg = (($("#skipRulesProcessing").is(":checked")) ?
 				"<h2 style='color:#228822'>Validation checks skipped per request.  Session sent to builder.</h2>" + "<br><em>DETAILS:</em><br><br>" + data
					:
 				"<h2 style='color:#228822'>Validation checks successful.  Session sent to builder.</h2>" + "<br><em>DETAILS:</em><br><br>" + data
					);
			
			$(".body.scroll").css("overflow","auto");
			$("#confirm-modal-overlay").hide();
			xmodal.message({
				title:  "Combined Session Building Results",
				width:  '800px',
				height:  '630px',
				content:  msg
			});
			xmodal.close('buildSessionConfirmModal');
		})
		.fail( function( data, textStatus, error ) {
			var responseText = data.responseText;
			$(".body.scroll").css("overflow","auto");
			try {
				var parseResponseText = JSON.parse(responseText);
				if (parseResponseText.constructor === Array) {
					responseText = "";
					for (var i=0;i<parseResponseText.length;i++) {
						responseText = responseText + "<li>" + parseResponseText[i] + "</li>";
					}
				}
			} catch (e) {
				// Do nothing
			}
 			var errMsg = "<h2 style='color:#AA1111'>Could not build session</h2><em>REASON:</em> &nbsp;" +  error + "<br><br><em>DETAILS:</em><br><br>" + responseText;
			
			$("#confirm-modal-overlay").hide();
			xmodal.message({
				title:  "Combined Session Building Results",
				width:  '800px',
				height:  '630px',
				content:  errMsg
			});
			xmodal.close('buildSessionConfirmModal');
		});
	 }

	confirmModal.id  = 'buildSessionConfirmModal';
	confirmModal.content  = '<h2 style="margin-bottom:10px;margin-top:10px">Build the combined session (SUBJECT=' + subjectLabel + ')?</h2>';
	confirmModal.cancelAction = function(){ return; };
	confirmModal.title = "Combined Session Builder";
	confirmModal.width = 800;
	confirmModal.height = 630;
	xModalOpenNew(confirmModal)
 	//console.log(CCF.sessionbuilding.configuration);
 	XNAT.spawner.spawn(CCF.sessionbuilding.spawnOptions(true)).render($("#buildSessionConfirmModal").find(".body").find(".inner"));
 	$("#buildSessionConfirmModal").find(".body").find(".inner").append(
		"<div id='confirm-modal-overlay' style='width:100%;height:100%;display:none;background-color:#FFFFFF;" +
		"z-index:10;opacity:0.9;margin:0px;position:absolute;top:0px;left:0px;padding:0px'>" + 
		"<div style='color:#22AA22;width:100%'><h3 style='text-align:center'>Checking rules and requirements for this session. &nbsp;Please wait...</h3</div>" +
		"<div style='top:50%;left:50%;margin-right: -50%;transform: translate(-50%, -50%);position:absolute;opacity:1.0;'><img src='/images/loading.gif'/></div>" +
		"</div>");
	$("#ccfSessionBuildingRulesClass").find("option[value='" + CCF.sessionbuilding.configuration.ccfSessionBuildingRulesClass + "']").prop('selected', true); 
	$("#ccfSessionBuildingResourceGenerator").find("option[value='" + CCF.sessionbuilding.configuration.ccfSessionBuildingResourceGen + "']").prop('selected', true); 
	$("#ccfSessionBuildingFileHandler").find("option[value='" + CCF.sessionbuilding.configuration.ccfSessionBuildingFileHandler + "']").prop('selected', true); 
	//console.log(CCF.sessionbuilding.configuration);
	CCF.sessionbuilding.updateParameters();
	$("#ccfSessionBuildingRulesClass").change(function() { CCF.sessionbuilding.updateParameters(); } );
	//$("#errorOverride").change(function() { CCF.sessionbuilding.errorOverrideChange(); } );
}


CCF.sessionbuilding.transferFiles = function(subjectLabel) {

	var confirmModal = xModalConfirm;
	confirmModal.okClose = false,
	confirmModal.okAction = function(){ 
		var confirmUrl = serverRoot+CCF.sessionbuilding.FILE_TRANSFER_URL + "?rulesClass=" + 
				$("#ccfSessionBuildingRulesClass").val() + "&resourceGenerator=" + $("#ccfSessionBuildingResourceGenerator").val() +
				"&fileHandler=" + $("#ccfSessionBuildingFileHandler").val() + "&buildProject=" + $("#ccfSessionBuildingBuildProject").val() + 
				"&overwriteExisting=" + $("#overwriteExisting").is(":checked") +
				"&sessionLabel=" + $("#sessionLabel").val()
			;
		$(".body.scroll").scrollTop(0);
		$(".body.scroll").css("overflow","hidden");
		confirmUrl = confirmUrl + CCF.sessionbuilding.addParameters(confirmUrl);
		$("#confirm-modal-overlay").show();
		$("#confirm-modal-overlay").show();
		$("#fileTransferConfirmModal-cancel-button").hide();
		// NOTE HERE:  XNAT.data.context.projectName seems to be the project ID of the page the user is on.  If the subject is shared, the other project
		// contexts are for the owning project of the subject.
		$.ajax({
			type : "POST",
			url:confirmUrl,
			cache: false,
			async: true,
			context: this,
			dataType: 'text'
		 })
		.done( function( data, textStatus, jqXHR ) {
 			var msg = "<h2 style='color:#228822'>Session sent to file transfer handler.</h2>" + "<br><em>DETAILS:</em><br><br>" + data;
			$(".body.scroll").css("overflow","auto");
			$("#confirm-modal-overlay").hide();
			xmodal.message({
				title:  "File Transfer Results",
				width:  '800px',
				height:  '630px',
				content:  msg
			});
			xmodal.close('fileTransferConfirmModal');
		})
		.fail( function( data, textStatus, error ) {
			var responseText = data.responseText;
			$(".body.scroll").css("overflow","auto");
			try {
				var parseResponseText = JSON.parse(responseText);
				if (parseResponseText.constructor === Array) {
					responseText = "";
					for (var i=0;i<parseResponseText.length;i++) {
						responseText = responseText + "<li>" + parseResponseText[i] + "</li>";
					}
				}
			} catch (e) {
				// Do nothing
			}
 			var errMsg = "<h2 style='color:#AA1111'>Could not transfer files</h2><em>REASON:</em> &nbsp;" +  error + "<br><br><em>DETAILS:</em><br><br>" + responseText;
			
			$("#confirm-modal-overlay").hide();
			xmodal.message({
				title:  "File Transfer Results",
				width:  '800px',
				height:  '630px',
				content:  errMsg
			});
			xmodal.close('fileTransferConfirmModal');
		});
	 }

	confirmModal.id  = 'fileTransferConfirmModal';
	confirmModal.content  = '<h3 style="margin-bottom:10px;margin-top:10px">Transfer session files / Build unproc resources (SUBJECT=' + subjectLabel + ')?</h3>';
	confirmModal.cancelAction = function(){ return; };
	confirmModal.title = "File Transfer";
	confirmModal.width = 800;
	confirmModal.height = 630;
	xModalOpenNew(confirmModal)
 	//console.log(CCF.sessionbuilding.configuration);
 	XNAT.spawner.spawn(CCF.sessionbuilding.spawnOptions(false)).render($("#fileTransferConfirmModal").find(".body").find(".inner"));
 	$("#fileTransferConfirmModal").find(".body").find(".inner").append(
		"<div id='confirm-modal-overlay' style='width:100%;height:100%;display:none;background-color:#FFFFFF;" +
		"z-index:10;opacity:0.9;margin:0px;position:absolute;top:0px;left:0px;padding:0px'>" + 
		"<div style='color:#22AA22;width:100%'><h3 style='text-align:center'>Checking session. &nbsp;Please wait...</h3</div>" +
		"<div style='top:50%;left:50%;margin-right: -50%;transform: translate(-50%, -50%);position:absolute;opacity:1.0;'><img src='/images/loading.gif'/></div>" +
		"</div>");
	$("#ccfSessionBuildingRulesClass").find("option[value='" + CCF.sessionbuilding.configuration.ccfSessionBuildingRulesClass + "']").prop('selected', true); 
	$("#ccfSessionBuildingResourceGenerator").find("option[value='" + CCF.sessionbuilding.configuration.ccfSessionBuildingResourceGen + "']").prop('selected', true); 
	$("#ccfSessionBuildingFileHandler").find("option[value='" + CCF.sessionbuilding.configuration.ccfSessionBuildingFileHandler + "']").prop('selected', true); 
	//console.log(CCF.sessionbuilding.configuration);
	CCF.sessionbuilding.updateParameters();
	$("#ccfSessionBuildingRulesClass").change(function() { CCF.sessionbuilding.updateParameters(); } );
}

CCF.sessionbuilding.updateParameters = function() {

	var rulesClass = $("#ccfSessionBuildingRulesClass").val();
	var rulesClassParams = CCF.sessionbuilding.configuration.ccfSessionBuildingRulesClassParameters[rulesClass];
	$("#ccfSessionBuildingRulesClassParameters-panel").find(".panel-body").html("")
	if (typeof rulesClassParams !== undefined && rulesClassParams.length > 0) {
		var rulesClassParamsVal = JSON.parse(rulesClassParams);
 		XNAT.spawner.spawn(rulesClassParamsVal).render($("#ccfSessionBuildingRulesClassParameters-panel").find(".panel-body"));
	}

}

/*
// This is not currently used.  overrideAsWarning should always default to false
CCF.sessionbuilding.errorOverrideChange = function() {

	var errorOverride = $("#errorOverride").val();
	$("#overrideAsWarning").prop("checked",  !(errorOverride == "true"));

}
*/

CCF.sessionbuilding.addParameters = function(urlString) {

	var addParams = "";
	var rulesClass = $("#ccfSessionBuildingRulesClass").val();
	var rulesClassParams = CCF.sessionbuilding.configuration.ccfSessionBuildingRulesClassParameters[rulesClass];
	if (typeof rulesClassParams !== undefined && rulesClassParams.length > 0) {
		var rulesClassParamsVal = JSON.parse(rulesClassParams);
		for (key in rulesClassParamsVal) {
			if (rulesClassParamsVal.hasOwnProperty(key)) {
				var paramId = rulesClassParamsVal[key]["id"]
				if (typeof paramId == 'undefined' || paramId == '') {
					continue;
				}
				var paramVal = $("#" + paramId).val();
				if (typeof paramVal !== 'undefined' && paramVal !== '') {
					addParams = addParams + "&" + paramId + "=" + paramVal; 
				}
			}
		}
	}
	return addParams;

}

CCF.sessionbuilding.spawnReport= function(allSubjects) {

	function getRulesClassOptions() {
		var config = CCF.sessionbuilding.configuration;
		var returnOpts = [];
		if (typeof config.ccfSessionBuildingSelectableRulesClasses !== 'undefined' && config.ccfSessionBuildingSelectableRulesClasses.length>0) {
			for (var i=0;i<config.ccfSessionBuildingSelectableRulesClasses.length;i++) {
				var rulesClass = config.ccfSessionBuildingSelectableRulesClasses[i];
				var rulesLabel = rulesClass.substring(rulesClass.lastIndexOf('.')+1);
				returnOpts.push({ label: rulesLabel, value: rulesClass }); 
			}
		} else {
			var rulesClass = config.ccfSessionBuildingRulesClass;
			var rulesLabel = rulesClass.substring(rulesClass.lastIndexOf('.')+1);
			returnOpts.push({ label: rulesLabel, value: rulesClass, selected: true }); 
		}
		return returnOpts;
	}

	function configPanel(contents) {
		return {
			id: 'ccfSessionBuildingReportPanel',
			kind: 'panel.form',
			width: '600px',
			height: '300px',
			label: 'CCF Session Building Report',
			header: false,
			footer: false,
			contents: {
				"Release Rules Class": rulesclass(),
				"Release Rules Parameters": rulesparameters(),
				"Report Type": reportFormat()
			}
		}
	}
	function rulesclass() {
		return {
			id: 'ccfSessionBuildingRulesClass',
			kind: 'panel.select.init',
			name: 'ccfSessionBuildingRulesClass',
			label: 'Release Rules Class',
			description: 'Rules class used to generate combined session to generate report for.',
			options: getRulesClassOptions()
		}
	}
	function rulesparameters() {
		return {
			id: 'ccfSessionBuildingRulesClassParameters',
			kind: 'panel',
			header: false,
			footer: false,
			name: 'ccfSessionBuildingRulesClassParameters'
		}
	}
	function reportFormat() {
		return {
			id: 'ccfSessionBuildingReport',
			kind: 'panel.element',
			name: 'ccfSessionBuildingReport',
			value: 'CSV',
			label: 'Report Format',
			contents: ((allSubjects == true) ? 
				{
					"EMAIL": reportFormatEmail(),
				} :
				{
					"CSV": reportFormatCSV(),
					"JSON": reportFormatJSON()
				}
			)
		}
	}
	function reportFormatEmail() {
		return {
			id: 'ccfSessionBuildingReportType',
			kind: 'input.radio',
			name: 'ccfSessionBuildingReportType',
			value: 'EMAIL',
			label: 'Report FormatXX',
			after: "<span style='margin-right:15px'>E-mail</span>"
		}
	}
	function reportFormatCSV() {
		return {
			id: 'ccfSessionBuildingReportType',
			kind: 'input.radio',
			name: 'ccfSessionBuildingReportType',
			value: 'CSV',
			label: 'Report FormatXX',
			after: "<span style='margin-right:15px'>Download</span>"
		}
	}
	function reportFormatJSON() {
		return {
			id: 'ccfSessionBuildingReportType',
			kind: 'input.radio',
			name: 'ccfSessionBuildingReportType',
			value: 'JSON',
			after: "<span>Display</span>",
		}
	}
/*
	function reportFormat() {
		return {
			id: 'ccfSessionBuildingReportType',
			kind: 'panel.select.init',
			name: 'ccfSessionBuildingReportType',
			label: 'Report Type',
			options: [ { label: "Download CSV Report", value: "CSV", selected: true },
			           { label: "Display Report Table", value: "JSON", selected: false } ]
		}
	}
*/
	return {
		root: configPanel()
	};	

}



CCF.sessionbuilding.sessionReport = function(subjectLabel) {

	var confirmModal = xModalConfirm;
	confirmModal.okClose = false,
	confirmModal.okAction = function(){ 
		var reportFormat = $("input[name=ccfSessionBuildingReportType]:checked").val();
		var confirmUrl = serverRoot+CCF.sessionbuilding.SESSION_REPORT_URL + "?rulesClass=" + 
				$("#ccfSessionBuildingRulesClass").val() + "&reportFormat=" + reportFormat
			;
		confirmUrl = confirmUrl + CCF.sessionbuilding.addParameters(confirmUrl);
		$("#report-modal-overlay").show();
		$("#reportSessionConfirmModal-cancel-button").hide();
		// TODO:  For CSV reports, we're basically calling the report generation twice.  Once to see if we'll get a successful report and get 
		// a decent error display, then again using window.location to actually download the report.  Is there a better way???  
		$.ajax({
			type : "GET",
			url:confirmUrl,
			cache: false,
			async: true,
			context: this,
			dataType: 'text'
		 })
		.done( function( data, textStatus, jqXHR ) {
			if (reportFormat=="CSV") {
				xmodal.close('reportSessionConfirmModal');
				XNAT.ui.banner.top(2000, "Building report file.  Your download should begin shortly");
				window.location.href=confirmUrl;
			} else {
				var dataJSON = JSON.parse(data);
				var tableStr = "<div style='height:500px; width:930px; overflow:auto;'><table style='font-size: 11px; border-width:1px; border-style:solid; border-collapse: collapse'>";
				for (var i=0; i<dataJSON.length; i++) {
					tableStr+="<tr style='border-width:1px; border-style:solid; border-collapse: collapse'>";
					for (var j=0; j<dataJSON[i].length; j++) {
						if (i>0) {
							tableStr+="<td style='padding-left:5px;padding-right:5px;border-width:1px; border-style:solid; border-collapse: collapse'>" + dataJSON[i][j] + "</td>";
						} else {
							tableStr+="<th style='padding-left:5px;padding-right:5px;background-color:#EEEEEE;border-width:1px; border-style:solid; border-collapse: collapse'>" + dataJSON[i][j] + "</th>";
						}
					}
					tableStr+="</tr>";
				}
				tableStr+="</table></div>";
	 			var msg = "<h2 style='color:#228822'>Session Report (SUBJECT=" + subjectLabel +")</h2>" + "<br><br>" + tableStr;
				
				$("#report-modal-overlay").hide();
				xmodal.message({
					title:  "Combined Session Building Report",
					width:  '1000px',
					height:  '800px',
					content:  msg
				});
				xmodal.close('reportSessionConfirmModal');
			}
		})
		.fail( function( data, textStatus, error ) {
			var responseText = data.responseText;
			try {
				var parseResponseText = JSON.parse(responseText);
				if (parseResponseText.constructor === Array) {
					responseText = "";
					for (var i=0;i<parseResponseText.length;i++) {
						responseText = responseText + "<li>" + parseResponseText[i] + "</li>";
					}
				}
			} catch (e) {
				// Do nothing
			}
	 		var errMsg = "<h2 style='color:#AA1111'>Could not generate report</h2><em>REASON:</em> &nbsp;" +  error + "<br><br><em>DETAILS:</em><br><br>" + responseText;
			
			$("#report-modal-overlay").hide();
			xmodal.message({
				title:  "Combined Session Building Results",
				width:  '800px',
				height:  '630px',
				content:  errMsg
			});
			xmodal.close('reportSessionConfirmModal');
		});
	 }

	confirmModal.id  = 'reportSessionConfirmModal';
	confirmModal.content  = '<h2 style="margin-bottom:10px;margin-top:10px">Generate Session Report (SUBJECT=' + subjectLabel + ')?</h2>';
	confirmModal.cancelAction = function(){ return; };
	confirmModal.title = "Combined Session Builder";
	confirmModal.width = 650;
	confirmModal.height = 400;
	xModalOpenNew(confirmModal)
 	//console.log(CCF.sessionbuilding.configuration);
 	XNAT.spawner.spawn(CCF.sessionbuilding.spawnReport(false)).render($("#reportSessionConfirmModal").find(".body").find(".inner"));
 	$("#reportSessionConfirmModal").find(".body").find(".inner").append(
		"<div id='report-modal-overlay' style='width:100%;height:100%;display:none;background-color:#FFFFFF;" +
		"z-index:10;opacity:0.9;margin:0px;position:absolute;top:0px;left:0px;padding:0px'>" + 
		"<div style='color:#22AA22;width:100%'><h3 style='text-align:center'>Generating session report. &nbsp;Please wait...</h3</div>" + 
		"<div style='top:50%;left:50%;margin-right: -50%;transform: translate(-50%, -50%);position:absolute;opacity:1.0;'><img src='/images/loading.gif'/></div>" +
		"</div><iframe id='download_iframe' style='display:none;'></iframe>");
	$("#ccfSessionBuildingRulesClass").find("option[value='" + CCF.sessionbuilding.configuration.ccfSessionBuildingRulesClass + "']").prop('selected', true); 
	//$("#ccfSessionBuildingReportType").find("option[value='CSV']").prop('selected', true); 
	$($("#ccfSessionBuildingReportType")[0]).attr('checked', true); 
	CCF.sessionbuilding.updateParameters();
	$("#ccfSessionBuildingRulesClass").change(function() { CCF.sessionbuilding.updateParameters(); } );

}


CCF.sessionbuilding.allSubjectsSessionReport = function(subjectLabel) {

	var confirmModal = xModalConfirm;
	confirmModal.okClose = false,
	confirmModal.okAction = function(){ 
		var reportFormat = $("input[name=ccfSessionBuildingReportType]:checked").val();
		var confirmUrl = serverRoot+CCF.sessionbuilding.ALL_SESSION_REPORT_URL + "?rulesClass=" + 
				$("#ccfSessionBuildingRulesClass").val() + "&reportFormat=" + reportFormat
			;
		confirmUrl = confirmUrl + CCF.sessionbuilding.addParameters(confirmUrl);
		$("#report-modal-overlay").show();
		$("#reportSessionConfirmModal-cancel-button").hide();
		// TODO:  For CSV reports, we're basically calling the report generation twice.  Once to see if we'll get a successful report and get 
		// a decent error display, then again using window.location to actually download the report.  Is there a better way???  
		$.ajax({
			type : "GET",
			url:confirmUrl,
			cache: false,
			async: true,
			context: this,
			dataType: 'text'
		 })
		.done( function( data, textStatus, jqXHR ) {
			xmodal.closeAll();
	 		var msg = "<h3>Request Sent</h3><br>" +
				"Your CSV report generation request has been sent. Depending on the number of subjects with sessions, " +
				"this report may take quite some time to generate.  You will receive an e-mail with a zip " +
				"attachment containing your CSV report files upon completion.";
			xmodal.message({
				title:  "Report Generation Request Sent",
				width:  '550px',
				height:  '275px',
				content:  msg
			});
		})
		.fail( function( data, textStatus, error ) {
			var responseText = data.responseText;
			try {
				var parseResponseText = JSON.parse(responseText);
				if (parseResponseText.constructor === Array) {
					responseText = "";
					for (var i=0;i<parseResponseText.length;i++) {
						responseText = responseText + "<li>" + parseResponseText[i] + "</li>";
					}
				}
			} catch (e) {
				// Do nothing
			}
	 		var errMsg = "<h2 style='color:#AA1111'>Could not generate report</h2><em>REASON:</em> &nbsp;" +  error + "<br><br><em>DETAILS:</em><br><br>" + responseText;
			
			$("#report-modal-overlay").hide();
			xmodal.message({
				title:  "Combined Session Building Results",
				width:  '800px',
				height:  '630px',
				content:  errMsg
			});
			xmodal.close('reportSessionConfirmModal');
		});
	 }

	confirmModal.id  = 'reportSessionConfirmModal';
	confirmModal.content  = '<h2 style="margin-bottom:10px;margin-top:10px">Generate Session Report (ALL SUBJECTS WITH COMBINED SESSIONS)?</h2>';
	confirmModal.cancelAction = function(){ return; };
	confirmModal.title = "Combined Session Builder";
	confirmModal.width = 650;
	confirmModal.height = 400;
	xModalOpenNew(confirmModal)
 	//console.log(CCF.sessionbuilding.configuration);
 	XNAT.spawner.spawn(CCF.sessionbuilding.spawnReport(true)).render($("#reportSessionConfirmModal").find(".body").find(".inner"));
 	$("#reportSessionConfirmModal").find(".body").find(".inner").append(
		"<div id='report-modal-overlay' style='width:100%;height:100%;display:none;background-color:#FFFFFF;" +
		"z-index:10;opacity:0.9;margin:0px;position:absolute;top:0px;left:0px;padding:0px'>" + 
		"<div style='color:#22AA22;width:100%'><h3 style='text-align:center'>Processing request. &nbsp;Please wait...</h3</div>" + 
		"<div style='top:50%;left:50%;margin-right: -50%;transform: translate(-50%, -50%);position:absolute;opacity:1.0;'><img src='/images/loading.gif'/></div>" +
		"</div><iframe id='download_iframe' style='display:none;'></iframe>");
	$("#ccfSessionBuildingRulesClass").find("option[value='" + CCF.sessionbuilding.configuration.ccfSessionBuildingRulesClass + "']").prop('selected', true); 
	//$("#ccfSessionBuildingReportType").find("option[value='CSV']").prop('selected', true); 
	$($("#ccfSessionBuildingReportType")[0]).attr('checked', true); 
	$("input:radio[name=ccfSessionBuildingReportType]").attr('disabled', true); 
	CCF.sessionbuilding.updateParameters();
	$("#ccfSessionBuildingRulesClass").change(function() { CCF.sessionbuilding.updateParameters(); } );

}


