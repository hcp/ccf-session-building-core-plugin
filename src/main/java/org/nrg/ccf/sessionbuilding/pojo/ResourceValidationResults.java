package org.nrg.ccf.sessionbuilding.pojo;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@RequiredArgsConstructor
@Setter
public class ResourceValidationResults {
	
	@NonNull
	private Boolean isValid;
	@Setter(AccessLevel.NONE)
	private final List<String> errorList = new ArrayList<>();
	
	public static ResourceValidationResults newValidInstance() {
		return new ResourceValidationResults(true);
	}

}
