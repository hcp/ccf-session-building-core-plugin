package org.nrg.ccf.sessionbuilding.runner;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.CcfWorkflowUtils;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sessionbuilding.enums.ProcessingType;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.exception.SessionBuildingException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseFileHandlerI;
import org.nrg.ccf.sessionbuilding.interfaces.CcfResourceGeneratorI;
import org.nrg.ccf.sessionbuilding.pojo.RulesProcessingResults;
import org.nrg.ccf.sessionbuilding.runner.result.SessionBuilderRunResults;
import org.nrg.ccf.sessionbuilding.utils.CcfReleaseRulesUtils;
import org.nrg.ccf.sessionbuilding.xapi.CcfSessionBuildingController;
import org.nrg.xdat.model.XnatFielddefinitiongroupI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatProjectparticipantI;
import org.nrg.xdat.om.XnatAbstractprotocol;
import org.nrg.xdat.om.XnatDatatypeprotocol;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatExperimentdataField;
import org.nrg.xdat.om.XnatFielddefinitiongroup;
import org.nrg.xdat.om.XnatFielddefinitiongroupField;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatProjectparticipant;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.BaseXnatDemographicdata;
import org.nrg.xdat.om.base.BaseXnatSubjectdata;
import org.nrg.xdat.security.ElementSecurity;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.EventUtils.CATEGORY;
import org.nrg.xft.event.EventUtils.TYPE;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.helpers.file.StoredFile;
import org.nrg.xnat.helpers.resource.XnatResourceInfo;
import org.nrg.xnat.helpers.resource.direct.DirectExptResourceImpl;

import lombok.NonNull;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BuildSessionRunner implements Runnable {

	private XnatSubjectdata _subj;
	private XnatImagesessiondata _combSess;
	private UserI _sessionUser;
	private Boolean _overrideAsWarning;
	private CcfReleaseFileHandlerI _fileHandler;
	private CcfResourceGeneratorI _resourceGenerator;
	private String _siteUrl;
	private RulesProcessingResults _rulesProcessingResults;
	private ProcessingType _processingType;
	private Map<String, String> _params;
	private List<String> _currentlyBuildingList;
	private static final String SESSION_BUILDING_RESULTS_CATALOG = "session_building_report";
	private static final String SESSION_BUILDING_RESULTS_FILE_APPENDINFO = "_SessionBuildingReport";
	private static final String SESSION_BUILDING_RESULTS_FILE_FILETYPE = ".html";
	private String errorInfo = "";
	
	public static final String SESSIONBUILDING_FIELDDEFINITION_GROUP = "Session Building Fields";

	public BuildSessionRunner(XnatSubjectdata subj, XnatImagesessiondata combSess,
			UserI sessionUser, CcfReleaseFileHandlerI fileHandler, CcfResourceGeneratorI resourceGenerator,
			Map<String, String> params, String siteUrl, ProcessingType processingType, RulesProcessingResults rulesProcessingResults) {
		_subj = subj;
		_combSess = combSess;
		_sessionUser = sessionUser;
		_fileHandler = fileHandler;
		_resourceGenerator = resourceGenerator;
		_siteUrl = siteUrl;
		_params = params;
		_processingType = processingType;
		_rulesProcessingResults = rulesProcessingResults;
	}

	public BuildSessionRunner(XnatSubjectdata subj, XnatImagesessiondata combSess,
			UserI sessionUser, CcfReleaseFileHandlerI fileHandler, CcfResourceGeneratorI resourceGenerator,
			Map<String, String> params, List<String> currentlyBuildingList, String siteUrl, ProcessingType processingType, RulesProcessingResults rulesProcessingResults) {
		this(subj, combSess, sessionUser, fileHandler, resourceGenerator, params, siteUrl, processingType, rulesProcessingResults);
		_currentlyBuildingList = currentlyBuildingList;
	}

	@Override
	public void run() {
		
		runWait(true);

	}
	
	public SessionBuilderRunResults runWait(boolean emailResults) {
		
		SessionBuilderRunResults results;
		boolean okStatus = false;
		String message	=  (_processingType.equals(ProcessingType.BUILD_SESSION)) ?
				"Session building has FAILED for session " + _combSess.getLabel() + "." :
				"File transfer has FAILED for session " + _combSess.getLabel() + "." 
				;
		try {
			okStatus = doTheRun();
			if (okStatus) {
				if (_siteUrl!= null && _siteUrl.length()>0) {
						message = ((_processingType.equals(ProcessingType.BUILD_SESSION)) ?
								"Session building has completed for session <a href='" :
								"File transfer has completed for session <a href='")
								+  _siteUrl + "/data/experiments/" + _combSess.getId() + "?format=html'>" + _combSess.getLabel() + "</a>.";
				} else {
						message = ((_processingType.equals(ProcessingType.BUILD_SESSION)) ?
								"Session building has completed for session " :
								"File transfer has completed for session " )
				+ _combSess.getLabel() + ".";
				}
			} else if (errorInfo!=null && errorInfo.length()>0) {
				message = message.concat("<br><br>ERRORINFO:  " + errorInfo);
			}
			results = new SessionBuilderRunResults(okStatus,message);
		} catch (Throwable t) {
			if (!message.contains("ERRORINFO:") && errorInfo!=null && errorInfo.length()>0) {
				message = message.concat("<br><br>ERRORINFO:  " + errorInfo);
			}
			message	= message.concat("<br><br>EXCEPTION: " + t.toString());
			results = new SessionBuilderRunResults(false,message);
		} finally {
			if (_currentlyBuildingList != null) {
				_currentlyBuildingList.remove(_subj.getId());
			}
			if (emailResults) {
				AdminUtils.sendUserHTMLEmail(
						message = ((_processingType.equals(ProcessingType.BUILD_SESSION)) ? "Session building " : "File transfer " )
								+ ((okStatus) ? "complete" : "failed") +
						" (SESSION=" + _combSess.getLabel() + ")<br><br>" + _rulesProcessingResults, message , false, new String[] { _sessionUser.getEmail() } );
			}
		}
		return results;

	}
	
	private boolean doTheRun() {
		
		final String combSessSubjId;
		
		final boolean shareSubject = _params.containsKey(CcfSessionBuildingController.SUBJECT_PARAM) && 
				_params.get(CcfSessionBuildingController.SUBJECT_PARAM).equalsIgnoreCase(CcfSessionBuildingController.SHARE_VALUE);
		final boolean overwriteExisting = _params.containsKey(CcfSessionBuildingController.OVERWRITE_PARAM) &&
				_params.get(CcfSessionBuildingController.OVERWRITE_PARAM).equalsIgnoreCase("true");
		_overrideAsWarning = _params.containsKey(CcfSessionBuildingController.OVERRIDE_AS_WARNING_PARAM) &&
				_params.get(CcfSessionBuildingController.OVERRIDE_AS_WARNING_PARAM).equalsIgnoreCase("true");
		
		if (_processingType.equals(ProcessingType.BUILD_SESSION)) {
			final List<XnatMrsessiondata> currentSessions = XnatMrsessiondata.getXnatMrsessiondatasByField("xnat:mrSessionData/label",_combSess.getLabel(), _sessionUser, false);
			for (final XnatMrsessiondata currentSession : currentSessions) {
				if (currentSession.getProject().equals(_combSess.getProject())) {
					if (overwriteExisting) {
						_combSess.setId(currentSession.getId());
					} else {
						errorInfo = "Session with this label already exists in the destination and <em>overwriteExisting</em> flag is false.";
						return false;
					}
				}
			}
			if (_combSess.getId() == null || _combSess.getId().length()<1) {
				try {
					_combSess.setId(XnatExperimentdata.CreateNewID());
				} catch (Exception e1) {
					errorInfo = "SERVER EXCEPTION - Couldn't generate experiment ID:  " + ExceptionUtils.getStackTrace(e1);
					log.error(errorInfo);
					return false;
				}
			}
			
			final XnatProjectdata combSessProj = XnatProjectdata.getXnatProjectdatasById(_combSess.getProject(), _sessionUser, false);
			try {
				combSessSubjId = createOrShareSubjectIfNecessary(_subj, combSessProj, shareSubject);
			} catch (Exception e1) {
				errorInfo = "SERVER EXCEPTION - Couldn't create or share subject:  " + ExceptionUtils.getStackTrace(e1);
				log.error(errorInfo);
				return false;
			}
			_combSess.setSubjectId(combSessSubjId);
			PersistentWorkflowI wrk;
			try {
				wrk = PersistentWorkflowUtils.buildOpenWorkflow(_sessionUser, _combSess.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.CREATE_VIA_WEB_SERVICE, null, null));
				final EventMetaI ci = wrk.buildEvent();
				if (SaveItemHelper.authorizedSave(_combSess,_sessionUser,false,overwriteExisting,ci)) {
					PersistentWorkflowUtils.complete(wrk, ci);
				} else {
					PersistentWorkflowUtils.fail(wrk,ci);
				}
				_fileHandler.transferFiles();
				_resourceGenerator.generateResources();
				setOverrideField();
				setOverrideAsWarningField(_overrideAsWarning);
				saveSessionBuildingReport();
				return true;
			} catch (Exception e) {
				errorInfo = "SERVER EXCEPTION:  " + ExceptionUtils.getStackTrace(e);
				log.error(errorInfo);
				try {
					log.error(_combSess.getItem().toXML_String());
				} catch (Exception e2) {
					log.error("ERROR:  Couldn't output session XML");
				}
				for (XnatImagescandataI scan : _combSess.getScans_scan()) {
					try {
						log.error(((XnatMrscandata)scan).getItem().toXML_String());
					} catch (Exception e2) {
						log.error("ERROR:  Couldn't output XML - SCAN " + scan.getId());
					}
				}
				return false;
			}	
		} else { // TRANSFER_FILES
			try {
				_fileHandler.transferFiles();
				_resourceGenerator.generateResources();
				return true;
			} catch (Exception e) {
				errorInfo = "SERVER EXCEPTION:  " + ExceptionUtils.getStackTrace(e);
				log.error(errorInfo);
				try {
					log.error(_combSess.getItem().toXML_String());
				} catch (Exception e2) {
					log.error("ERROR:  Couldn't output session XML");
				}
				for (XnatImagescandataI scan : _combSess.getScans_scan()) {
					try {
						log.error(((XnatMrscandata)scan).getItem().toXML_String());
					} catch (Exception e2) {
						log.error("ERROR:  Couldn't output XML - SCAN " + scan.getId());
					}
				}
				return false;
			}	
		}
		
	}

	private void setOverrideField() throws Exception {
		
		configureExperimentFields();
		final XnatExperimentdataField field = new XnatExperimentdataField();
		field.setName(CommonConstants.RULES_OVERRIDE_REQUIRED_FIELD);
		field.setField(isOverrideRequired(_rulesProcessingResults).toString());
		_combSess.addFields_field(field);
		
	}

	private void setOverrideAsWarningField(Boolean value) throws Exception {
		configureExperimentFields();
		final XnatExperimentdataField field = new XnatExperimentdataField();
		field.setName(CommonConstants.OVERRIDE_AS_WARNING_FIELD);
		field.setField(value.toString());
		_combSess.addFields_field(field);
		
	}

	private Boolean isOverrideRequired(RulesProcessingResults rulesProcessingResults) {
		if (rulesProcessingResults == null) {
			return false;
		}
		if ((!rulesProcessingResults.getSuccess()) || (rulesProcessingResults.getCaughtException() != null)) {
			return true;
		} else if (rulesProcessingResults.getWarningList().size()>0) {
			for (String info : rulesProcessingResults.getWarningList()) {
				// Not optimal, but we're assuming reporting consistency here
				if (!info.contains("RULESWARNING:")) {
					return true;
				}
			}
		}
		return false;
	}

	private void saveSessionBuildingReport() throws Exception {
		
		if (_rulesProcessingResults == null) {
			return;
		}
		EventMetaI ci = CcfWorkflowUtils.buildEvent(_sessionUser, _combSess.getItem(), CATEGORY.DATA, TYPE.PROCESS,
				"Save session building report", PersistentWorkflowUtils.COMPLETE);
		final XnatResourcecatalog catalog = ResourceUtils.createSessionResource(_combSess, SESSION_BUILDING_RESULTS_CATALOG, _sessionUser, ci);
		catalog.clearCountAndSize();
		//final String catalogPath = catalog.getCatalogFile(CommonConstants.ROOT_PATH).getParentFile().getAbsolutePath();
		final String resultsName = _combSess.getLabel() + SESSION_BUILDING_RESULTS_FILE_APPENDINFO;
		final Path reportP = Files.createTempFile(resultsName, SESSION_BUILDING_RESULTS_FILE_FILETYPE);
		final File reportF = reportP.toFile();
		FileUtils.writeStringToFile(reportF, buildReport(_rulesProcessingResults.getStatus()), false);
		final ArrayList<StoredFile> fws = new ArrayList<>();
		fws.add(new StoredFile(reportF, true));
		final DirectExptResourceImpl sessionModifier = new DirectExptResourceImpl(_combSess.getProjectData(),_combSess,true,_sessionUser,ci);
		try {
			sessionModifier.addFile(fws, catalog.getLabel(), null, resultsName + SESSION_BUILDING_RESULTS_FILE_FILETYPE,
					new XnatResourceInfo(_sessionUser, new Date(), new Date()), false);
		} catch (Exception e1) {
			throw new ReleaseResourceException("ERROR:  Exception thrown saving file to resource.",e1);
		}
		catalog.calculate(CommonConstants.ROOT_PATH);
		catalog.setFileCount(catalog.getCount(CommonConstants.ROOT_PATH));
		catalog.setFileSize(catalog.getSize(CommonConstants.ROOT_PATH));
		//EventMetaI cat_ci = CcfWorkflowUtils.buildEvent(_sessionUser, _combSess.getItem(), CATEGORY.DATA, TYPE.PROCESS,
		//		"Save session building report", PersistentWorkflowUtils.COMPLETE);
		SaveItemHelper.authorizedSave(catalog,_sessionUser,false,false,ci);
		
	}

	private String buildReport(@NonNull String status) {
		final StringBuilder sb = new StringBuilder("<!DOCTYPE html>\n<html lang=\"en\">\n<body>\n");
		sb.append("<h3>Session Building Report:  ").append(_combSess.getLabel()).append("</h3>\n");
		sb.append(status);
		sb.append("\n</body>\n</html>\n");
		return sb.toString();
	}

	private String createOrShareSubjectIfNecessary(final XnatSubjectdata subj, final XnatProjectdata proj, final boolean shareSubject) throws Exception {
		if (proj == null) {
			throw new SessionBuildingException("ERROR:  Passed project value is null.  Please make sure configured session building process exists");
		}
		if (subj.getProject().equals(proj.getId())) {
			return subj.getId();
		}
		// See if subject already exists in project
		final XnatSubjectdata existingSubj =  XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), 
				CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj), _sessionUser, false);
		if (existingSubj!=null) {
			return existingSubj.getId();
		}
		boolean matched=false;
		for (final XnatProjectparticipantI pp : subj.getSharing_share()) {
			if (pp.getProject().equals(proj.getId())) {
				return pp.getSubjectId();
			}
		}
		if (!matched) {
			if (shareSubject) {
				final XnatProjectparticipantI pp= new XnatProjectparticipant(_sessionUser);
				((XnatProjectparticipant)pp).setProject(proj.getId());
				subj.setSharing_share((XnatProjectparticipant)pp);
				PersistentWorkflowI wrk;
				try {
					wrk = PersistentWorkflowUtils.buildOpenWorkflow(_sessionUser, subj.getItem(),
							EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.MODIFY_VIA_WEB_SERVICE, null, null));
					final EventMetaI ci = wrk.buildEvent();
					if (SaveItemHelper.authorizedSave(subj,_sessionUser,false,true,ci)) {
						PersistentWorkflowUtils.complete(wrk, ci);
					} else {
						PersistentWorkflowUtils.fail(wrk,ci);
					}
				} catch (Exception e) {
					log.error("SERVER EXCEPTION:  " + ExceptionUtils.getStackTrace(e));
					throw(e);
				}
				return subj.getId();
			} else {
				final XnatSubjectdata newSubj = new XnatSubjectdata();
				newSubj.setLabel(CcfReleaseRulesUtils.getSubjectLabelForProject(subj, proj));
				newSubj.setId(BaseXnatSubjectdata.CreateNewID());
				newSubj.setProject(proj.getId());
				if (subj.getDemographics()!=null & subj.getDemographics() instanceof BaseXnatDemographicdata) {
					// Keep only gender and handedness in newly created subject (drop PHI)
					final BaseXnatDemographicdata oldDemog = (BaseXnatDemographicdata)subj.getDemographics();
					final BaseXnatDemographicdata newDemog = new BaseXnatDemographicdata();
					newDemog.setGender(oldDemog.getGender());
					newDemog.setHandedness(oldDemog.getHandedness());
					newSubj.setDemographics(newDemog.getItem());
				}
				PersistentWorkflowI wrk;
				try {
					wrk = PersistentWorkflowUtils.buildOpenWorkflow(_sessionUser, newSubj.getItem(),
							EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.CREATE_VIA_WEB_SERVICE, null, null));
					final EventMetaI ci = wrk.buildEvent();
					if (SaveItemHelper.authorizedSave(newSubj,_sessionUser,false,true,ci)) {
						PersistentWorkflowUtils.complete(wrk, ci);
					} else {
						PersistentWorkflowUtils.fail(wrk,ci);
					}
				} catch (Exception e) {
					log.error("SERVER EXCEPTION:  " + ExceptionUtils.getStackTrace(e));
					throw(e);
				}
				return newSubj.getId();
			}
		}
		return null;
	}
	
	
	//private void configureExperimentFields(String projectId, String xsiType) throws Exception {
	private void configureExperimentFields() throws Exception {
		
		final UserI user = _sessionUser;
		final String projectId = _combSess.getProject();
		final String xsiType = _combSess.getXSIType();
		final List<XnatFielddefinitiongroup> groups = XnatFielddefinitiongroup.getXnatFielddefinitiongroupsByField(XnatFielddefinitiongroup.SCHEMA_ELEMENT_NAME + "/id",
				SESSIONBUILDING_FIELDDEFINITION_GROUP, user, false);
		XnatFielddefinitiongroup fieldDefGroup = null;
		for (final XnatFielddefinitiongroup group : groups) {
			if (!group.getDataType().equals(xsiType)) {
				continue;
			}
			for (final XnatFielddefinitiongroupField field : group.getFields_field()) {
				if (field.getName().equals(CommonConstants.RULES_OVERRIDE_REQUIRED_FIELD) ||
				    field.getName().equals(CommonConstants.OVERRIDE_AS_WARNING_FIELD)) {
					fieldDefGroup  = group;
				}
			}
		}
		if (fieldDefGroup == null) {
			fieldDefGroup = new XnatFielddefinitiongroup();
			fieldDefGroup.setId(SESSIONBUILDING_FIELDDEFINITION_GROUP);
			fieldDefGroup.setDescription(SESSIONBUILDING_FIELDDEFINITION_GROUP);
			fieldDefGroup.setDataType(xsiType);
			fieldDefGroup.setShareable(true);
			fieldDefGroup.setProjectSpecific(false);
			addFieldDefGroupField(fieldDefGroup,CommonConstants.RULES_OVERRIDE_REQUIRED_FIELD,"custom","boolean",false,xsiType);
			addFieldDefGroupField(fieldDefGroup,CommonConstants.OVERRIDE_AS_WARNING_FIELD,"custom","boolean",false,xsiType);
			final PersistentWorkflowI wrk;
			try {
				wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, fieldDefGroup.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.CREATE_VIA_WEB_SERVICE, null, null));
				final EventMetaI ci = wrk.buildEvent();
				if (SaveItemHelper.authorizedSave(fieldDefGroup, user, true, false, ci)) {
					PersistentWorkflowUtils.complete(wrk,ci);
				} else {
					log.error("Failed to save FieldDefinitionGroup",ci.getMessage());
					PersistentWorkflowUtils.fail(wrk,ci);
				}
			} catch (Exception e) {
				log.error("Exception thrown saving FieldDefinitionGroup",e);
			}
		} else {
			checkAndUpdateField(fieldDefGroup, CommonConstants.RULES_OVERRIDE_REQUIRED_FIELD, "custom", "boolean", xsiType, user);
			checkAndUpdateField(fieldDefGroup, CommonConstants.OVERRIDE_AS_WARNING_FIELD, "custom", "boolean", xsiType, user);
		}
		final CriteriaCollection cc = new CriteriaCollection("AND");
		cc.addClause(XnatAbstractprotocol.SCHEMA_ELEMENT_NAME + "/data-type",xsiType);
		cc.addClause(XnatAbstractprotocol.SCHEMA_ELEMENT_NAME + "/xnat_projectdata_id",projectId);
		final List<XnatDatatypeprotocol> protocols = XnatDatatypeprotocol.getXnatDatatypeprotocolsByField(cc, user, false);
		if (protocols == null || protocols.isEmpty()) {
			final XnatDatatypeprotocol protocol = new XnatDatatypeprotocol();
			protocol.setId(projectId + "_" + xsiType.replaceAll(":", "_"));
			protocol.setName(ElementSecurity.GetPluralDescription(xsiType));
			protocol.setDescription("");
			protocol.setDataType(xsiType);
			protocol.setProperty(XnatAbstractprotocol.SCHEMA_ELEMENT_NAME + "/xnat_projectdata_id",projectId);
			protocol.addDefinitions_definition(fieldDefGroup);
			final PersistentWorkflowI wrk;
			try {
				wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, protocol.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.CREATE_VIA_WEB_SERVICE, null, null));
				final EventMetaI ci = wrk.buildEvent();
				if (SaveItemHelper.authorizedSave(protocol, user, false, false, ci)) {
					PersistentWorkflowUtils.complete(wrk,ci);
				} else {
					log.error("Failed to save XnatDatatypeprotocol",ci.getMessage());
					PersistentWorkflowUtils.fail(wrk,ci);
				}
			} catch (Exception e) {
				log.error("Exception thrown saving XnatDatatypeprotocol",e);
			}
		} else {
			for (final XnatDatatypeprotocol protocol : protocols) {
				final List<XnatFielddefinitiongroupI> definitions = protocol.getDefinitions_definition();
				boolean foundDef = false;
				for (final XnatFielddefinitiongroupI def : definitions) {
					if (def.getXnatFielddefinitiongroupId().equals(fieldDefGroup.getXnatFielddefinitiongroupId())) {
						foundDef = true;
					}
				}
				if (!foundDef) {
					protocol.addDefinitions_definition(fieldDefGroup);
				}
				final PersistentWorkflowI wrk;
				try {
					wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, protocol.getItem(),
							EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.MODIFY_VIA_WEB_SERVICE, null, null));
					final EventMetaI ci = wrk.buildEvent();
					if (SaveItemHelper.authorizedSave(protocol, user, false, true, ci)) {
						PersistentWorkflowUtils.complete(wrk,ci);
					} else {
						log.error("Failed to save XnatDatatypeprotocol",ci.getMessage());
						PersistentWorkflowUtils.fail(wrk,ci);
					}
				} catch (Exception e) {
					log.error("Exception thrown saving XnatDatatypeprotocol",e);
				}
			}
		}
		
	}
	
	private void checkAndUpdateField(XnatFielddefinitiongroup fieldDefGroup, String fieldName, String fieldType, String fieldDataType, String xsiType, UserI user) throws Exception {
		boolean hasOverrideField = false;
		for (final XnatFielddefinitiongroupField field : fieldDefGroup.getFields_field()) {
			if (field.getName().equals(fieldName)) {
				hasOverrideField = true;
			}
		}
		if (!(hasOverrideField)) {
			if (!hasOverrideField) {
				addFieldDefGroupField(fieldDefGroup,fieldName,fieldType,fieldDataType,false,xsiType);
			}
			final PersistentWorkflowI wrk;
			try {
				wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, fieldDefGroup.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.MODIFY_VIA_WEB_SERVICE, null, null));
				final EventMetaI ci = wrk.buildEvent();
				if (SaveItemHelper.authorizedSave(fieldDefGroup, user, true, true, ci)) {
					PersistentWorkflowUtils.complete(wrk,ci);
				} else {
					log.error("Failed to save FieldDefinitionGroup",ci.getMessage());
					PersistentWorkflowUtils.fail(wrk,ci);
				}
			} catch (Exception e) {
				log.error("Exception thrown saving FieldDefinitionGroup",e);
			}
		}
	}

	private void addFieldDefGroupField(XnatFielddefinitiongroup fieldDefGroup, String fieldName, String fieldType, String fieldDataType, Boolean required, String xsiType) throws Exception {
			final XnatFielddefinitiongroupField field = new XnatFielddefinitiongroupField();
			field.setName(fieldName);
			field.setType(fieldType);
			field.setDatatype(fieldDataType);
			field.setRequired(required);
			field.setXmlpath(xsiType + "/fields/field[name=" + fieldName + "]/field");
			fieldDefGroup.addFields_field(field);
	}

}
